import { describe, expect, test, vi } from 'vitest'

import { cleanup } from '@testing-library/svelte'
import matchers from '@testing-library/jest-dom/matchers'

expect.extend(matchers)

vi.mock('./src/module/utils.js', () => ({
  getLocalizer: () => (a) => a,
  getGame: vi.fn(),
}))

// runs a cleanup after each test case (e.g. clearing jsdom)
afterEach(() => {
  cleanup()
})

const Hooks = {
  once: vi.fn(),
  on: vi.fn(),
}
global.Hooks = Hooks

class ActiveEffectConfig {
  async getData() {
    return {
      effect: {
        changes: [],
      },
    }
  }
}
vi.stubGlobal('ActiveEffectConfig', ActiveEffectConfig)
