import * as fs from 'fs'

// Gitlab Regex: Total Coverage: (\d+\.\d+ \%)
const jsonSummary = 'coverage/coverage-summary.json'
const coverageRaw = fs.readFileSync(jsonSummary)
const coverage = JSON.parse(coverageRaw)

const totalSum = ['lines', 'statements', 'functions', 'branches']
  .map((i) => coverage.total[i].pct)
  .reduce((a, b) => a + b, 0)
const avgCoverage = totalSum / 4
console.debug()
console.debug('========= Total Coverage ============')
console.debug(`Total Coverage: ${avgCoverage.toFixed(2)} %`)
console.debug('=======================================')
