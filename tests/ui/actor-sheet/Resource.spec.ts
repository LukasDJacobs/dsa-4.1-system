import { fireEvent, render } from '@testing-library/svelte'

import html from 'svelte-htm'

import Resource from '../../../src/ui/actor-sheet/Resource.svelte'
import { ResourceData } from '../../../src/module/model/character-data.js'
import { get, writable } from 'svelte/store'

describe('Resource', () => {
  test('Is rendered correctly and can update the value', async () => {
    const resource = writable({
      value: 5,
      min: 0,
      max: 7,
    } as ResourceData)
    const { getByRole } = render(
      html`<${Resource} bind:resource=${resource} />`
    )

    const text = getByRole('gridcell')
    const currentValue = getByRole('spinbutton')
    expect(text).toBeInTheDocument()
    expect(text).toHaveTextContent(get(resource).max + ' max')
    expect(currentValue).toHaveValue(get(resource).value)
    const newValue = 3
    await fireEvent.input(currentValue, { target: { value: newValue } })
    expect(currentValue).toHaveValue(newValue)
    expect(get(resource).value).toBe(newValue)
  })
})
