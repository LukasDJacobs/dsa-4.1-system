import { render } from '@testing-library/svelte'

import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

vi.mock(
  '../../../src/ui/Dialog.svelte',
  async () => await import('../MockDialog.svelte')
)

import { getGame } from '../../../src/module/utils.js'
;(getGame as Mock<any>).mockImplementation(() => ({
  ruleset: createMockRuleset(),
}))

import { Mock } from 'vitest'
import { createMockRuleset } from '../../ruleset/rules/helpers.js'
import { writable } from 'svelte/store'
import ActorSheetHeader from '../../../src/ui/actor-sheet/ActorSheetHeader.svelte'
import { attributeNames } from '../../../src/module/model/character-data.js'

describe('ActorSheetHeader', () => {
  test('Is rendered correctly and can update the value', async () => {
    const value = 5
    const doc = {
      name: 'testName',
      system: {
        base: {
          basicAttributes: attributeNames.reduce(
            (a, v) => ({ ...a, [v]: value }),
            {}
          ),
        },
      },
    }
    const context = {
      doc: writable(doc),
      '#external': {
        application: {},
      },
    }
    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${ActorSheetHeader} />
      </$>
      `
    )

    const attributes = getByTestId('actor-attributes')
    attributeNames.forEach((name) => expect(attributes).toHaveTextContent(name))

    const actorName = getByTestId('actor-name')
    expect(actorName).toHaveTextContent(doc.name)
  })
})
