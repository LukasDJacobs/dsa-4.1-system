import { fireEvent, render } from '@testing-library/svelte'

import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

import { writable } from 'svelte/store'
import Wounds from '../../../src/ui/actor-sheet/Wounds.svelte'
import { createMockRuleset } from '../../ruleset/rules/helpers.js'

import { getGame } from '../../../src/module/utils.js'
import { vi } from 'vitest'
;(getGame as vi.Mock<any>).mockImplementation(() => ({
  ruleset: createMockRuleset(),
}))

describe('Wounds', () => {
  test('Is rendered correctly in view mode', async () => {
    const addWoundMock = vi.fn()
    const removeWoundMock = vi.fn()
    const context = {
      doc: writable({
        countWounds: vi.fn().mockReturnValue(3),
        addWound: addWoundMock,
        removeWound: removeWoundMock,
      }),
    }

    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${Wounds} />
      </$>
      `
    )
    const woundCount = getByTestId('wound-count').textContent
    expect(woundCount).toEqual('3')

    const addWound = getByTestId('add-wound')
    await fireEvent.click(addWound)
    expect(addWoundMock).toHaveBeenCalled()

    const removeWound = getByTestId('remove-wound')
    await fireEvent.click(removeWound)
    expect(removeWoundMock).toHaveBeenCalled()
  })
})
