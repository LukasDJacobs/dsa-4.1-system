import { render, within } from '@testing-library/svelte'

import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

import { writable } from 'svelte/store'
import WoundThresholds from '../../../src/ui/actor-sheet/WoundThresholds.svelte'
import { createMockRuleset } from '../../ruleset/rules/helpers.js'

import { getGame } from '../../../src/module/utils.js'
import { vi } from 'vitest'
;(getGame as vi.Mock<any>).mockImplementation(() => ({
  ruleset: createMockRuleset(),
}))

describe('WoundThresholds', () => {
  test('Is rendered correctly in view mode', async () => {
    global.game = {
      ruleset: createMockRuleset(),
    }
    const context = {
      doc: writable({
        system: {
          base: {
            combatAttributes: {
              passive: {
                woundThresholds: {
                  first: 5,
                  second: 10,
                  third: 15,
                  mod: 3,
                },
              },
            },
          },
        },
      }),
    }

    const { findAllByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${WoundThresholds} />
      </$>
      `
    )

    const woundThresholdElements = await findAllByTestId('wound-threshold')
    expect(woundThresholdElements).toHaveLength(3)
    const woundThresholds = woundThresholdElements.map((el) => el.textContent)
    expect(woundThresholds).toEqual(['8', '13', '18'])
  })

  test('Is rendered correctly in edit mode', async () => {
    global.game = {
      ruleset: createMockRuleset(),
    }
    const context = {
      isEditMode: writable(true),

      doc: writable({
        system: {
          base: {
            combatAttributes: {
              passive: {
                woundThresholds: {
                  first: 5,
                  second: 10,
                  third: 15,
                  mod: 3,
                },
              },
            },
          },
        },
      }),
    }

    const { findAllByTestId, getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${WoundThresholds} />
      </$>
      `
    )

    const woundThresholdElements = await findAllByTestId('wound-threshold')
    expect(woundThresholdElements).toHaveLength(3)
    const woundThresholds = woundThresholdElements.map(
      (el) => (within(el).getByRole('spinbutton') as HTMLInputElement).value
    )
    expect(woundThresholds).toEqual(['5', '10', '15'])

    const woundThresholdModElement = getByTestId('wound-threshold-mod')
    expect(
      (
        within(woundThresholdModElement).getByRole(
          'spinbutton'
        ) as HTMLInputElement
      ).value
    ).toEqual('3')
  })
})
