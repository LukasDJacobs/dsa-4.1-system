import { render, within } from '@testing-library/svelte'

import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

import Trait from '../../../src/ui/actor-sheet/Trait.svelte'
import type {
  DataAccessor,
  TraitItemType,
} from '../../../src/module/model/item-data.js'
import { Nameable } from '../../../src/module/model/utils.js'

describe('Trait', () => {
  const context = {
    '#external': {},
  }

  test('Is rendered correctly with value', async () => {
    const trait = {
      sheet: {
        render: vi.fn(),
      },
      system: {
        value: 4,
      },
      name: 'testName',
    } as any as DataAccessor<'advantage' | 'disadvantage'>
    const { getByRole } = render(
      html`
      <${Fragment} context=${context}>
      <${Trait} trait=${trait} />
      </$>
      `
    )

    const text = getByRole('gridcell')
    expect(text).toBeInTheDocument()
    expect(text).toHaveTextContent(trait.name + ` (${trait.system.value})`)
    expect(within(text).queryByTestId('link')).not.toBeInTheDocument()
  })

  test('Is rendered correctly without value', async () => {
    const trait = {
      sheet: {
        render: vi.fn(),
      },
      system: {},
      name: 'testName',
    } as any as Nameable<DataAccessor<TraitItemType>>
    const { getByRole } = render(
      html`
      <${Fragment} context=${context}>
      <${Trait} trait=${trait} />
      </$>
      `
    )
    const text = getByRole('gridcell')
    expect(text).toBeInTheDocument()
    expect(text).toHaveTextContent(trait.name)
    expect(within(text).queryByTestId('link')).not.toBeInTheDocument()
  })
})
