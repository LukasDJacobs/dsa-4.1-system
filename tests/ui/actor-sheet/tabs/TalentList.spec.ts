import { fireEvent, render, within } from '@testing-library/svelte'

import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

vi.mock(
  '../../../../src/ui/Dialog.svelte',
  async () => await import('../../MockDialog.svelte')
)
import { getGame } from '../../../../src/module/utils.js'

import TalentList from '../../../../src/ui/actor-sheet/tabs/TalentList.svelte'
import { writable } from 'svelte/store'
import { Mock } from 'vitest'
import { createMockRuleset } from '../../../ruleset/rules/helpers.js'

describe('TalentList', () => {
  test('Is rendered correctly', async () => {
    const ruleset = createMockRuleset()

    ;(getGame as Mock<any>).mockImplementation(() => ({
      ruleset,
    }))
    const value = 5

    const talent = {
      system: {
        value,
        test: {
          firstAttribute: 'test',
          secondAttribute: 'teste',
          thirdAttribute: 'testen',
        },
        effectiveEncumbarance: {
          type: 'none',
        },
      },
      type: 'talent',
      name: 'testTalent',
      sheet: {
        render: vi.fn(),
      },
      update: vi.fn(),
    }
    const talentWithCategory = (category) => {
      const t = { ...talent, system: { ...talent.system } }
      t.system.category = category
      return t
    }
    const talents = [
      talentWithCategory('physical'),
      talentWithCategory('physical'),
      talentWithCategory('nature'),
    ]
    const context = {
      '#external': {},
      isEditMode: writable(true),
      doc: writable({
        system: {
          base: {},
        },
        talents,
      }),

      openDialogMock: vi.fn(),
    }
    const { getByTestId, findAllByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${TalentList}  />
      </$>
      `
    )

    let talentList = await findAllByTestId('talent-value')
    expect(talentList.length).toEqual(3)

    const categoryFilter = getByTestId('category-filter')

    await fireEvent.click(within(categoryFilter).getByText('physical'))

    talentList = await findAllByTestId('talent-value')
    expect(talentList.length).toEqual(2)

    await fireEvent.click(within(categoryFilter).getByText('nature'))

    talentList = await findAllByTestId('talent-value')
    expect(talentList.length).toEqual(1)

    await fireEvent.click(within(categoryFilter).getByText('nature'))

    talentList = await findAllByTestId('talent-value')
    expect(talentList.length).toEqual(3)
  })

  test('Is rendered correctly if provided only one talent category', async () => {
    const ruleset = createMockRuleset()

    ;(getGame as Mock<any>).mockImplementation(() => ({
      ruleset,
    }))
    const value = 5

    const talent = {
      system: {
        value,
        test: {
          firstAttribute: 'test',
          secondAttribute: 'teste',
          thirdAttribute: 'testen',
        },
        effectiveEncumbarance: {
          type: 'none',
        },
      },
      type: 'talent',
      name: 'testTalent',
      sheet: {
        render: vi.fn(),
      },
      update: vi.fn(),
    }
    const talentWithCategory = (category) => {
      const t = { ...talent, system: { ...talent.system } }
      t.system.category = category
      return t
    }
    const talents = [
      talentWithCategory('physical'),
      talentWithCategory('physical'),
      talentWithCategory('nature'),
    ]
    const context = {
      '#external': {},
      isEditMode: writable(true),
      doc: writable({
        system: {
          base: {},
        },
        talents,
      }),

      openDialogMock: vi.fn(),
    }
    const { findAllByTestId, queryByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${TalentList} talentCategories=${['nature']}   />
      </$>
      `
    )

    const talentList = await findAllByTestId('talent-value')
    expect(talentList.length).toEqual(1)

    const categoryFilter = queryByTestId('category-filter')
    expect(categoryFilter).not.toBeInTheDocument()
  })
})
