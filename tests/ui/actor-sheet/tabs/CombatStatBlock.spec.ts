import { fireEvent, render } from '@testing-library/svelte'

import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

vi.mock(
  '../../../../src/ui/Dialog.svelte',
  async () => await import('../../MockDialog.svelte')
)
vi.mock('../../../../src/module/character/character.js', () => {
  const Character = vi.fn()
  Character.prototype.maneuverList = vi.fn()
  Character.prototype.damage = vi.fn()
  Character.prototype.attackValue = vi.fn()
  Character.prototype.rangedAttackValue = vi.fn()
  Character.prototype.parryValue = vi.fn()
  Character.prototype.attack = vi.fn()
  Character.prototype.rangedAttack = vi.fn()
  Character.prototype.parry = vi.fn()
  Character.prototype.rollDamage = vi.fn()
  return { Character }
})
import { getGame } from '../../../../src/module/utils.js'
import { Character } from '../../../../src/module/character/character.js'

import CombatStatBlock from '../../../../src/ui/actor-sheet/tabs/CombatStatBlock.svelte'
import { writable } from 'svelte/store'
import { Mock } from 'vitest'
import { createMockRuleset } from '../../../ruleset/rules/helpers.js'

describe('CombatStatBlock', () => {
  test.each([[true], [false]])('Is rendered correctly', async (isSecondary) => {
    const ruleset = createMockRuleset()

    ;(getGame as Mock<any>).mockImplementation(() => ({
      ruleset,
    }))

    const weapon = {
      name: 'testWeapon',
      type: 'meleeWeapon',
    }
    const context = {
      isEditMode: writable(true),
      doc: writable({
        system: {
          base: {},
        },
      }),

      openDialogMock: vi.fn(),
    }
    const mockCharacter = new Character({} as any) as vi.Mock<Character>

    mockCharacter.attackValue.mockReturnValue(12)
    mockCharacter.parryValue.mockReturnValue(11)
    mockCharacter.damage.mockReturnValue('1d6')
    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${CombatStatBlock} weapon=${weapon} isSecondary=${isSecondary}/>
      </$>
      `
    )

    const attackRoll = getByTestId('attack-roll')
    expect(attackRoll).toHaveTextContent('attack 12')
    expect(mockCharacter.attackValue).toHaveBeenCalledWith(isSecondary)

    await fireEvent.click(attackRoll)

    expect(context.openDialogMock).toHaveBeenCalled()
    expect(mockCharacter.attack).toHaveBeenCalled()

    const parryRoll = getByTestId('parry-roll')
    expect(parryRoll).toHaveTextContent('parry 11')
    expect(mockCharacter.parryValue).toHaveBeenCalledWith(isSecondary)

    await fireEvent.click(parryRoll)

    expect(context.openDialogMock).toHaveBeenCalled()
    expect(mockCharacter.parry).toHaveBeenCalled()

    const damageRoll = getByTestId('damage-roll')
    expect(damageRoll).toHaveTextContent('damage 1d6')
    expect(mockCharacter.damage).toHaveBeenCalledWith(isSecondary)

    await fireEvent.click(damageRoll)

    expect(context.openDialogMock).toHaveBeenCalled()
    expect(mockCharacter.rollDamage).toHaveBeenCalled()
  })

  test('Is rendered correctly', async () => {
    const ruleset = createMockRuleset()

    ;(getGame as Mock<any>).mockImplementation(() => ({
      ruleset,
    }))

    const weapon = {
      name: 'testWeapon',
      type: 'rangedWeapon',
    }
    const context = {
      isEditMode: writable(true),
      doc: writable({
        system: {
          base: {},
        },
      }),

      openDialogMock: vi.fn(),
    }
    const mockCharacter = new Character({} as any) as vi.Mock<Character>

    mockCharacter.rangedAttackValue.mockReturnValue(12)
    mockCharacter.damage.mockReturnValue('1d6')
    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${CombatStatBlock} weapon=${weapon} />
      </$>
      `
    )

    const rangedAttackRoll = getByTestId('ranged-attack-roll')
    expect(rangedAttackRoll).toHaveTextContent('rangedAttack 12')
    expect(mockCharacter.rangedAttackValue).toHaveBeenCalledWith()

    await fireEvent.click(rangedAttackRoll)

    expect(context.openDialogMock).toHaveBeenCalled()
    expect(mockCharacter.attack).toHaveBeenCalled()

    const damageRoll = getByTestId('damage-roll')
    expect(damageRoll).toHaveTextContent('damage 1d6')
    expect(mockCharacter.damage).toHaveBeenCalledWith(false)

    await fireEvent.click(damageRoll)

    expect(context.openDialogMock).toHaveBeenCalled()
    expect(mockCharacter.rollDamage).toHaveBeenCalled()
  })
})
