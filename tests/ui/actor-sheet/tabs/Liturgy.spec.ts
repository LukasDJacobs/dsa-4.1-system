import { fireEvent, render, within } from '@testing-library/svelte'

import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

vi.mock(
  '../../../../src/ui/Dialog.svelte',
  async () => await import('../../MockDialog.svelte')
)
import { getGame } from '../../../../src/module/utils.js'

vi.mock('../../../../src/module/pdf-integration.ts', () => {
  const openPDFPage = vi.fn()
  class EnhancedJournalPDFPageSheet {}
  return {
    EnhancedJournalPDFPageSheet: EnhancedJournalPDFPageSheet,
    openPDFPage: openPDFPage,
  }
})

import { openPDFPage } from '../../../../src/module/pdf-integration.js'

import Liturgy from '../../../../src/ui/actor-sheet/tabs/Liturgy.svelte'
import { writable } from 'svelte/store'
import { Mock } from 'vitest'
import { createMockRuleset } from '../../../ruleset/rules/helpers.js'

describe('Liturgy', () => {
  test('Is rendered correctly', async () => {
    const ruleset = createMockRuleset()

    ;(getGame as Mock<any>).mockImplementation(() => ({
      ruleset,
    }))
    const degree = 5

    const liturgy = {
      system: {
        degree,
        targetClasses: ['tc1', 'tc2'],
        castTime: {
          duration: 2,
          unit: 'rounds',
        },
        range: 5,
      },
      type: 'liturgy',
      name: 'testLiturgy',
      sheet: {
        render: vi.fn(),
      },
      update: vi.fn(),
    }
    const context = {
      '#external': {},
      isEditMode: writable(true),
      doc: writable({
        system: {
          base: {},
        },
        talent: () => ({}),
        talents: [],
      }),

      openDialogMock: vi.fn(),
    }
    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${Liturgy} liturgy=${liturgy} />
      </$>
      `
    )

    const liturgyRoll = getByTestId('liturgy-roll')
    const openPDF = getByTestId('open-pdf')
    const liturgyEdit = getByTestId('liturgy-edit')
    const liturgyDegree = within(getByTestId('liturgy-degree')).getByRole(
      'spinbutton'
    )
    const liturgyTargetClasses = getByTestId('liturgy-targetClasses')
    const liturgyCastTime = getByTestId('liturgy-casttime')
    const liturgyRange = getByTestId('liturgy-range')

    expect(liturgyEdit).toBeInTheDocument()
    expect(liturgyDegree).toHaveValue(degree)
    expect(liturgyTargetClasses).toHaveTextContent('tc1, tc2')
    expect(liturgyCastTime).toHaveTextContent('2 rounds')
    expect(liturgyRange).toHaveTextContent('5')

    await fireEvent.click(liturgyEdit)
    expect(liturgy.sheet.render).toHaveBeenCalled()

    await fireEvent.change(liturgyDegree)
    expect(liturgy.update).toHaveBeenCalled()

    await fireEvent.click(liturgyRoll)

    expect(context.openDialogMock).toHaveBeenCalled()
    expect(ruleset.execute).toHaveBeenCalled()

    await fireEvent.click(openPDF)
    expect(openPDFPage).toHaveBeenCalled()
  })

  test('Is rendered correctly in view mode', async () => {
    const ruleset = createMockRuleset()

    ;(getGame as Mock<any>).mockImplementation(() => ({
      ruleset,
    }))
    const degree = 5

    const liturgy = {
      system: {
        degree,
        targetClasses: ['tc1', 'tc2'],
        castTime: {
          duration: 2,
          unit: 'rounds',
        },
        range: 5,
      },
      type: 'liturgy',
      name: 'testLiturgy',
      sheet: {
        render: vi.fn(),
      },
      update: vi.fn(),
    }
    const context = {
      '#external': {},
      isEditMode: writable(false),
      doc: writable({
        system: {
          base: {},
        },
        talent: () => ({}),
        talents: [],
      }),

      openDialogMock: vi.fn(),
    }
    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${Liturgy} liturgy=${liturgy} />
      </$>
      `
    )

    const liturgyRoll = getByTestId('liturgy-roll')
    const liturgyDegree = getByTestId('liturgy-degree')
    const liturgyTargetClasses = getByTestId('liturgy-targetClasses')
    const liturgyCastTime = getByTestId('liturgy-casttime')
    const liturgyRange = getByTestId('liturgy-range')

    expect(liturgyDegree).toHaveTextContent(degree.toString())
    expect(liturgyTargetClasses).toHaveTextContent('tc1, tc2')
    expect(liturgyCastTime).toHaveTextContent('2 rounds')
    expect(liturgyRange).toHaveTextContent('5')

    await fireEvent.click(liturgyRoll)

    expect(context.openDialogMock).toHaveBeenCalled()
    expect(ruleset.execute).toHaveBeenCalled()
  })
})
