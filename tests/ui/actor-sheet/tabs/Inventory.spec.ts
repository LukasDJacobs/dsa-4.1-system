import { render, within } from '@testing-library/svelte'

import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

import Inventory from '../../../../src/ui/actor-sheet/tabs/Inventory.svelte'
import { writable } from 'svelte/store'

describe('Inventory', () => {
  test('Is rendered correctly', async () => {
    const items = {
      meleeWeapon: [{ name: 'testWeapon', system: { isConsumable: false } }],
    }
    const context = {
      isEditMode: writable(true),
      doc: writable({
        system: {
          base: {},
        },
        itemTypes: items,
      }),
      '#external': {
        application: {
          _onDragStart: vi.fn(),
        },
      },
    }
    const { findAllByRole } = render(
      html`
      <${Fragment} context=${context}>
      <${Inventory}  />
      </$>
      `
    )

    const tabs = await findAllByRole('tab')
    expect(tabs.map((tab) => tab.textContent)).toEqual([
      'meleeWeapons',
      'rangedWeapons',
      'armor',
      'shields',
      'genericItems',
    ])

    const tabPanel = (await findAllByRole('tabpanel'))[0]
    expect(within(tabPanel).getByTestId('item-name')).toHaveTextContent(
      'testWeapon'
    )
  })
})
