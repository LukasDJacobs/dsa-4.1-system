import { fireEvent, render, within } from '@testing-library/svelte'

import Fragment from 'svelte-fragment-component'
import html from 'svelte-htm'

vi.mock(
  '../../../../src/ui/Dialog.svelte',
  async () => await import('../../MockDialog.svelte')
)
import { getGame } from '../../../../src/module/utils.js'
;(getGame as vi.Mock<any>).mockImplementation(() => ({
  ruleset: createMockRuleset(),
}))

import BaseStats from '../../../../src/ui/actor-sheet/tabs/BaseStats.svelte'

import { createMockRuleset } from '../../../ruleset/rules/helpers.js'
import { writable } from 'svelte/store'
import userEvent from '@testing-library/user-event'

describe('BaseStats', () => {
  test('Is rendered correctly with value', async () => {
    global.game = {
      ruleset: createMockRuleset(),
    }
    const negativeAttribute = {
      name: 'Angst',
      system: {
        negativeAttribute: true,
        value: 10,
      },
      update: vi.fn(),
    }
    const disadvantage = {
      name: 'Klein',
      system: {
        negativeAttribute: false,
      },
    }
    const disadvantages = [negativeAttribute, disadvantage]
    const advantages = [
      {
        name: 'Toll',
        system: {},
      },
    ]
    const specialAbilities = [
      {
        name: 'Wuchtschlag',
        system: {},
      },
    ]
    const effects = [
      {
        name: 'testEffect',
      },
      {
        name: 'testEffect2',
      },
    ]
    const doc = {
      system: {
        base: {},
      },
      disadvantage: vi.fn(),
      itemTypes: {
        disadvantage: disadvantages,
        advantage: advantages,
        specialAbility: specialAbilities,
      },
      effects: {
        contents: effects,
      },
    }
    const context = {
      '#external': {},
      isEditMode: writable(true),
      doc: writable(doc),

      openDialogMock: vi.fn(),
    }
    doc.disadvantage.mockReturnValue(disadvantages[0])

    const { getByTestId } = render(html`
        <${Fragment} context=${context}>
        <${BaseStats} />
        </$>
    `)

    const negativeAttributes = getByTestId('negative-attributes')
    expect(negativeAttributes).toHaveTextContent(negativeAttribute.name)
    expect(negativeAttributes).not.toHaveTextContent(disadvantage.name)
    expect(
      (await within(negativeAttributes).findAllByTestId('attribute-roll'))
        .length
    ).toEqual(1)

    const negativeAttributeInput =
      within(negativeAttributes).getByRole('spinbutton')

    const newValue = 12
    await userEvent.type(
      negativeAttributeInput,
      '{backspace}{backspace}' + newValue.toString()
    )
    await fireEvent.change(negativeAttributeInput)

    expect(negativeAttribute.update).toHaveBeenCalledWith(
      expect.objectContaining({
        system: expect.objectContaining({ value: newValue }),
      })
    )

    const advantageList = getByTestId('advantage-list')
    expect(advantageList).toHaveTextContent(advantages[0].name)

    const disadvantageList = getByTestId('disadvantage-list')
    expect(disadvantageList).toHaveTextContent(disadvantage.name)

    const specialAbilityList = getByTestId('special-ability-list')
    expect(specialAbilityList).toHaveTextContent(specialAbilities[0].name)

    const effectList = getByTestId('effect-list')
    expect(effectList).toHaveTextContent('add')
    expect(effectList).toHaveTextContent(effects[0].name)
    expect(effectList).toHaveTextContent(effects[1].name)
    expect(
      (await within(effectList).findAllByTestId('effect-label')).length
    ).toEqual(2)
  })
})
