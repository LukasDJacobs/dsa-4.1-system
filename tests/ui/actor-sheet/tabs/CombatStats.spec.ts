import { fireEvent, render, within } from '@testing-library/svelte'

import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

vi.mock(
  '../../../../src/ui/Dialog.svelte',
  async () => await import('../../MockDialog.svelte')
)
vi.mock('../../../../src/module/character/character.js', () => {
  const Character = vi.fn()
  Character.prototype.maneuverList = vi.fn()
  Character.prototype.damage = vi.fn()
  Character.prototype.attackValue = vi.fn()
  Character.prototype.rangedAttackValue = vi.fn()
  Character.prototype.parryValue = vi.fn()
  Character.prototype.attack = vi.fn()
  Character.prototype.rangedAttack = vi.fn()
  Character.prototype.parry = vi.fn()
  Character.prototype.dodge = vi.fn()
  Character.prototype.rollDamage = vi.fn()
  Character.prototype.initiative = vi.fn()
  Character.prototype.dodgeValue = vi.fn()
  return { Character }
})
import { getGame } from '../../../../src/module/utils.js'
import { Character } from '../../../../src/module/character/character.js'

import CombatStats from '../../../../src/ui/actor-sheet/tabs/CombatStats.svelte'
import { writable } from 'svelte/store'
import { Mock } from 'vitest'
import { createMockRuleset } from '../../../ruleset/rules/helpers.js'
import userEvent from '@testing-library/user-event'

describe('CombatStats', () => {
  test('Is rendered correctly', async () => {
    const ruleset = createMockRuleset()

    ;(getGame as Mock<any>).mockImplementation(() => ({
      ruleset,
    }))

    const weapon = {
      name: 'testWeapon',
      id: 'testWeaponValue',
      type: 'meleeWeapon',
    }
    const shield = {
      name: 'testShield',
      type: 'shield',
    }
    const talents = []
    const weapons = [weapon]
    const shields = [shield]
    const doc = {
      system: {
        base: {
          combatState: {
            primaryHand: undefined,
          },
        },
      },
      combatState: {
        isArmed: true,
      },
      talents,
      weapons,
      shields,
    }
    const context = {
      isEditMode: writable(true),
      doc: writable(doc),

      openDialogMock: vi.fn(),
    }
    const mockCharacter = new Character({} as any) as vi.Mock<Character>
    const initiativeData = {
      formula: '1d6+3',
    }

    mockCharacter.attackValue.mockReturnValue(12)
    mockCharacter.parryValue.mockReturnValue(11)
    mockCharacter.dodgeValue.mockReturnValue(8)
    mockCharacter.damage.mockReturnValue('1d6')
    mockCharacter.initiative.mockReturnValue(initiativeData)
    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${CombatStats}  />
      </$>
      `
    )

    const primaryHand = getByTestId('primary-hand-options')
    expect(
      Array.from(
        (within(primaryHand).getByRole('combobox') as HTMLSelectElement).options
      ).map((o) => o.textContent)
    ).toEqual(expect.arrayContaining([weapon.name]))

    const secondaryHand = getByTestId('secondary-hand-options')
    expect(
      Array.from(
        (within(secondaryHand).getByRole('combobox') as HTMLSelectElement)
          .options
      ).map((o) => o.textContent)
    ).toEqual(expect.arrayContaining([weapon.name, shield.name]))

    const initiatve = getByTestId('initiative')
    expect(initiatve).toHaveTextContent(initiativeData.formula)

    const combatStats = getByTestId('combat-stats')
    // eslint-disable-next-line jest-dom/prefer-to-have-text-content
    expect(combatStats.textContent).toEqual(' ')

    await userEvent.selectOptions(
      within(primaryHand).getByRole('combobox'),
      weapon.id
    )
    expect(doc.system.base.combatState.primaryHand).toEqual(weapon.id)

    const dodgeRoll = getByTestId('dodge-roll')
    expect(dodgeRoll).toHaveTextContent('dodge 8')
    expect(mockCharacter.dodgeValue).toHaveBeenCalledWith()

    await fireEvent.click(dodgeRoll)

    expect(context.openDialogMock).toHaveBeenCalled()
    expect(mockCharacter.dodge).toHaveBeenCalled()
  })

  test('Is rendered correctly', async () => {
    const ruleset = createMockRuleset()

    ;(getGame as Mock<any>).mockImplementation(() => ({
      ruleset,
    }))

    const weapon = {
      name: 'testWeapon',
      id: 'testWeaponValue',
      type: 'meleeWeapon',
    }
    const shield = {
      name: 'testShield',
      type: 'shield',
    }
    const talents = []
    const weapons = [weapon]
    const shields = [shield]
    const doc = {
      system: {
        base: {
          combatState: {
            primaryHand: undefined,
          },
        },
      },
      combatState: {
        isArmed: true,
        primaryHand: {
          name: 'testWeapon',
        },
        secondaryHand: {
          name: 'testAnotherWeapon',
        },
        unarmedTalent: {
          name: 'unarmedTalentTest',
        },
      },
      talents,
      weapons,
      shields,
    }
    const context = {
      isEditMode: writable(true),
      doc: writable(doc),

      openDialogMock: vi.fn(),
    }
    const mockCharacter = new Character({} as any) as vi.Mock<Character>
    const initiativeData = {
      formula: '1d6+3',
    }

    mockCharacter.attackValue.mockReturnValue(12)
    mockCharacter.parryValue.mockReturnValue(11)
    mockCharacter.damage.mockReturnValue('1d6')
    mockCharacter.initiative.mockReturnValue(initiativeData)
    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${CombatStats} />
      </$>
      `
    )
    const combatStats = getByTestId('combat-stats')

    expect(combatStats).toHaveTextContent(doc.combatState.primaryHand.name)
    expect(combatStats).toHaveTextContent(doc.combatState.secondaryHand.name)
    expect(combatStats).not.toHaveTextContent(
      doc.combatState.unarmedTalent.name
    )
  })

  test('Is rendered correctly', async () => {
    const ruleset = createMockRuleset()

    ;(getGame as Mock<any>).mockImplementation(() => ({
      ruleset,
    }))

    const talent = {
      name: 'testTalent',
      id: 'testTalentValue',
      type: 'combatTalent',
      system: {
        combat: {
          category: 'unarmed',
        },
      },
    }

    const weapon = {
      name: 'testWeapon',
      id: 'testWeaponValue',
      type: 'meleeWeapon',
    }
    const shield = {
      name: 'testShield',
      type: 'shield',
    }
    const talents = [talent]
    const weapons = [weapon]
    const shields = [shield]
    const doc = {
      system: {
        base: {
          combatState: {
            primaryHand: undefined,
          },
        },
      },
      combatState: {
        isArmed: false,
        primaryHand: {
          name: 'testWeapon',
        },
        secondaryHand: {
          name: 'testAnotherWeapon',
        },
        unarmedTalent: {
          name: 'unarmedTalentTest',
        },
      },
      talents,
      weapons,
      shields,
    }
    const context = {
      isEditMode: writable(true),
      doc: writable(doc),

      openDialogMock: vi.fn(),
    }
    const mockCharacter = new Character({} as any) as vi.Mock<Character>
    const initiativeData = {
      formula: '1d6+3',
    }

    mockCharacter.attackValue.mockReturnValue(12)
    mockCharacter.parryValue.mockReturnValue(11)
    mockCharacter.damage.mockReturnValue('1d6')
    mockCharacter.initiative.mockReturnValue(initiativeData)
    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${CombatStats}  />
      </$>
      `
    )

    const unarmedTalent = getByTestId('unarmed-talent-options')
    expect(
      Array.from(
        (within(unarmedTalent).getByRole('combobox') as HTMLSelectElement)
          .options
      ).map((o) => o.textContent)
    ).toEqual(expect.arrayContaining([talent.name]))

    const combatStats = getByTestId('combat-stats')

    expect(combatStats).not.toHaveTextContent(doc.combatState.primaryHand.name)
    expect(combatStats).not.toHaveTextContent(
      doc.combatState.secondaryHand.name
    )
    expect(combatStats).toHaveTextContent(doc.combatState.unarmedTalent.name)
  })
})
