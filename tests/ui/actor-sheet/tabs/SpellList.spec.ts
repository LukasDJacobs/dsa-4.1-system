import { render } from '@testing-library/svelte'

import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

vi.mock(
  '../../../../src/ui/Dialog.svelte',
  async () => await import('../../MockDialog.svelte')
)
import { getGame } from '../../../../src/module/utils.js'

vi.mock('../../../../src/module/pdf-integration.ts', () => {
  const openPDFPage = vi.fn()
  class EnhancedJournalPDFPageSheet {}
  return {
    EnhancedJournalPDFPageSheet: EnhancedJournalPDFPageSheet,
    openPDFPage: openPDFPage,
  }
})

import SpellList from '../../../../src/ui/actor-sheet/tabs/SpellList.svelte'
import { writable } from 'svelte/store'
import { Mock } from 'vitest'
import { createMockRuleset } from '../../../ruleset/rules/helpers.js'

describe('SpellList', () => {
  test('Is rendered correctly', async () => {
    const ruleset = createMockRuleset()

    ;(getGame as Mock<any>).mockImplementation(() => ({
      ruleset,
    }))
    const value = 5

    const spell = {
      system: {
        value,
        test: {
          firstAttribute: 'test',
          secondAttribute: 'teste',
          thirdAttribute: 'testen',
        },
        effectiveEncumbarance: {
          type: 'none',
        },
      },
      type: 'spell',
      name: 'testSpell',
      sheet: {
        render: vi.fn(),
      },
      update: vi.fn(),
    }
    const spells = [spell, spell, spell]
    const context = {
      '#external': {},
      isEditMode: writable(true),
      doc: writable({
        system: {
          base: {},
        },
        spells,
      }),

      openDialogMock: vi.fn(),
    }
    const { findAllByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${SpellList}  />
      </$>
      `
    )

    const spellList = await findAllByTestId('spell-value')
    expect(spellList.length).toEqual(3)
  })
})
