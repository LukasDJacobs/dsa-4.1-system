import { fireEvent, render } from '@testing-library/svelte'

import html from 'svelte-htm'

import Effect from '../../../../src/ui/actor-sheet/tabs/Effect.svelte'

describe('Effect', () => {
  test('Is rendered correctly', async () => {
    const effect = {
      name: 'testEffect',
      disabled: false,
      sheet: { render: vi.fn() },
      delete: vi.fn(),
      update: vi.fn(),
    }

    const { getByTestId } = render(html` <${Effect} effect=${effect} /> `)

    const label = getByTestId('effect-label')
    expect(label).toHaveTextContent(effect.name)

    await fireEvent.click(label)
    expect(effect.sheet.render).toHaveBeenCalled()

    const deleteEffect = getByTestId('effect-delete')
    await fireEvent.click(deleteEffect)
    expect(effect.delete).toHaveBeenCalled()

    const disableEffect = getByTestId('effect-disable')
    await fireEvent.click(disableEffect)
    expect(effect.update).toHaveBeenCalledWith({ disabled: true })
    effect.disabled = true

    await fireEvent.click(disableEffect)
    expect(effect.update).toHaveBeenCalledWith({ disabled: false })
  })
})
