import { render } from '@testing-library/svelte'
import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

import EffectList from '../../../../src/ui/actor-sheet/tabs/EffectList.svelte'
import { writable } from 'svelte/store'

describe('EffectList', () => {
  test('Is rendered correctly', async () => {
    const effects = [
      {
        name: 'testEffect',
      },
      {
        name: 'testEffect2',
      },
    ]
    const context = {
      doc: writable({
        effects: {
          contents: effects,
        },
      }),
    }
    const { findAllByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${EffectList}  />
      </$>
      `
    )

    const labels = await findAllByTestId('effect-label')
    expect(labels.map((label) => label.textContent)).toEqual(
      effects.map((effect) => effect.name)
    )
  })
})
