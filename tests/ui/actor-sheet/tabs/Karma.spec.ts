import { render } from '@testing-library/svelte'

import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

vi.mock(
  '../../../../src/ui/Dialog.svelte',
  async () => await import('../../MockDialog.svelte')
)
import { getGame } from '../../../../src/module/utils.js'

vi.mock('../../../../src/module/pdf-integration.ts', () => {
  const openPDFPage = vi.fn()
  class EnhancedJournalPDFPageSheet {}
  return {
    EnhancedJournalPDFPageSheet: EnhancedJournalPDFPageSheet,
    openPDFPage: openPDFPage,
  }
})

import Karma from '../../../../src/ui/actor-sheet/tabs/Karma.svelte'
import { writable } from 'svelte/store'
import { Mock } from 'vitest'
import { createMockRuleset } from '../../../ruleset/rules/helpers.js'

describe('Karma', () => {
  test('Is rendered correctly', async () => {
    const ruleset = createMockRuleset()

    ;(getGame as Mock<any>).mockImplementation(() => ({
      ruleset,
    }))
    const degree = 5

    const liturgy = {
      system: {
        degree,
        targetClasses: ['tc1', 'tc2'],
        castTime: {
          duration: 2,
          unit: 'rounds',
        },
        range: 5,
      },
      type: 'liturgy',
      name: 'testLiturgy',
      sheet: {
        render: vi.fn(),
      },
      update: vi.fn(),
    }
    const liturgies = [liturgy, liturgy, liturgy]

    const value = 5

    const karmaTalent = {
      system: {
        value,
        category: 'karma',
        effectiveEncumbarance: {
          type: 'none',
        },
      },
      type: 'talent',
      name: 'testTalent',
      sheet: {
        render: vi.fn(),
      },
      update: vi.fn(),
    }

    const context = {
      '#external': {},
      isEditMode: writable(true),
      doc: writable({
        system: {
          base: {},
        },
        talent: () => ({}),
        liturgies,
        talents: [karmaTalent],
      }),

      openDialogMock: vi.fn(),
    }
    const { findAllByTestId, getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${Karma}  />
      </$>
      `
    )

    const liturgyList = await findAllByTestId('liturgy-degree')
    expect(liturgyList.length).toEqual(3)

    const karmaTalentRoll = getByTestId('talent-roll')
    expect(karmaTalentRoll).toHaveTextContent(karmaTalent.name)
  })
})
