import { fireEvent, render, within } from '@testing-library/svelte'

import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

vi.mock(
  '../../../../src/ui/Dialog.svelte',
  async () => await import('../../MockDialog.svelte')
)
import { getGame } from '../../../../src/module/utils.js'

vi.mock('../../../../src/module/pdf-integration.ts', () => {
  const openPDFPage = vi.fn()
  class EnhancedJournalPDFPageSheet {}
  return {
    EnhancedJournalPDFPageSheet: EnhancedJournalPDFPageSheet,
    openPDFPage: openPDFPage,
  }
})

import { openPDFPage } from '../../../../src/module/pdf-integration.js'

import Spell from '../../../../src/ui/actor-sheet/tabs/Spell.svelte'
import { writable } from 'svelte/store'
import { Mock } from 'vitest'
import { createMockRuleset } from '../../../ruleset/rules/helpers.js'

describe('Spell', () => {
  test('Is rendered correctly', async () => {
    const ruleset = createMockRuleset()

    ;(getGame as Mock<any>).mockImplementation(() => ({
      ruleset,
    }))
    const value = 5

    const spell = {
      system: {
        value,
        test: {
          firstAttribute: 'test',
          secondAttribute: 'teste',
          thirdAttribute: 'testen',
        },
      },
      type: 'spell',
      name: 'testSpell',
      sheet: {
        render: vi.fn(),
      },
      update: vi.fn(),
    }
    const context = {
      '#external': {},
      isEditMode: writable(true),
      doc: writable({
        system: {
          base: {},
        },
      }),

      openDialogMock: vi.fn(),
    }
    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${Spell} spell=${spell} />
      </$>
      `
    )

    const spellRoll = getByTestId('spell-roll')
    const openPDF = getByTestId('open-pdf')
    const spellEdit = getByTestId('spell-edit')
    const spellValue = within(getByTestId('spell-value')).getByRole(
      'spinbutton'
    )

    expect(spellEdit).toBeInTheDocument()
    expect(spellValue).toHaveValue(value)

    expect(spellRoll).toHaveTextContent(spell.name)
    expect(spellRoll).toHaveTextContent('test_abbr / teste_abbr / testen_abbr')

    await fireEvent.click(spellEdit)
    expect(spell.sheet.render).toHaveBeenCalled()

    await fireEvent.change(spellValue)
    expect(spell.update).toHaveBeenCalled()

    await fireEvent.click(spellRoll)

    expect(context.openDialogMock).toHaveBeenCalled()
    expect(ruleset.execute).toHaveBeenCalled()

    await fireEvent.click(openPDF)
    expect(openPDFPage).toHaveBeenCalled()
  })

  test('Is rendered correctly in view mode', async () => {
    const ruleset = createMockRuleset()

    ;(getGame as Mock<any>).mockImplementation(() => ({
      ruleset,
    }))
    const value = 5

    const spell = {
      system: {
        value,
        test: {
          firstAttribute: 'test',
          secondAttribute: 'teste',
          thirdAttribute: 'testen',
        },
        effectiveEncumbarance: {
          type: 'none',
        },
      },
      type: 'spell',
      name: 'testSpell',
      sheet: {
        render: vi.fn(),
      },
      update: vi.fn(),
    }
    const context = {
      '#external': {},
      isEditMode: writable(false),
      doc: writable({
        system: {
          base: {},
        },
      }),

      openDialogMock: vi.fn(),
    }
    const { getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${Spell} spell=${spell} />
      </$>
      `
    )

    const spellRoll = getByTestId('spell-roll')
    const spellValue = getByTestId('spell-value')

    expect(spellValue).toHaveTextContent(value.toString())

    expect(spellRoll).toHaveTextContent(spell.name)
    expect(spellRoll).toHaveTextContent('test_abbr / teste_abbr / testen_abbr')

    await fireEvent.click(spellRoll)

    expect(context.openDialogMock).toHaveBeenCalled()
    expect(ruleset.execute).toHaveBeenCalled()
  })
})
