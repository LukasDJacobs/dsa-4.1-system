import { render, within } from '@testing-library/svelte'

import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

import Resources from '../../../src/ui/actor-sheet/Resources.svelte'
import {
  ResourceData,
  resourceNames,
} from '../../../src/module/model/character-data.js'
import { writable } from 'svelte/store'

describe('Resources', () => {
  test('Is rendered correctly', async () => {
    const resource = {
      value: 5,
      min: 0,
      max: 7,
    } as ResourceData
    const context = {
      doc: writable({
        system: {
          settings: {
            hasAstralEnergy: true,
            hasKarmicEnergy: true,
          },
        },
      }),
    }
    const resources = resourceNames.reduce(
      (a, v) => ({ ...a, [v]: resource }),
      {}
    )
    const { getAllByRole } = render(
      html`
      <${Fragment} context=${context}>
      <${Resources} resources=${resources} />
      </$>
      `
    )

    const groups = getAllByRole('group')
    expect(
      groups.map((group) => within(group).getByTestId('label').textContent)
    ).toEqual(resourceNames)
    groups.forEach((group) =>
      expect(within(group).getByRole('spinbutton')).toHaveValue(resource.value)
    )
  })
})
