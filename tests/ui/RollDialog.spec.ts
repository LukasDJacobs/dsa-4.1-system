import { fireEvent, render } from '@testing-library/svelte'
import RollDialog from '../../src/ui/RollDialog.svelte'

describe('RollDialog', () => {
  test('Is rendered correctly and submits correctly', async () => {
    const callback = vi.fn()
    const foundryApp = {
      close: vi.fn(),
      setPosition: vi.fn(),
    }
    const { getByRole } = render(RollDialog, { callback, foundryApp })
    const button = getByRole('button')
    const input = getByRole('spinbutton')
    expect(button).toBeInTheDocument()
    expect(input).toBeInTheDocument()
    expect(button).toHaveTextContent('roll')
    expect(input.parentElement).toHaveTextContent('modifier')

    const inputValue = 23

    await fireEvent.input(input, { target: { value: inputValue } })

    await fireEvent.click(button)
    expect(callback).toHaveBeenCalledWith(inputValue)
    expect(foundryApp.close).toHaveBeenCalled()
  })
})
