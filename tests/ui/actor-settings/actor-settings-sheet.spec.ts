import ActorSettingsSheet from '../../../src/ui/actor-settings/ActorSettingsSheet.svelte'
import { render } from '@testing-library/svelte'
import userEvent from '@testing-library/user-event'
import { writable } from 'svelte/store'
import { CharacterSettings } from '../../../src/module/model/character-data.js'

describe('ActorSettingsSheet', () => {
  test.each<[keyof CharacterSettings, boolean, boolean]>([
    ['autoCalcBaseAttack', false, true],
    ['autoCalcBaseAttack', true, false],
    ['autoCalcBaseParry', false, true],
    ['autoCalcBaseParry', true, false],
    ['autoCalcBaseRangedAttack', false, true],
    ['autoCalcBaseRangedAttack', true, false],
    ['autoCalcInitiative', false, true],
    ['autoCalcInitiative', true, false],
    ['autoCalcMagicResistance', false, true],
    ['autoCalcMagicResistance', true, false],
    ['autoCalcWoundThresholds', false, true],
    ['autoCalcWoundThresholds', true, false],
  ])(
    'Submit without changes',
    async (
      optionName: keyof CharacterSettings,
      oldValue: boolean,
      newValue: boolean
    ) => {
      const update = vi.fn()
      const doc = {
        system: {
          settings: { [optionName]: oldValue },
        },
        update,
      }
      const store = writable(doc)
      const { getByLabelText } = render(ActorSettingsSheet, {
        doc: store,
        localize: (l) => l,
      })
      const attackCheckbox = getByLabelText(optionName)
      await userEvent.click(attackCheckbox)

      expect(update).toHaveBeenCalledWith(
        expect.objectContaining({
          system: { settings: { [optionName]: newValue } },
        })
      )
    }
  )
})
