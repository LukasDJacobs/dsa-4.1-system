import Select from '../../src/ui/Select.svelte'
import { render } from '@testing-library/svelte'

describe('Select', () => {
  test('Is rendered correctly as empty select', async () => {
    const items = []
    const { getByRole } = render(Select, { items })
    const select = getByRole('combobox', {
      name: 'select',
    }) as HTMLSelectElement
    expect(select).toBeInTheDocument()
    expect(select.options.length).toBe(0)
  })

  test('Is rendered correctly as a grouped select and preselects', async () => {
    const items = [
      {
        label: 'groupless',
        value: 0,
      },
      {
        label: 'First',
        value: 1,
        group: 'First group',
      },
      {
        label: 'Second in first group',
        value: 2,
        group: 'First group',
      },

      {
        label: 'Second',
        value: 3,
        group: 'second group',
      },
    ]
    const selected = 2
    const expectedSelectedIndex = 3
    const { getByRole } = render(Select, { items, value: selected })
    const select = getByRole('combobox', {
      name: 'select',
    }) as HTMLSelectElement
    expect(select).toBeInTheDocument()
    expect(select.options.length).toBe(6)
    expect(select.options.selectedIndex).toBe(expectedSelectedIndex)
    expect(select.options.item(0)?.value).toBe('0')
    expect(select.options.item(0)?.text).toBe('groupless')

    expect(select.options.item(1)?.text).toBe('First group')
    expect(select.options.item(1)?.disabled).toBe(true)
    expect(select.options.item(2)?.value).toBe('1')
    expect(select.options.item(2)?.text).toBe('First')
    expect(select.options.item(3)?.value).toBe('2')
    expect(select.options.item(3)?.text).toBe('Second in first group')

    expect(select.options.item(4)?.text).toBe('second group')
    expect(select.options.item(4)?.disabled).toBe(true)
    expect(select.options.item(5)?.value).toBe('3')
    expect(select.options.item(5)?.text).toBe('Second')
  })
})
