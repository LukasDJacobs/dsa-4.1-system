import { describe, expect, test, vi } from 'vitest'
import { fireEvent, render } from '@testing-library/svelte'
import html from 'svelte-htm'
import Fragment from 'svelte-fragment-component'

import EffectCreator from '../../src/ui/EffectCreator.svelte'
import { writable } from 'svelte/store'

describe('EffectCreator', () => {
  test('Creates effects', async () => {
    const doc = {
      createEmbeddedDocuments: vi.fn(),
    }
    const context = {
      doc: writable(doc),
    }
    const { getByRole } = render(
      html`
      <${Fragment} context=${context}>
        <${EffectCreator} />
      </$>
      `
    )
    const link = getByRole('link') as HTMLAnchorElement
    expect(link).toBeInTheDocument()
    expect(link).toHaveTextContent('add')
    expect(link.firstChild?.nodeName).toBe('I')
    expect(link.firstChild).toHaveClass('fas fa-plus')

    await fireEvent.click(link)
    expect(doc.createEmbeddedDocuments).toHaveBeenCalledWith(
      'ActiveEffect',
      [
        {
          name: 'newEffect',
          icon: 'icons/svg/item-bag.svg',
        },
      ],
      {
        renderSheet: true,
      }
    )
  })
})
