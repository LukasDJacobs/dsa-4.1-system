import SkillRoll from '../../src/ui/skill-roll/SkillRoll.svelte'
import { render, fireEvent } from '@testing-library/svelte'
import userEvent from '@testing-library/user-event'

import { when } from 'jest-when'

import { BaseCharacter } from '../../src/module/model/character.js'
import { BaseSkill, Testable } from '../../src/module/model/properties.js'

describe('SkillRollDialog', () => {
  const callback = vi.fn()
  const character = {} as BaseCharacter
  const testAttributes = ['courage', 'courage', 'courage']
  const skill = {
    testAttributes,
  } as Testable<BaseSkill>
  const foundryApp = {
    close: vi.fn(),
    setPosition: vi.fn(),
  }

  const customModifier = {
    name: 'custom',
    mod: 0,
    modifierType: 'other',
  }

  beforeEach(() => {
    callback.mockReset()
  })

  test('Submit without changes', async () => {
    const { getByTestId } = render(SkillRoll, {
      callback,
      character,
      skill,
      foundryApp,
    })
    const submitButton = getByTestId('submit-button')
    await fireEvent.click(submitButton)
    expect(callback).toHaveBeenCalledWith([customModifier], 0, testAttributes)
    expect(foundryApp.close).toHaveBeenCalled()
  })

  test('Submit with other test attributes', async () => {
    const { getByTestId } = render(SkillRoll, {
      callback,
      character,
      skill,
      foundryApp,
    })
    const submitButton = getByTestId('submit-button')
    const selectFirstTestAttribute = getByTestId('test-attribute-0')
    await userEvent.selectOptions(selectFirstTestAttribute, 'strength')

    await fireEvent.click(submitButton)
    const expectedTestAttributes = ['strength', 'courage', 'courage']
    expect(callback).toHaveBeenCalledWith(
      [customModifier],
      0,
      expectedTestAttributes
    )
    expect(foundryApp.close).toHaveBeenCalled()
  })

  test('Submit with changed modifier', async () => {
    const { getByTestId } = render(SkillRoll, {
      callback,
      character,
      skill,
      foundryApp,
    })
    const submitButton = getByTestId('submit-button')
    const modifierInput = getByTestId('modifier-input')
    const expectedModifier = 5
    await userEvent.type(modifierInput, expectedModifier.toString())

    await fireEvent.click(submitButton)
    const expectedCustomModifier = {
      name: 'custom',
      mod: expectedModifier,
      modifierType: 'other',
    }
    expect(callback).toHaveBeenCalledWith(
      [expectedCustomModifier],
      expectedModifier,
      testAttributes
    )
    expect(foundryApp.close).toHaveBeenCalled()
  })

  test('Hide effective encumbarance for other skills', async () => {
    const skill = {
      skillType: 'spell',
      testAttributes,
    }

    const { queryByTestId } = render(SkillRoll, {
      callback,
      character,
      skill,
      foundryApp,
    })
    const effectiveEncumbranceCheckbox = queryByTestId(
      'effective-encumbarance-checkbox'
    )
    expect(effectiveEncumbranceCheckbox).not.toBeInTheDocument()
  })

  test('Submit with effective encumbarance', async () => {
    const effectiveEncumbrance = 5
    const skill = {
      skillType: 'talent',
      talentType: 'normal',
      effectiveEncumbarance: {
        formula: 'BE',
      },
      testAttributes,
    }
    const character = {
      effectiveEncumbarance: vi.fn().mockReturnValue(effectiveEncumbrance),
    } as unknown as BaseCharacter

    const { getByTestId } = render(SkillRoll, {
      callback,
      character,
      skill,
      foundryApp,
    })
    const submitButton = getByTestId('submit-button')
    const effectiveEncumbranceCheckbox = getByTestId(
      'effective-encumbarance-checkbox'
    )
    await userEvent.click(effectiveEncumbranceCheckbox)

    await fireEvent.click(submitButton)
    const encumbaranceModifier = {
      name: 'effectiveEncumbarance',
      mod: effectiveEncumbrance,
      modifierType: 'other',
    }
    expect(callback).toHaveBeenCalledWith(
      [customModifier, encumbaranceModifier],
      effectiveEncumbrance,
      testAttributes
    )
    expect(foundryApp.close).toHaveBeenCalled()
  })

  test('Submit with degree modifier', async () => {
    const skill = {
      skillType: 'liturgy',
      degree: 'III',
      testAttributes,
    }
    const expectedModifier = 4
    const character = {
      degreeModifier: vi.fn().mockReturnValue(expectedModifier),
    } as unknown as BaseCharacter

    when(character.degreeModifier)
      .calledWith(skill.degree)
      .mockReturnValue(expectedModifier)

    const { getByTestId } = render(SkillRoll, {
      callback,
      character,
      skill,
      foundryApp,
    })
    const submitButton = getByTestId('submit-button')
    const degreeInput = getByTestId('test-degree')
    await userEvent.selectOptions(degreeInput, skill.degree)

    await fireEvent.click(submitButton)
    const degreeModifier = {
      name: 'degreeModifier',
      mod: expectedModifier,
      modifierType: 'other',
    }
    expect(callback).toHaveBeenCalledWith(
      [customModifier, degreeModifier],
      expectedModifier,
      testAttributes
    )
    expect(foundryApp.close).toHaveBeenCalled()
  })
})
