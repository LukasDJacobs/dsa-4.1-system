import { when } from 'jest-when'

import {
  SweepRule,
  SweepManeuver,
  Sweep,
} from '../../../../src/module/ruleset/rules/maneuvers/sweep.js'

import type { BaseCharacter } from '../../../../src/module/model/character.js'
import { Computation } from '../../../../src/module/ruleset/rule-components.js'
import { createTestRuleset } from '../helpers.js'
import { ComputeManeuverList } from '../../../../src/module/ruleset/rules/maneuvers/basic-maneuver.js'
import type { ManeuverType } from '../../../../src/module/model/modifier.js'

describe('Sweep', function () {
  const ruleset = createTestRuleset()

  ruleset.registerComputation(
    new Computation(
      ComputeManeuverList,
      vi.fn().mockImplementation(() => ({ maneuvers: [] }))
    )
  )

  ruleset.add(SweepRule)
  ruleset.compileRules()

  const character = {} as BaseCharacter

  it('should have a minimal modifier of 4', function () {
    expect(SweepManeuver.minMod).toEqual(4)
  })

  test.each([
    [false, 'offensive', false],
    [false, 'defensive', false],
    [false, 'defensive', true],
    [true, 'offensive', true],
  ] as [boolean, ManeuverType, boolean][])(
    'should add Sweep only if the character has the ability and the list is for offensive maneuvers',
    function (
      expected: boolean,
      maneuverType: ManeuverType,
      hasAbility: boolean
    ) {
      character.has = vi.fn()
      when(character.has).calledWith(Sweep).mockReturnValue(hasAbility)

      const result = ruleset.compute(ComputeManeuverList, {
        character,
        maneuverType,
      })

      expect(result.maneuvers.includes(SweepManeuver)).toEqual(expected)
    }
  )
})
