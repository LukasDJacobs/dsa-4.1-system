import { when } from 'jest-when'

import {
  DefensiveStyleRule,
  DefensiveStyleManeuver,
  DefensiveStyle,
} from '../../../../src/module/ruleset/rules/maneuvers/defensive-style.js'

import type { BaseCharacter } from '../../../../src/module/model/character.js'
import { Computation } from '../../../../src/module/ruleset/rule-components.js'
import { createTestRuleset } from '../helpers.js'
import { ComputeManeuverList } from '../../../../src/module/ruleset/rules/maneuvers/basic-maneuver.js'
import type { ManeuverType } from '../../../../src/module/model/modifier.js'

describe('DefensiveStyle', function () {
  const ruleset = createTestRuleset()

  ruleset.registerComputation(
    new Computation(
      ComputeManeuverList,
      vi.fn().mockImplementation(() => ({ maneuvers: [] }))
    )
  )

  ruleset.add(DefensiveStyleRule)
  ruleset.compileRules()

  const character = {} as BaseCharacter

  test.each([
    [false, 'offensive', false],
    [false, 'defensive', false],
    [true, 'defensive', true],
    [false, 'offensive', true],
  ] as [boolean, ManeuverType, boolean][])(
    'should add defensive style only if the character has the ability and the list is for defensive maneuvers',
    function (
      expected: boolean,
      maneuverType: ManeuverType,
      hasAbility: boolean
    ) {
      character.has = vi.fn()
      when(character.has).calledWith(DefensiveStyle).mockReturnValue(hasAbility)

      const result = ruleset.compute(ComputeManeuverList, {
        character,
        maneuverType,
      })

      expect(result.maneuvers.includes(DefensiveStyleManeuver)).toEqual(
        expected
      )
    }
  )
})
