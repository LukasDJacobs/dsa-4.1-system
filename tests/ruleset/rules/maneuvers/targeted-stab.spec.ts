import { when } from 'jest-when'

import {
  TargetedStabRule,
  TargetedStabManeuver,
  TargetedStab,
} from '../../../../src/module/ruleset/rules/maneuvers/targeted-stab.js'

import type { BaseCharacter } from '../../../../src/module/model/character.js'
import { Computation } from '../../../../src/module/ruleset/rule-components.js'
import { createTestRuleset } from '../helpers.js'
import { ComputeManeuverList } from '../../../../src/module/ruleset/rules/maneuvers/basic-maneuver.js'
import type { ManeuverType } from '../../../../src/module/model/modifier.js'

describe('TargetedStab', function () {
  const ruleset = createTestRuleset()

  ruleset.registerComputation(
    new Computation(
      ComputeManeuverList,
      vi.fn().mockImplementation(() => ({ maneuvers: [] }))
    )
  )

  ruleset.add(TargetedStabRule)
  ruleset.compileRules()

  const character = {} as BaseCharacter

  it('should have a minimal modifier of 4', function () {
    expect(TargetedStabManeuver.minMod).toEqual(4)
  })

  test.each([
    [false, 'offensive', false],
    [false, 'defensive', false],
    [false, 'defensive', true],
    [true, 'offensive', true],
  ] as [boolean, ManeuverType, boolean][])(
    'should add TargetedStab only if the character has the ability and the list is for offensive maneuvers',
    function (
      expected: boolean,
      maneuverType: ManeuverType,
      hasAbility: boolean
    ) {
      character.has = vi.fn()
      when(character.has).calledWith(TargetedStab).mockReturnValue(hasAbility)

      const result = ruleset.compute(ComputeManeuverList, {
        character,
        maneuverType,
      })

      expect(result.maneuvers.includes(TargetedStabManeuver)).toEqual(expected)
    }
  )
})
