import {
  FeintRule,
  FeintManeuver,
} from '../../../../src/module/ruleset/rules/maneuvers/feint.js'

import type { BaseCharacter } from '../../../../src/module/model/character.js'
import { Computation } from '../../../../src/module/ruleset/rule-components.js'
import { createTestRuleset } from '../helpers.js'
import { ComputeManeuverList } from '../../../../src/module/ruleset/rules/maneuvers/basic-maneuver.js'

describe('Feint', function () {
  const ruleset = createTestRuleset()

  ruleset.registerComputation(
    new Computation(
      ComputeManeuverList,
      vi.fn().mockImplementation(() => ({ maneuvers: [] }))
    )
  )

  ruleset.add(FeintRule)
  ruleset.compileRules()

  const character = {} as BaseCharacter

  it('should add feint to the list of offensive maneuvers', function () {
    const result = ruleset.compute(ComputeManeuverList, {
      character,
      maneuverType: 'offensive',
    })

    expect(result.maneuvers.includes(FeintManeuver)).toEqual(true)
  })

  it('should not add feint to the list of defensive maneuvers', function () {
    const result = ruleset.compute(ComputeManeuverList, {
      character,
      maneuverType: 'defensive',
    })

    expect(result.maneuvers.includes(FeintManeuver)).toEqual(false)
  })
})
