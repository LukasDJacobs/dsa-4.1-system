import {
  CombatComputationData,
  CombatComputationResult,
  ComputeArmorClass,
  ComputeAttack,
  ComputeDamageFormula,
  ComputeDodge,
  ComputeEffectiveEncumbarance,
  ComputeEncumbarance,
  ComputeParry,
  ComputeRangedAttack,
  DerivedCombatAttributesRule,
  RangeClass,
  SizeClass,
} from '../../../src/module/ruleset/rules/derived-combat-attributes.js'
import type { CombatTalent } from '../../../src/module/model/properties.js'
import { when } from 'jest-when'
import type {
  BaseCharacter,
  CharacterDataAccessor,
} from '../../../src/module/model/character.js'
import type { Armor, Weapon } from '../../../src/module/model/items.js'
import { createTestRuleset } from './helpers.js'
import { ManeuverDescriptor } from '../../../src/module/model/modifier.js'
import { ComputationIdentifier } from '../../../src/module/ruleset/rule-components.js'

describe('DerivedCombatAttributes', function () {
  let character: BaseCharacter

  beforeEach(() => {
    character = {
      data: {} as CharacterDataAccessor,
    } as BaseCharacter
  })

  const ruleset = createTestRuleset()

  ruleset.add(DerivedCombatAttributesRule)
  ruleset.compileRules()
  const combatTalent = {} as CombatTalent

  it('should provide computation for attack values', function () {
    const baseAttack = 8
    character.baseAttack = baseAttack

    const attackMod = 5
    combatTalent.attackMod = attackMod

    const expectedAttack = 13

    const result = ruleset.compute(ComputeAttack, {
      character,
      talent: combatTalent,
    })

    expect(result.value).toEqual(expectedAttack)
  })

  it('should provide computation for parry values', function () {
    const baseParry = 9
    character.baseParry = baseParry

    const parryMod = 5
    combatTalent.parryMod = parryMod
    const expectedParry = 14

    const result = ruleset.compute(ComputeParry, {
      character,
      talent: combatTalent,
    })

    expect(result.value).toEqual(expectedParry)
  })

  it('should provide computation for ranged attack values', function () {
    const baseRangedAttack = 10
    character.baseRangedAttack = baseRangedAttack

    const rangedAttackMod = 5
    combatTalent.rangedAttackMod = rangedAttackMod
    const expectedRangedAttack = 15

    const result = ruleset.compute(ComputeRangedAttack, {
      character,
      talent: combatTalent,
      rangeClass: RangeClass.Medium,
      sizeClass: SizeClass.Medium,
    })

    expect(result.value).toEqual(expectedRangedAttack)
  })

  it('should provide computation for the damage formula of a weapon', function () {
    const weapon = {} as Weapon

    const formula = '1d6+7'
    weapon.damage = formula

    const result = ruleset.compute(ComputeDamageFormula, {
      character,
      weapon,
    })

    expect(result.formula).toEqual(formula)
  })

  test.each([2, 5])(
    'should provide computation for the damage formula of a weapon with damage multiplier',
    function (damageMultiplier: number) {
      const weapon = {} as Weapon

      const baseDamage = '1d6+7'
      const formula = `${damageMultiplier}*(${baseDamage})`
      weapon.damage = baseDamage

      const result = ruleset.compute(ComputeDamageFormula, {
        character,
        weapon,
        damageMultiplier,
      })

      expect(result.formula).toEqual(formula)
    }
  )

  test.each([2, 5])(
    'should provide computation for the damage formula of a weapon with bonus damage',
    function (bonusDamage: number) {
      const weapon = {} as Weapon

      const baseDamage = '1d6+7'
      const formula = `${baseDamage} + ${bonusDamage}`

      weapon.damage = baseDamage

      const result = ruleset.compute(ComputeDamageFormula, {
        character,
        weapon,
        bonusDamage,
      })

      expect(result.formula).toEqual(formula)
    }
  )

  it('should provide computation for the damage formula of a weapon with bonus damage and damage multiplier', function () {
    const weapon = {} as Weapon

    const bonusDamage = 8
    const damageMultiplier = 4

    const baseDamage = '1d6+7'
    const formula = `${damageMultiplier}*(${baseDamage}) + ${bonusDamage}`

    weapon.damage = baseDamage

    const result = ruleset.compute(ComputeDamageFormula, {
      character,
      weapon,
      bonusDamage,
      damageMultiplier,
    })

    expect(result.formula).toEqual(formula)
  })

  it('should provide computation for dodge values', function () {
    const dodge = 11
    character.baseDodge = dodge

    const result = ruleset.compute(ComputeDodge, {
      character,
      talent: combatTalent,
    })

    expect(result.value).toEqual(dodge)
  })

  it('should provide attack computation which translates a wepaon into a combat talent', function () {
    const weapon = {} as Weapon

    const talentSID = 'talent-swords'
    weapon.talent = talentSID
    const baseAttack = 8
    character.baseAttack = baseAttack
    character.talent = vi.fn()
    when(character.talent).calledWith(talentSID).mockReturnValue(combatTalent)

    const attackMod = 5
    combatTalent.attackMod = attackMod
    const expectedAttack = 13

    const result = ruleset.compute(ComputeAttack, {
      character,
      weapon: weapon,
    })

    expect(result.value).toEqual(expectedAttack)
  })

  it('should  provide parry computation which translates a wepaon into a combat talent', function () {
    const weapon = {} as Weapon

    const talentSID = 'talent-swords'
    weapon.talent = talentSID
    const baseParry = 8
    character.baseParry = baseParry
    character.talent = vi.fn()
    when(character.talent).calledWith(talentSID).mockReturnValue(combatTalent)

    const parryMod = 5
    combatTalent.parryMod = parryMod
    const expectedParry = 13

    const result = ruleset.compute(ComputeParry, {
      character,
      weapon: weapon,
    })

    expect(result.value).toEqual(expectedParry)
  })

  it('should provide ranged attack computation which translates a wepaon into a combat talent', function () {
    const weapon = {} as Weapon

    const talentSID = 'talent-swords'
    weapon.talent = talentSID

    const baseRangedAttack = 8
    character.baseRangedAttack = baseRangedAttack
    character.talent = vi.fn()
    when(character.talent).calledWith(talentSID).mockReturnValue(combatTalent)

    const rangedAttackMod = 5
    combatTalent.rangedAttackMod = rangedAttackMod
    const expectedRangedAttack = 13

    const result = ruleset.compute(ComputeRangedAttack, {
      character,
      weapon: weapon,
      rangeClass: RangeClass.Medium,
      sizeClass: SizeClass.Medium,
    })

    expect(result.value).toEqual(expectedRangedAttack)
  })

  it('should provide an attack computation which returns zero if an invalid talent is provided', function () {
    const expectedAttack = 0

    const result = ruleset.compute(ComputeAttack, {
      character,
      talent: undefined,
    })

    expect(result.value).toEqual(expectedAttack)
  })

  it('should provide an parry computation which returns zero if an invalid talent is provided', function () {
    const expectedParry = 0

    const result = ruleset.compute(ComputeParry, {
      character,
      talent: undefined,
    })

    expect(result.value).toEqual(expectedParry)
  })

  it('should provide an ranged attack computation which returns zero if an invalid talent is provided', function () {
    const expectedRangedAttack = 0

    const result = ruleset.compute(ComputeRangedAttack, {
      character,
      talent: undefined,
      rangeClass: RangeClass.Medium,
      sizeClass: SizeClass.Medium,
    })

    expect(result.value).toEqual(expectedRangedAttack)
  })

  it('encumbarance value should be zero without any armor', function () {
    const expectedEncumbarance = 0

    const result = ruleset.compute(ComputeEncumbarance, {
      character,
    })

    expect(result).toEqual(expectedEncumbarance)
  })

  test.each([
    ['BE', 2, 2],
    ['BE–1', 2, 1],
    ['BE–3', 2, 0],
    ['BE+1', 2, 3],
    ['BEx2', 2, 4],
    [null, 2, 0],
  ])(
    'should be able to compute the effective encumbarance based on a formula',
    function (
      formula: string | null,
      encumbarance: number,
      expectedEncumbarance: number
    ) {
      character.encumbarance = encumbarance

      const result = ruleset.compute(ComputeEffectiveEncumbarance, {
        character,
        formula,
      })

      expect(result).toEqual(expectedEncumbarance)
    }
  )

  it('armor class value should be zero without any armor', function () {
    const expectedArmorClass = 0

    const result = ruleset.compute(ComputeArmorClass, {
      character,
    })

    expect(result).toEqual(expectedArmorClass)
  })

  it('encumbarance value should equal the armors encumbarance', function () {
    const encumbarance = 2
    character.armorItems = [
      {
        name: 'Kettenhemd',
        armorClass: 2,
        encumbarance,
      } as Armor,
    ]
    const expectedEncumbarance = 2

    const result = ruleset.compute(ComputeEncumbarance, {
      character,
    })

    expect(result).toEqual(expectedEncumbarance)
  })

  it('armor class value should equal the armors armor class', function () {
    const armorClass = 2
    character.armorItems = [
      {
        name: 'Kettenhemd',
        armorClass: armorClass,
        encumbarance: 3,
      } as Armor,
    ]
    const expectedArmorClass = 2

    const result = ruleset.compute(ComputeArmorClass, {
      character,
    })

    expect(result).toEqual(expectedArmorClass)
  })

  it('encumbarance value should equal the sum of the armors encumbarances', function () {
    const encumbaranceArmor = 2
    const encumbaranceHelm = 1
    character.armorItems = [
      {
        name: 'Kettenhemd',
        armorClass: 2,
        encumbarance: encumbaranceArmor,
      } as Armor,
      {
        name: 'Helm',
        armorClass: 1,
        encumbarance: encumbaranceHelm,
      } as Armor,
    ]
    const expectedEncumbarance = 3

    const result = ruleset.compute(ComputeEncumbarance, {
      character,
    })

    expect(result).toEqual(expectedEncumbarance)
  })

  it('armor class value should equal the sum of the armors armor classes', function () {
    const armorClassArmor = 2
    const armorClassHelm = 1
    character.armorItems = [
      {
        name: 'Kettenhemd',
        armorClass: armorClassArmor,
        encumbarance: 3,
      } as Armor,
      {
        name: 'Helm',
        armorClass: armorClassHelm,
        encumbarance: 2,
      } as Armor,
    ]
    const expectedArmorClass = 3

    const result = ruleset.compute(ComputeArmorClass, {
      character,
    })

    expect(result).toEqual(expectedArmorClass)
  })

  test.each([ComputeAttack, ComputeParry, ComputeRangedAttack])(
    'should add maneuvers to modifiers',
    function (
      computeAction: ComputationIdentifier<
        CombatComputationData,
        CombatComputationResult
      >
    ) {
      const weapon = {} as Weapon

      const maneuvers: ManeuverDescriptor[] = [
        {
          name: 'offensive',
          mod: 2,
          type: 'offensive',
          minMod: 0,
          modifierType: 'maneuver',
        },
        {
          name: 'defensive',
          mod: 2,
          type: 'defensive',
          minMod: 0,
          modifierType: 'maneuver',
        },
      ]

      const result = ruleset.compute(computeAction, {
        character,
        weapon,
        talent: combatTalent,
        maneuvers,
      })

      expect(result.modifiers).toEqual(maneuvers)
    }
  )
})
