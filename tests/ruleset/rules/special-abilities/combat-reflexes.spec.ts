import { when } from 'jest-when'
import {
  CombatReflexes,
  CombatReflexesRule,
} from '../../../../src/module/ruleset/rules/special-abilities/combat-reflexes.js'

import type { BaseCharacter } from '../../../../src/module/model/character.js'
import { Computation } from '../../../../src/module/ruleset/rule-components.js'
import { createTestRuleset } from '../helpers.js'
import { ComputeInitiative } from '../../../../src/module/ruleset/rules/basic-combat.js'

describe('Combat reflexes', function () {
  const ruleset = createTestRuleset()
  const initiativeValue = 10

  const executeHook = vi.fn().mockReturnValue(initiativeValue)
  ruleset.registerComputation(new Computation(ComputeInitiative, executeHook))

  ruleset.add(CombatReflexesRule)
  ruleset.compileRules()

  const character = {} as BaseCharacter

  it('should add 4 to the initiative if the character has combat reflexes', async function () {
    character.has = vi.fn()

    const options = {
      character,
    }

    when(character.has).calledWith(CombatReflexes).mockReturnValue(true)

    const result = ruleset.compute(ComputeInitiative, options)

    expect(result).toEqual(14)
  })

  it('should not change the initiative if the character has no combat reflexes', async function () {
    character.has = vi.fn()

    const options = {
      character,
    }

    when(character.has).calledWith(CombatReflexes).mockReturnValue(false)

    const result = ruleset.compute(ComputeInitiative, options)

    expect(result).toEqual(10)
  })
})
