import {
  ComputeAttack,
  ComputeParry,
} from '../../../src/module/ruleset/rules/derived-combat-attributes.js'
import { BasicShieldCombatRule } from '../../../src/module/ruleset/rules/basic-shield-combat.js'
import type { CombatTalent } from '../../../src/module/model/properties.js'
import type {
  BaseCharacter,
  CharacterDataAccessor,
} from '../../../src/module/model/character.js'
import type { Shield } from '../../../src/module/model/items.js'
import { createTestRuleset } from './helpers.js'
import { Computation } from '../../../src/module/ruleset/rule-components.js'

describe('BasicShieldCombatRule', function () {
  const character = {
    data: {} as CharacterDataAccessor,
  } as BaseCharacter

  const ruleset = createTestRuleset()

  const computeAttack = vi.fn()
  ruleset.registerComputation(new Computation(ComputeAttack, computeAttack))

  const computeParry = vi.fn()
  ruleset.registerComputation(new Computation(ComputeParry, computeParry))

  ruleset.add(BasicShieldCombatRule)
  ruleset.compileRules()

  const combatTalent = {} as CombatTalent
  const shield = {} as Shield

  it('should add shield modifiers to the attack value', function () {
    const baseAttack = 8
    const attackMod = 5

    computeAttack.mockReturnValue({ value: baseAttack + attackMod })

    const shieldAttackMod = -3
    shield.weaponMod = {
      attack: shieldAttackMod,
      parry: 0,
    }

    const expectedAttack = 10

    const result = ruleset.compute(ComputeAttack, {
      character,
      talent: combatTalent,
      shield,
    })

    expect(result.value).toEqual(expectedAttack)
  })

  it('should use shield modifiers for the parry value', function () {
    const baseParry = 8
    character.baseParry = baseParry

    const parryMod = 5
    combatTalent.parryMod = parryMod
    computeParry.mockReturnValue({ value: baseParry + parryMod })

    const shieldParryMod = 3
    shield.weaponMod = {
      attack: 0,
      parry: shieldParryMod,
    }

    const expectedParry = 11

    const result = ruleset.compute(ComputeParry, {
      character,
      talent: combatTalent,
      shield,
    })

    expect(result.value).toEqual(expectedParry)
  })
})
