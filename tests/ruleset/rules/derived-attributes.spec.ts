import {
  ComputeBaseAttack,
  ComputeBaseInitiative,
  ComputeBaseParry,
  ComputeBaseRangedAttack,
  ComputeMagicResistance,
  ComputeWoundThresholds,
  DerivedAttributesRule,
} from '../../../src/module/ruleset/rules/derived-attributes.js'
import { when } from 'jest-when'
import type {
  BaseCharacter,
  CharacterDataAccessor,
} from '../../../src/module/model/character.js'
import { createTestRuleset } from './helpers.js'
import { AttributeName } from '../../../src/module/model/character-data.js'

function mockAttributes(
  character: BaseCharacter,
  attributes: Partial<Record<AttributeName, number>>
) {
  Object.entries(attributes).forEach(([attribute, value]) =>
    when(character.data.attribute)
      .calledWith(attribute)
      .mockReturnValue({ value })
  )
}

describe('DerivedCombatAttributes', function () {
  let character: BaseCharacter

  beforeEach(() => {
    character = {
      data: {} as CharacterDataAccessor,
    } as BaseCharacter
    character.data.attribute = vi.fn()
  })

  const ruleset = createTestRuleset()

  ruleset.add(DerivedAttributesRule)
  ruleset.compileRules()

  test.each([
    [15, 15, 15, 9],
    [14, 15, 15, 9],
    [12, 15, 15, 8],
    [11, 15, 15, 8],
  ])(
    'should provide computation for base attack values',
    function (courage, agility, strength, expected) {
      const attributes = {
        courage,
        agility,
        strength,
      }
      mockAttributes(character, attributes)

      const result = ruleset.compute(ComputeBaseAttack, {
        character,
      })

      expect(result).toEqual(expected)
    }
  )

  test.each([
    [15, 15, 15, 9],
    [14, 15, 15, 9],
    [12, 15, 15, 8],
    [11, 15, 15, 8],
  ])(
    'should provide computation for base parry values',
    function (intuition, agility, strength, expected) {
      const attributes = {
        intuition,
        agility,
        strength,
      }
      mockAttributes(character, attributes)

      const result = ruleset.compute(ComputeBaseParry, {
        character,
      })

      expect(result).toEqual(expected)
    }
  )

  test.each([
    [15, 15, 15, 9],
    [14, 15, 15, 9],
    [12, 15, 15, 8],
    [11, 15, 15, 8],
  ])(
    'should provide computation for base ranged attack values',
    function (intuition, dexterity, strength, expected) {
      const attributes = {
        intuition,
        dexterity,
        strength,
      }
      mockAttributes(character, attributes)

      const result = ruleset.compute(ComputeBaseRangedAttack, {
        character,
      })

      expect(result).toEqual(expected)
    }
  )

  test.each([
    [15, 15, 15, 12],
    [14, 15, 15, 12],
    [13, 15, 15, 11],
    [15, 13, 15, 12],
    [15, 12, 15, 11],
  ])(
    'should provide computation for base initiative values',
    function (courage, intuition, agility, expected) {
      const attributes = {
        courage,
        intuition,
        agility,
      }
      mockAttributes(character, attributes)

      const result = ruleset.compute(ComputeBaseInitiative, {
        character,
      })

      expect(result).toEqual(expected)
    }
  )

  test.each([
    [15, 15, 15, 9],
    [14, 15, 15, 9],
    [12, 15, 15, 8],
    [11, 15, 15, 8],
  ])(
    'should provide computation for magic resistance values',
    function (courage, cleverness, constitution, expected) {
      const attributes = {
        courage,
        cleverness,
        constitution,
      }
      mockAttributes(character, attributes)

      const result = ruleset.compute(ComputeMagicResistance, {
        character,
      })

      expect(result).toEqual(expected)
    }
  )

  test.each([
    [15, 0, [8, 15, 23, 0]],
    [12, 0, [6, 12, 18, 0]],
    [13, 1, [7, 13, 20, 1]],
    [15, 2, [8, 15, 23, 2]],
  ])(
    'should provide computation for wound thresholds',
    function (constitution, mod, expected) {
      const attributes = {
        constitution,
      }
      mockAttributes(character, attributes)

      const result = ruleset.compute(ComputeWoundThresholds, {
        character,
        mod,
      })

      expect(result).toEqual({
        first: expected[0],
        second: expected[1],
        third: expected[2],
        mod: expected[3],
      })
    }
  )
})
