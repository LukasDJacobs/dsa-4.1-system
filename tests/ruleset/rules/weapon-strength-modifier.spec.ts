import {
  ComputeDamageFormula,
  createDamageFormula,
} from '../../../src/module/ruleset/rules/derived-combat-attributes.js'
import { WeaponStrengthModifierRule } from '../../../src/module/ruleset/rules/weapon-strength-modifier.js'
import { when } from 'jest-when'
import type { BaseCharacter } from '../../../src/module/model/character.js'
import type { MeleeWeapon } from '../../../src/module/model/items.js'
import { createTestRuleset } from './helpers.js'
import { Computation } from '../../../src/module/ruleset/rule-components.js'
import { NamedAttribute } from '../../../src/module/model/properties.js'

describe('WeaponStrengthModifier', function () {
  const character = {} as BaseCharacter

  const ruleset = createTestRuleset()

  const computeDamage = vi.fn()
  ruleset.registerComputation(
    new Computation(ComputeDamageFormula, computeDamage)
  )

  ruleset.add(WeaponStrengthModifierRule)
  ruleset.compileRules()

  const weapon = {} as MeleeWeapon

  const strengthAttribute = {} as NamedAttribute

  const strength = 15
  strengthAttribute.value = strength
  character.attribute = vi.fn()
  when(character.attribute)
    .calledWith('strength')
    .mockReturnValue(strengthAttribute)

  const baseDamage = '1d6 + 3'
  weapon.damage = baseDamage
  weapon.type = 'melee'

  beforeEach(() => {
    computeDamage.mockReturnValue(createDamageFormula(baseDamage))
  })

  test.each([
    [13, 2, '1d6 + 3 + 1'],
    [14, 2, '1d6 + 3'],
    [16, 2, '1d6 + 3'],
    [17, 2, '1d6 + 3 + -1'],
  ])(
    'should add weapon strength modifier to damage',
    function (threshold, hitPointStep, expectedDamage) {
      const strengthMod = {
        threshold,
        hitPointStep,
      }
      weapon.strengthMod = strengthMod

      const result = ruleset.compute(ComputeDamageFormula, {
        character,
        weapon,
      })

      expect(result.formula).toEqual(expectedDamage)
    }
  )
})
