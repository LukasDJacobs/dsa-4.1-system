import { when } from 'jest-when'
import { Ruleset } from '../../src/module/ruleset/ruleset.js'
import {
  Computation,
  CreateActionIdentifier,
  CreateComputationIdentifier,
  CreateEffectIdentifier,
} from '../../src/module/ruleset/rule-components.js'
import type {
  BaseActionOptionData,
  BaseComputationOptionData,
} from '../../src/module/ruleset/rule-components.js'
import type {
  Rule,
  RuleConfigManager,
  RuleCreator,
  RuleDescriptor,
} from '../../src/module/ruleset/rule.js'
import { TestAction, TestEffect } from './test-classes.js'
import type { TestEffectListenedResult } from './test-classes.js'

describe('Ruleset', function () {
  const configManager = {} as RuleConfigManager

  const actionIdentifier = CreateActionIdentifier<
    BaseActionOptionData & Record<any, any>,
    any
  >('test')

  const computationIdentifier = CreateComputationIdentifier<
    BaseComputationOptionData & Record<any, any>,
    unknown
  >('test')

  it('should be able to call a registered computation and an associated hook', function () {
    const ruleset = new Ruleset(configManager)
    const options = {
      character: undefined as any,
      paramter: 8,
      intermediateResult: 13,
      result: 5,
    }
    const executeMock = (Computation.prototype.execute = vi.fn())
    when(executeMock).calledWith(options).mockReturnValue(options.result)
    const computation = new Computation(computationIdentifier, undefined as any)
    ruleset.registerComputation(computation)
    const result = ruleset.compute(computationIdentifier, options)
    expect(result).toEqual(options.result)
  })

  it('should be able to execute a registered action with an hook', async function () {
    const ruleset = new Ruleset(configManager)

    const options = {
      someOption: 'value',
    }
    const executionResult = {
      options: {
        action: actionIdentifier,
        someOption: 'value',
      },
      resultValue: 'value',
    }
    const expectedResult = {
      options: {
        action: actionIdentifier,
        someOption: 'value',
      },
      resultValue: 'result',
    }
    const action = new TestAction(actionIdentifier)
    ruleset.registerAction(action)
    const hook = vi.fn()
    when(hook)
      .calledWith(executionResult.options, executionResult)
      .mockReturnValue(expectedResult)
    ruleset.registerActionPostHook(actionIdentifier, hook)
    const result = await ruleset.execute(actionIdentifier, options)
    expect(result).toEqual(expectedResult)
  })

  test('should apply valid effects with a pre hook after executing an action', async function () {
    const ruleset = new Ruleset(configManager)
    const effectName = 'chat'
    const effectIdentifier = CreateEffectIdentifier(effectName)
    const hook = vi.fn()
    hook.mockImplementation((result) => result)
    const callback = vi.fn()
    const options = {
      someOption: 'value',
      callback,
    }
    const action = new TestAction(actionIdentifier)
    const effect = new TestEffect(effectIdentifier)
    ruleset.registerAction(action)
    ruleset.registerEffect<TestEffectListenedResult, TestEffect>(effect)
    ruleset.registerEffectPreHook(effectIdentifier, hook)
    ruleset.registerActionResultListener(actionIdentifier, effectIdentifier)
    await ruleset.execute(actionIdentifier, options)
    expect(callback).toHaveBeenCalled()
    expect(hook).toHaveBeenCalled()
  })

  it('should set itself as a ruleset to a new action', function () {
    const ruleset = new Ruleset(configManager)
    const actionName = 'someAction'
    const actionIdentifier = CreateActionIdentifier(actionName)
    const action = new TestAction(actionIdentifier)
    ruleset.registerAction(action)
    expect(action.ruleset).toEqual(ruleset)
  })
})

describe('Ruleset Nice Interface', function () {
  const configManager = {} as RuleConfigManager

  const actionIdentifier = CreateActionIdentifier<
    BaseActionOptionData & Record<any, any>,
    any
  >('test')

  const computationIdentifier = CreateComputationIdentifier<
    BaseComputationOptionData & Record<any, any>,
    unknown
  >('test')

  it('should be able to call a registered computation and a post hook', function () {
    const ruleset = new Ruleset(configManager)
    const options = {
      character: undefined as any,
      paramter: 8,
      intermediateResult: 13,
      result: 5,
    }

    ruleset
      .on(computationIdentifier)
      .do((options: Record<any, any>) => options.intermediateResult)
    ruleset.after(computationIdentifier).do((options, result) => {
      if (result === options.intermediateResult) {
        return options.result
      }
    })

    const result = ruleset.compute(computationIdentifier, options)
    expect(result).toEqual(options.result)
  })

  it('should be able to execute a registered action with a post hook', async function () {
    const ruleset = new Ruleset(configManager)
    const options = {
      someOption: 'value',
    }
    const executionResult = {
      options: {
        action: actionIdentifier,
        someOption: 'value',
      },
      resultValue: 'value',
    }
    const expectedResult = {
      options: {
        action: actionIdentifier,
        someOption: 'value',
      },
      resultValue: 'result',
    }

    ruleset.on(actionIdentifier).do(TestAction)
    const hook = vi.fn()
    when(hook)
      .calledWith(executionResult.options, executionResult)
      .mockReturnValue(expectedResult)
    ruleset.after(actionIdentifier).do(hook)
    const result = await ruleset.execute(actionIdentifier, options)
    expect(result).toEqual(expectedResult)
  })

  test.each([false, true])(
    'should be able to execute a registered action with a post hook and one predicate',
    async function (pred) {
      const ruleset = new Ruleset(configManager)
      const origOption = 'value'
      const changedOption = 'anotherValue'
      const expectedOption = pred ? changedOption : origOption
      const options = {
        someOption: origOption,
      }
      const executionResult = {
        options: {
          action: actionIdentifier,
          someOption: origOption,
        },
        resultValue: origOption,
      }
      const expectedResult = {
        ...executionResult,
        resultValue: expectedOption,
      }

      ruleset.on(actionIdentifier).do(TestAction)
      const hook = vi.fn()
      when(hook)
        .calledWith(executionResult.options, executionResult)
        .mockReturnValue(expectedResult)
      ruleset
        .after(actionIdentifier)
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        .when((_options, _result) => pred)
        .do(hook)
      const result = await ruleset.execute(actionIdentifier, options)
      expect(result).toEqual(expectedResult)
    }
  )

  test.each([false, true])(
    'should be able to execute a registered action with a post hook and multiple predicates',
    async function (pred) {
      const ruleset = new Ruleset(configManager)
      const origOption = 'value'
      const changedOption = 'anotherValue'
      const expectedOption = pred ? changedOption : origOption
      const options = {
        someOption: origOption,
      }
      const executionResult = {
        options: {
          action: actionIdentifier,
          someOption: origOption,
        },
        resultValue: origOption,
      }
      const expectedResult = {
        ...executionResult,
        resultValue: expectedOption,
      }

      ruleset.on(actionIdentifier).do(TestAction)
      const hook = vi.fn()
      when(hook)
        .calledWith(executionResult.options, executionResult)
        .mockReturnValue(expectedResult)
      ruleset
        .after(actionIdentifier)
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        .when((_options, _result) => pred)
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        .when((_options, _result) => pred)
        .do(hook)
      const result = await ruleset.execute(actionIdentifier, options)
      expect(result).toEqual(expectedResult)
    }
  )

  it('should be able to execute a registered action with a pre hook', async function () {
    const ruleset = new Ruleset(configManager)
    const options = {}
    const preHookOptions = {
      action: actionIdentifier,
    }
    const executionResult = {
      action: actionIdentifier,
      someOption: 'value',
    }
    const expectedResult = {
      options: {
        action: actionIdentifier,
        someOption: 'value',
      },
      resultValue: 'value',
    }

    ruleset.on(actionIdentifier).do(TestAction)
    const hook = vi.fn()
    when(hook).calledWith(preHookOptions).mockReturnValue(executionResult)
    ruleset.before(actionIdentifier).do(hook)
    const result = await ruleset.execute(actionIdentifier, options)
    expect(result).toEqual(expectedResult)
  })

  test.each([true, false])(
    'should be able to execute a registered action with a pre hook and one predicate',
    async function (pred) {
      const ruleset = new Ruleset(configManager)
      const origOption = 'value'
      const changedOption = 'anotherValue'
      const options = {
        someOption: origOption,
      }
      const preHookOptions = {
        action: actionIdentifier,
        someOption: origOption,
      }
      const executionResult = {
        action: actionIdentifier,
        someOption: changedOption,
      }
      const expectedOption = pred ? changedOption : origOption
      const expectedResult = {
        options: {
          action: actionIdentifier,
          someOption: expectedOption,
        },
        resultValue: expectedOption,
      }

      ruleset.on(actionIdentifier).do(TestAction)
      const hook = vi.fn()
      when(hook).calledWith(preHookOptions).mockReturnValue(executionResult)
      ruleset
        .before(actionIdentifier)
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        .when((_options) => pred)
        .do(hook)
      const result = await ruleset.execute(actionIdentifier, options)
      expect(result).toEqual(expectedResult)
    }
  )

  test.each([true, false])(
    'should be able to execute a registered action with a pre hook and multiple predicates',
    async function (pred) {
      const ruleset = new Ruleset(configManager)
      const origOption = 'value'
      const changedOption = 'anotherValue'
      const options = {
        someOption: origOption,
      }
      const preHookOptions = {
        action: actionIdentifier,
        someOption: origOption,
      }
      const executionResult = {
        action: actionIdentifier,
        someOption: changedOption,
      }
      const expectedOption = pred ? changedOption : origOption
      const expectedResult = {
        options: {
          action: actionIdentifier,
          someOption: expectedOption,
        },
        resultValue: expectedOption,
      }

      ruleset.on(actionIdentifier).do(TestAction)
      const hook = vi.fn()
      when(hook).calledWith(preHookOptions).mockReturnValue(executionResult)
      ruleset
        .before(actionIdentifier)
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        .when((_options) => pred)
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        .when((_options) => pred)
        .do(hook)
      const result = await ruleset.execute(actionIdentifier, options)
      expect(result).toEqual(expectedResult)
    }
  )

  test('should apply valid effects after executing an action', async function () {
    const ruleset = new Ruleset(configManager)
    const effectName = 'chat'
    const effectIdentifier = CreateEffectIdentifier(effectName)
    const callback = vi.fn()
    const options = {
      someOption: 'value',
      callback,
    }
    const effect = new TestEffect(effectIdentifier)
    ruleset.registerEffect<TestEffectListenedResult, TestEffect>(effect)
    ruleset.on(actionIdentifier).do(TestAction)
    ruleset.after(actionIdentifier).trigger(effectIdentifier)

    await ruleset.execute(actionIdentifier, options)
    expect(callback).toHaveBeenCalled()
  })

  it('should set itself as a ruleset to a new action', function () {
    const ruleset = new Ruleset(configManager)
    const action = new TestAction(actionIdentifier)
    ruleset.registerAction(action)
    expect(action.ruleset).toEqual(ruleset)
  })

  it('should be able to add, configure and compile several rules', function () {
    const ruleset = new Ruleset(configManager)

    const createRuleMock = () =>
      ({
        createConfig: vi.fn(),
        loadConfig: vi.fn(),
        loadInto: vi.fn(),
      } as unknown as vi.Mocked<Rule>)

    const rule1 = createRuleMock()
    const rule2 = createRuleMock()
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const mockedRuleCreator1: RuleCreator = (_descriptor: RuleDescriptor) =>
      rule1
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const mockedRuleCreator2: RuleCreator = (_descriptor: RuleDescriptor) =>
      rule2

    ruleset.add({} as RuleDescriptor, mockedRuleCreator1)
    expect(rule1.createConfig).toHaveBeenCalledWith(configManager)
    expect(rule1.loadConfig).toHaveBeenCalled()
    // rule1.createConfig as vi.Mock<any, any>

    ruleset.add({} as RuleDescriptor, mockedRuleCreator2)
    ruleset.compileRules()
    expect(rule1.loadInto).toHaveBeenCalledWith(ruleset)
    expect(rule2.loadInto).toHaveBeenCalledWith(ruleset)
  })
})
