import {
  Computation,
  ChatEffect,
  Action,
  CreateActionIdentifier,
  CreateComputationIdentifier,
  CreateEffectIdentifier,
} from '../../src/module/ruleset/rule-components.js'
import { TestAction, TestEffect } from './test-classes.js'
import { when } from 'jest-when'

describe('Computation', function () {
  const name = 'attack'
  const functorResult = 5
  const param = {
    character: undefined,
    result: functorResult,
    increment: 0,
  }
  const functor = (options) => options.result
  const identifier = CreateComputationIdentifier(name)

  const computation = new Computation(identifier, functor)

  it('should have a name and a functor which can be executed', function () {
    expect(computation.identifier).toEqual(identifier)
    const result = computation.execute(param)
    expect(result).toEqual(functorResult)
  })

  it('should be able to use a hook which modifies options for the computation', function () {
    const expectedResult = 7
    const hook = (options) => {
      options.result = expectedResult
      return options
    }
    computation.addPreHook(hook)
    const result = computation.execute(param)
    expect(result).toEqual(expectedResult)
  })

  it('should be able to use a hook which resumes the computation', function () {
    param.increment = 6
    const expectedResult = 13
    const hook = (options, prev) => options.increment + prev
    computation.addPostHook(hook)
    const result = computation.execute(param)
    expect(result).toEqual(expectedResult)
  })
})

describe('Action', function () {
  const name = 'attack'
  const identifier = CreateActionIdentifier(name)

  let action: Action<any, any>
  const options = { someOption: 'value' }

  beforeEach(function () {
    action = new TestAction(identifier)
  })

  it('should have a name', function () {
    expect(action.identifier).toEqual(identifier)
  })

  it('should be executable with a set of options which are returned in the result', async function () {
    const expectedResult = {
      options,
      resultValue: options.someOption,
    }
    const result = await action.execute(options)
    expect(result).toEqual(expectedResult)
  })

  it('should be able to able to call a hook pre execution', async function () {
    const finalOptions = { someOption: 'value', anotherOption: 'anotherValue' }
    const expectedResult = {
      options: finalOptions,
      resultValue: options.someOption,
    }
    const hook = vi.fn()
    when(hook).calledWith(options).mockReturnValue(finalOptions)
    action.addPreHook(hook)
    const result = await action.execute(options)
    expect(result).toEqual(expectedResult)
  })

  it('should be able to able to call a hook post execution', async function () {
    const executionResult = {
      options: options,
      resultValue: options.someOption,
    }
    const expectedResult = {
      options: options,
      resultValue: 'anotherValue',
    }
    const hook = vi.fn()
    when(hook)
      .calledWith(options, executionResult)
      .mockReturnValue(expectedResult)
    action.addPostHook(hook)
    const result = await action.execute(options)
    expect(result).toEqual(expectedResult)
  })

  // it('should be able to have a custom rule manger', function () {
  //   const mockRuleManager = mock<RuleManager>()
  //   const ruleset = instance(mockRuleManager)
  //   action.ruleset = ruleset
  //   expect(action.ruleset).toEqual(ruleset)
  // })
})

describe('TestAction', function () {
  it('should be executed with pre and post hook', async function () {
    const name = 'attack'
    const identifier = CreateActionIdentifier(name)

    const action = new TestAction(identifier)
    const options = {
      someOption: 'value',
    }
    const finalOptions = { someOption: 'value', anotherOption: 'anotherValue' }
    const executionResult = {
      options: finalOptions,
      resultValue: options.someOption,
    }
    const expectedResult = {
      options: finalOptions,
      resultValue: options.someOption,
      additionalValue: 5,
    }
    const preHook = vi.fn()
    when(preHook).calledWith(options).mockReturnValue(finalOptions)
    action.addPreHook(preHook)
    const postHook = vi.fn()
    when(postHook)
      .calledWith(finalOptions, executionResult)
      .mockReturnValue(expectedResult)
    action.addPostHook(postHook)
    const result = await action.execute(options)
    expect(result).toEqual(expectedResult)
  })
})

describe('Effect', function () {
  const effectName = 'chat'
  const effectIdentifier = CreateEffectIdentifier(effectName)
  const actionName = 'attack'
  const actionIdentifier = CreateActionIdentifier(actionName)

  const actionResult = {
    action: actionIdentifier,
    options: {},
  }
  const effect = new TestEffect(effectIdentifier)

  it('should have a name', function () {
    expect(effect.identifier.name).toEqual(effectIdentifier.name)
  })

  it('should be appliable to a result', async function () {
    const result = await effect.apply(actionResult)
    expect(result).toEqual(true)
  })

  // it('should be applied to actions it listens to', function () {
  //   effect.listenTo(actionIdentifier)
  //   const result = effect.apply(actionResult)
  //   expect(result).toEqual(true)
  // })

  it('should be able to able to add a hook for pre execution', async function () {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    effect.addPreHook((_result: any) => undefined)
  })
})

describe('TestEffect', function () {
  const effectName = 'chat'
  const effectIdentifier = CreateEffectIdentifier(effectName)
  const actionName = 'attack'
  const actionIdentifier = CreateActionIdentifier(actionName)

  const validResult = {
    action: actionIdentifier,
    options: {
      callback: undefined,
    },
  }
  const effect = new TestEffect(effectIdentifier)

  beforeEach(function () {
    validResult.options.callback = vi.fn() as any
  })

  it('should call its callback if applied for a valid action', async function () {
    // effect.listenTo(actionIdentifier)
    const result = await effect.apply(validResult)
    expect(result).toEqual(true)
    expect(validResult.options.callback).toHaveBeenCalled()
  })

  // it('should not call its callback if applied for an invalid action', function () {
  //   const effect = new TestEffect(effectIdentifier)
  //   const result = effect.apply(validResult)
  //   expect(result).toEqual(false)
  //   expect(validResult.options.callback).not.toHaveBeenCalled()
  // })

  it('should call a callback that was set via a pre hook', async function () {
    // effect.listenTo(actionIdentifier)
    const callback = vi.fn()
    const hook = (options) => {
      options.result.options.callback = callback
      return options
    }
    effect.addPreHook(hook)
    const result = await effect.apply(validResult)
    expect(result).toEqual(true)
    expect(callback).toHaveBeenCalled()
  })
})

describe('ChatEffect', function () {
  it('should create a chat message for a roll', async function () {
    const actionName = 'attack'
    const actionIdentifier = CreateActionIdentifier(actionName)

    const callback = vi.fn()
    const validResult = {
      action: actionIdentifier,
      options: {},
      roll: {
        toMessage: callback,
      },
    }
    const effect = new ChatEffect()
    effect.listenTo(actionIdentifier)
    const result = await effect.apply(validResult)
    expect(result).toEqual(true)
    expect(callback).toHaveBeenCalled()
  })
})
