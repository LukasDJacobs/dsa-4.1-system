import { Action, Effect } from '../../src/module/ruleset/rule-components.js'
import type { RuleComponentIdentifier } from '../../src/module/ruleset/rule-components.js'

export class TestAction extends Action<any, any> {
  private executeFunc: (_: any) => any

  constructor(
    identifier: RuleComponentIdentifier,
    executeFunc?: (_options: any) => any
  ) {
    super(identifier)
    if (executeFunc !== undefined) {
      this.executeFunc = executeFunc
    } else {
      this.executeFunc = (options: any) => ({
        options,
        resultValue: options.someOption,
      })
    }
  }

  async _execute(options: any): Promise<any> {
    return this.executeFunc(options)
  }
}

export interface TestEffectListenedResult {
  options: any
}

export class TestEffect extends Effect<TestEffectListenedResult> {
  _apply<R extends TestEffectListenedResult>(result: R): Promise<boolean> {
    if (typeof result.options.callback === 'function') {
      result.options.callback()
    }
    return new Promise((resolve) => resolve(true))
  }
}

export class EffectSpy extends Effect<any> {
  result: any

  _apply(result: any): Promise<boolean> {
    this.result = result
    return new Promise((resolve) => resolve(true))
  }
}
