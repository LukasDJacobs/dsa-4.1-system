import {
  GenericMeleeWeapon,
  GenericRangedWeapon,
} from '../../src/module/character/weapon.js'
import type { DataAccessor } from '../../src/module/model/item-data.js'

describe('GenericMeleeWeapon', function () {
  const talentSID = 'Sword'
  const initiativeMod = 3
  const name = 'SuperSword'
  const damageFormula = '1d6+3'
  const strengthMod = {
    threshold: 12,
    hitPointStep: 2,
  }
  const weaponMod = {
    attack: 1,
    parry: -1,
  }
  const itemData = {
    name,
    system: {
      talent: talentSID,
      initiativeMod,
      damage: damageFormula,
      strengthMod,
      weaponMod,
    },
  } as DataAccessor<'meleeWeapon'>

  it('should provide the name of the underlying wepaon', function () {
    const weapon = new GenericMeleeWeapon(itemData)
    expect(weapon.name).toEqual(name)
  })

  it('should provide the talent name of the underlying wepaon data', function () {
    const weapon = new GenericMeleeWeapon(itemData)
    expect(weapon.talent).toEqual(talentSID)
  })

  it('should provide the damage formula of the underlying wepaon data', function () {
    const weapon = new GenericMeleeWeapon(itemData)
    expect(weapon.damage).toEqual(damageFormula)
  })

  it('should provide the initiative mod of the underlying wepaon data', function () {
    const weapon = new GenericMeleeWeapon(itemData)
    expect(weapon.initiativeMod).toEqual(initiativeMod)
  })

  it('should provide the initiative mod of the underlying wepaon data', function () {
    const weapon = new GenericMeleeWeapon(itemData)
    expect(weapon.strengthMod).toEqual(strengthMod)
  })

  it('should provide the initiative mod of the underlying wepaon data', function () {
    const weapon = new GenericMeleeWeapon(itemData)
    expect(weapon.weaponMod).toEqual(weaponMod)
  })
})

describe('GenericRangedWeapon', function () {
  const talentSID = 'Bow'
  const name = 'Longbow'
  const damageFormula = '1d6+5'
  const itemData = {
    name,
    system: {
      talent: talentSID,
      damage: damageFormula,
    },
  } as DataAccessor<'rangedWeapon'>

  it('should provide the name of the underlying wepaon', function () {
    const weapon = new GenericRangedWeapon(itemData)
    expect(weapon.name).toEqual(name)
  })

  it('should provide the talent name of the underlying wepaon data', function () {
    const weapon = new GenericRangedWeapon(itemData)
    expect(weapon.talent).toEqual(talentSID)
  })

  it('should provide the damage formula of the underlying wepaon data', function () {
    const weapon = new GenericRangedWeapon(itemData)
    expect(weapon.damage).toEqual(damageFormula)
  })
})
