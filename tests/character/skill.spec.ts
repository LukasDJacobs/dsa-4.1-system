import {
  GenericCombatTalent,
  GenericLiturgy,
  GenericTalent,
  RollableSkill,
  Skill,
  TestableSkill,
} from '../../src/module/character/skill.js'
import type { BaseCharacter } from '../../src/module/model/character.js'
import type {
  DataAccessor,
  LiturgyData,
} from '../../src/module/model/item-data.js'
import type { TestAttributes } from '../../src/module/model/properties.js'
import { createMockRuleset } from '../ruleset/rules/helpers.js'

describe('Skill', function () {
  const character = {} as BaseCharacter
  const ruleset = createMockRuleset()

  it('should have a static creator for can take spell data', function () {
    const spellValue = 12
    const spellName = 'Ignifaxius'
    const testAttributes: TestAttributes = ['courage', 'cleverness', 'agility']
    const spellData = {
      name: spellName,
      type: 'spell',
      system: {
        value: spellValue,
        test: {
          firstAttribute: testAttributes[0],
          secondAttribute: testAttributes[1],
          thirdAttribute: testAttributes[2],
        },
      },
    } as DataAccessor<'spell'>

    const spell = Skill.create(spellData, character, ruleset)
    expect(spell?.skillType).toEqual('spell')
    expect(spell?.value).toEqual(spellValue)
    expect(spell?.name).toEqual(spellName)
  })

  it('should have a static creator for can take talent data', function () {
    const talentValue = 12
    const talentName = 'Verstecken'
    const testAttributes: TestAttributes = ['courage', 'cleverness', 'agility']
    const talentData = {
      name: talentName,
      type: 'talent',
      system: {
        value: talentValue,
        test: {
          firstAttribute: testAttributes[0],
          secondAttribute: testAttributes[1],
          thirdAttribute: testAttributes[2],
        },
      },
    } as DataAccessor<'talent'>

    const talent = Skill.create(talentData, character, ruleset)
    expect(talent?.skillType).toEqual('talent')
    expect(talent?.value).toEqual(talentValue)
    expect(talent?.name).toEqual(talentName)
  })

  it('should have a static creator for can take combat talent data', function () {
    const talentValue = 12
    const talentName = 'Dolche'
    const talentData = {
      name: talentName,
      type: 'combatTalent',
      system: {
        value: talentValue,
      },
    } as DataAccessor<'combatTalent'>

    const talent = Skill.create(talentData, character, ruleset)
    expect(talent?.skillType).toEqual('talent')
    expect(talent?.value).toEqual(talentValue)
    expect(talent?.name).toEqual(talentName)
  })

  it('should have a static creator for can take liturgy data', function () {
    const liturgyName = 'Beten'
    const liturgyDegree = 'II'
    const liturgyData = {
      name: liturgyName,
      type: 'liturgy',
      system: {
        degree: liturgyDegree,
      } as LiturgyData,
    } as DataAccessor<'liturgy'>

    character.karmaTalent = vi.fn()

    const liturgy = Skill.create(liturgyData, character, ruleset)
    expect(liturgy?.skillType).toEqual('liturgy')
    expect(liturgy?.name).toEqual(liturgyName)
  })
})

describe('RollableSkill', function () {
  const character = {} as BaseCharacter
  const ruleset = createMockRuleset()

  it('should have a static creator for can take spell data', function () {
    const spellValue = 12
    const spellName = 'Ignifaxius'
    const testAttributes: TestAttributes = ['courage', 'cleverness', 'agility']
    const spellData = {
      name: spellName,
      type: 'spell',
      system: {
        value: spellValue,
        test: {
          firstAttribute: testAttributes[0],
          secondAttribute: testAttributes[1],
          thirdAttribute: testAttributes[2],
        },
      },
    } as DataAccessor<'spell'>

    const spell = RollableSkill.create(spellData, character, ruleset)
    spell?.roll()
    expect(ruleset.execute).toHaveBeenCalled()
  })
})

describe('TestableSkill', function () {
  const character = {} as BaseCharacter
  const ruleset = createMockRuleset()

  it('should have a static creator for can take spell data', function () {
    const spellValue = 12
    const spellName = 'Ignifaxius'
    const testAttributes: TestAttributes = ['courage', 'cleverness', 'agility']
    const spellData = {
      name: spellName,
      type: 'spell',
      system: {
        value: spellValue,
        test: {
          firstAttribute: testAttributes[0],
          secondAttribute: testAttributes[1],
          thirdAttribute: testAttributes[2],
        },
      },
    } as DataAccessor<'spell'>

    const spell = TestableSkill.create(spellData, character, ruleset)
    expect(spell?.testAttributes).toEqual(testAttributes)
  })
})

describe('GenericTalent', function () {
  const character = {} as BaseCharacter
  const ruleset = createMockRuleset()

  it('should provide the effective encumbarance', function () {
    const talentValue = 12
    const talentName = 'Verstecken'
    const testAttributes: TestAttributes = ['courage', 'cleverness', 'agility']
    const talentData = {
      name: talentName,
      type: 'talent',
      system: {
        value: talentValue,
        test: {
          firstAttribute: testAttributes[0],
          secondAttribute: testAttributes[1],
          thirdAttribute: testAttributes[2],
        },
        effectiveEncumbarance: {
          type: 'formula',
          formula: 'BE-2',
        },
      },
    } as DataAccessor<'talent'>

    const talent = new GenericTalent(talentData, character, ruleset)
    expect(talent.effectiveEncumbarance).toEqual(
      talentData.system.effectiveEncumbarance
    )
  })
})

describe('GenericCombatTalent', function () {
  const character = {} as BaseCharacter
  const ruleset = createMockRuleset()

  it('should provide the effective encumbarance', function () {
    const talentValue = 12
    const talentName = 'Verstecken'
    const talentData = {
      name: talentName,
      type: 'combatTalent',
      system: {
        value: talentValue,
        effectiveEncumbarance: {
          type: 'formula',
          formula: 'BE-2',
        },
      },
    } as DataAccessor<'combatTalent'>

    const talent = new GenericCombatTalent(talentData, character, ruleset)
    expect(talent.effectiveEncumbarance).toEqual(
      talentData.system.effectiveEncumbarance
    )
  })

  it('should provide be unarmed for raufen and ringen', function () {
    const talentValue = 12
    const talentName = 'Raufen'
    const talentData = {
      name: talentName,
      type: 'combatTalent',
      system: {
        sid: 'talent-raufen',
        value: talentValue,
        effectiveEncumbarance: {
          type: 'formula',
          formula: 'BE-2',
        },
      },
    } as DataAccessor<'combatTalent'>

    const talent = new GenericCombatTalent(talentData, character, ruleset)
    expect(talent.isUnarmed).toBeTruthy()
  })

  it('should provide be armed if not raufen and ringen', function () {
    const talentValue = 12
    const talentName = 'Dolch'
    const talentData = {
      name: talentName,
      type: 'combatTalent',
      system: {
        sid: 'talent-dolche',
        value: talentValue,
        effectiveEncumbarance: {
          type: 'formula',
          formula: 'BE-2',
        },
      },
    } as DataAccessor<'combatTalent'>

    const talent = new GenericCombatTalent(talentData, character, ruleset)
    expect(talent.isUnarmed).toBeFalsy()
  })
})

describe('GenericLiturgy', function () {
  const character = {} as BaseCharacter
  const ruleset = createMockRuleset()

  it('should provide the degree, value and test attributes', function () {
    const liturgyDegree = 'II'
    const liturgyName = 'Beten'
    const karmaTalentValue = 4
    const testAttributes: TestAttributes = ['courage', 'cleverness', 'agility']

    const liturgyData = {
      name: liturgyName,
      type: 'liturgy',
      system: {
        degree: liturgyDegree,
      },
    } as DataAccessor<'liturgy'>

    character.karmaTalent = vi.fn().mockReturnValue({
      value: karmaTalentValue,
      testAttributes,
    })

    const liturgy = new GenericLiturgy(liturgyData, character, ruleset)
    expect(liturgy.degree).toEqual(liturgyDegree)
    expect(liturgy.value).toEqual(karmaTalentValue)
    expect(liturgy.testAttributes).toEqual(testAttributes)
  })
})
