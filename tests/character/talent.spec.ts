import { GenericTalent } from '../../src/module/character/skill.js'
import type { BaseCharacter } from '../../src/module/model/character.js'
import type { DataAccessor } from '../../src/module/model/item-data.js'

import type {
  SkillDescriptor,
  TestAttributes,
} from '../../src/module/model/properties.js'
import { TalentAction } from '../../src/module/ruleset/rules/basic-skill.js'
import { createMockRuleset } from '../ruleset/rules/helpers.js'

describe('GenericTalent', function () {
  const character = {} as BaseCharacter
  const ruleset = createMockRuleset()

  const talentValue = 12
  const talentSID = 'talent-swimming'
  const talentName = 'Swimming'
  const testAttributes: TestAttributes = ['courage', 'cleverness', 'agility']
  const itemData = {
    name: talentName,
    system: {
      sid: talentSID,
      value: talentValue,
      test: {
        firstAttribute: testAttributes[0],
        secondAttribute: testAttributes[1],
        thirdAttribute: testAttributes[2],
      },
    },
  } as DataAccessor<'talent'>

  const talent = new GenericTalent(itemData, character, ruleset)

  it('should have its given name', function () {
    expect(talent.name).toEqual(talentName)
  })

  it('should be of skill type talent', function () {
    expect(talent.skillType).toEqual('talent')
  })

  it('should return the correct value of its related data entity', function () {
    expect(talent.value).toEqual(talentValue)
  })

  it('should provide a list of test attributes', function () {
    expect(talent.testAttributes[0]).toEqual(testAttributes[0])
    expect(talent.testAttributes[1]).toEqual(testAttributes[1])
    expect(talent.testAttributes[2]).toEqual(testAttributes[2])
  })

  const talentDescriptor: SkillDescriptor = {
    name: talentName,
    identifier: talentSID,
    skillType: 'talent',
  }

  it('should execute the roll talent action on its given ruleset', function () {
    const mod = 5

    const rollOptions = {
      character,
      mod,
      skill: talentDescriptor,
    }
    talent.roll({
      mod,
    })

    expect(ruleset.execute).toHaveBeenCalledWith(TalentAction, rollOptions)
  })
})
