import { Attribute } from '../../src/module/character/attribute.js'
import {
  CharacterBaseData,
  CharacterData,
} from '../../src/module/model/character-data.js'
import { RollAttribute } from '../../src/module/ruleset/rules/basic-roll-mechanic.js'
import { createMockRuleset } from '../ruleset/rules/helpers.js'

describe('Attribute', function () {
  const attributeValue = 12
  const characterData = {
    base: {
      basicAttributes: {
        courage: {
          value: attributeValue,
        },
      },
    } as unknown as CharacterBaseData,
  } as CharacterData
  const attributeName = 'courage'
  const ruleset = createMockRuleset()

  const attribute = new Attribute(characterData, attributeName, ruleset)

  it('should be have its given name', function () {
    expect(attribute.name).toEqual(attributeName)
  })

  it('should return the correct value of its related data attribute', function () {
    expect(attribute.value).toEqual(attributeValue)
  })

  it('should execute the roll attribute action on its given ruleset', function () {
    const rollOptions = {
      targetValue: attributeValue,
    }
    attribute.roll()
    expect(ruleset.execute).toHaveBeenCalledWith(
      RollAttribute,
      expect.objectContaining(rollOptions)
    )
  })
})
