import type {
  CharacterSettings,
  CharacterBaseData,
} from '../../src/module/model/character-data.js'
import { createMockRuleset } from '../ruleset/rules/helpers.js'

const Hooks = {
  on: vi.fn(),
}
global.Hooks = Hooks

class Item {}
global.Item = Item

const CONST = {
  ACTIVE_EFFECT_MODES: {
    ADD: 0,
  },
}
global.CONST = CONST

class Actor {
  prepareDerivedData() {
    return
  }
}
global.Actor = Actor

global.foundry = {
  utils: {
    mergeObject: (org, other) => other,
    expandObject: vi.fn(),
    flattenObject: vi.fn(),
  },
}

const ruleset = createMockRuleset()
import { getGame } from '../../src/module/utils.js'
getGame.mockReturnValue({ ruleset })

ruleset.compute.mockReturnValue(1)

const { DsaActor } = await import('../../src/module/actor/actor.js')

describe('DsaActor', () => {
  it('to calculate derived data if enabled', () => {
    const settings: CharacterSettings = {
      autoCalcBaseAttack: true,
      autoCalcBaseParry: true,
      autoCalcBaseRangedAttack: true,
      autoCalcInitiative: true,
      autoCalcMagicResistance: true,
      autoCalcWoundThresholds: true,
    }
    const actor = new DsaActor({
      name: 'test',
      type: 'character',
    })
    actor.system = {
      base: {} as CharacterBaseData,
      settings: settings as CharacterSettings,
      additionalItems: [],
    }
    actor.items = []
    actor.effects = []
    actor.prepareDerivedData()
    expect(actor.system.base.combatAttributes.active.baseAttack.value).toEqual(
      1
    )
    expect(actor.system.base.combatAttributes.active.baseParry.value).toEqual(1)
    expect(
      actor.system.base.combatAttributes.active.baseRangedAttack.value
    ).toEqual(1)
    expect(
      actor.system.base.combatAttributes.active.baseInitiative.value
    ).toEqual(1)
    expect(
      actor.system.base.combatAttributes.passive.magicResistance.value
    ).toEqual(1)
    expect(actor.system.base.combatAttributes.passive.woundThresholds).toEqual(
      1
    )
  })

  it('to not calculate derived data if disabled', () => {
    const settings: CharacterSettings = {
      autoCalcBaseAttack: false,
      autoCalcBaseParry: false,
      autoCalcBaseRangedAttack: false,
      autoCalcInitiative: false,
      autoCalcMagicResistance: false,
      autoCalcWoundThresholds: false,
    }
    const actor = new DsaActor({
      name: 'test',
      type: 'character',
    })
    actor.system = {
      base: {
        combatAttributes: {
          active: {
            baseAttack: {
              value: 0,
            },
            baseParry: {
              value: 0,
            },
            baseRangedAttack: {
              value: 0,
            },
            baseInitiative: {
              value: 0,
            },
          },
          passive: {
            magicResistance: {
              value: 0,
            },
            woundThresholds: {
              first: 0,
              second: 0,
              third: 0,
              mod: 0,
            },
          },
        } as any,
      } as CharacterBaseData,
      settings: settings as CharacterSettings,
      additionalItems: [],
    }
    actor.items = []
    actor.effects = []
    actor.prepareDerivedData()
    expect(actor.system.base.combatAttributes.active.baseAttack.value).toEqual(
      0
    )
    expect(actor.system.base.combatAttributes.active.baseParry.value).toEqual(0)
    expect(
      actor.system.base.combatAttributes.active.baseRangedAttack.value
    ).toEqual(0)
    expect(
      actor.system.base.combatAttributes.active.baseInitiative.value
    ).toEqual(0)
    expect(
      actor.system.base.combatAttributes.passive.magicResistance.value
    ).toEqual(0)
    expect(actor.system.base.combatAttributes.passive.woundThresholds).toEqual({
      first: 0,
      second: 0,
      third: 0,
      mod: 0,
    })
  })

  it('to not calculate derived data if disabled and apply effects only once', () => {
    const settings: CharacterSettings = {
      autoCalcBaseAttack: false,
      autoCalcBaseParry: false,
      autoCalcBaseRangedAttack: false,
      autoCalcInitiative: false,
      autoCalcMagicResistance: false,
      autoCalcWoundThresholds: false,
    }
    const actor = new DsaActor({
      name: 'test',
      type: 'character',
    })
    actor.system = {
      base: {
        combatAttributes: {
          active: {
            baseAttack: {
              value: 0,
            },
            baseParry: {
              value: 0,
            },
            baseRangedAttack: {
              value: 0,
            },
            baseInitiative: {
              value: 0,
            },
          },
          passive: {
            magicResistance: {
              value: 0,
            },
            woundThresholds: {
              first: 0,
              second: 0,
              third: 0,
              mod: 0,
            },
          },
        } as any,
      } as CharacterBaseData,
      settings: settings as CharacterSettings,
      additionalItems: [],
    }
    actor.items = []
    actor.effects = [
      {
        changes: [
          {
            key: 'system.base.combatAttributes.active.baseAttack.value',
            priority: 1,
            mode: 0,
          },
        ],
        isSupressed: false,
        disabled: false,
        apply: () => 1,
      },
    ]
    global.foundry = {
      utils: {
        duplicate: (obj) => obj,
        flattenObject: (obj) => obj,
        expandObject: (obj) => obj,
      },
    }
    actor.prepareDerivedData()
    expect(actor.system.base.combatAttributes.active.baseAttack.value).toEqual(
      0
    )
    expect(actor.system.base.combatAttributes.active.baseParry.value).toEqual(0)
    expect(
      actor.system.base.combatAttributes.active.baseRangedAttack.value
    ).toEqual(0)
    expect(
      actor.system.base.combatAttributes.active.baseInitiative.value
    ).toEqual(0)
    expect(
      actor.system.base.combatAttributes.passive.magicResistance.value
    ).toEqual(0)
    expect(actor.system.base.combatAttributes.passive.woundThresholds).toEqual({
      first: 0,
      second: 0,
      third: 0,
      mod: 0,
    })
  })

  it('can add, delete and count wounds', () => {
    const actor = new DsaActor({
      name: 'test',
      type: 'character',
    })
    actor.effects = [
      {
        getFlag: vi.fn().mockReturnValue('simpleWound'),
        delete: vi.fn(),
      } as any,
    ]
    actor.createEmbeddedDocuments = vi.fn()
    actor.addWound()
    expect(actor.createEmbeddedDocuments).toHaveBeenCalledWith('ActiveEffect', [
      expect.objectContaining({
        name: 'wound',
        icon: 'icons/svg/blood.svg',
      }),
    ])
    actor.removeWound()
    expect(actor.effects[0].delete).toHaveBeenCalled()
    const count = actor.countWounds()
    expect(count).toEqual(1)
  })
})
