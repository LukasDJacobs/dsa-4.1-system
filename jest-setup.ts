import '@testing-library/jest-dom'
import { jest } from '@jest/globals'

jest.unstable_mockModule('./src/module/utils.js', () => ({
  getLocalizer: jest.fn().mockImplementation(() => (a) => a),
}))
