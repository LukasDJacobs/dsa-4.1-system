export type ModifierType = 'maneuver' | 'maneuverModifier' | 'other'

export interface ModifierDescriptor {
  name: string
  mod: number
  modifierType: ModifierType
  source?: string
}

export type ManeuverType = 'offensive' | 'defensive'

export interface ManeuverDescriptor extends ModifierDescriptor {
  type: ManeuverType
  minMod: number
}

export type ManeuverModifier = {
  name: string
  value: number
  source: string
}
