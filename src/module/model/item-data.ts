import { AttributeName } from './character-data.js'
import { RangeClass } from '../ruleset/rules/derived-combat-attributes.js'

export interface DataTypeMap {
  talent: TalentData
  language: LanguageData
  scripture: ScriptureData
  combatTalent: CombatTalentData
  advantage: AdvantageData
  disadvantage: DisadvantageData
  specialAbility: SpecialAbilityData
  spell: SpellData
  spellVariant: SpellVariantData
  liturgy: LiturgyData
  liturgyVariant: LiturgyVariantData
  meleeWeapon: MeleeWeaponData
  rangedWeapon: RangedWeaponData
  armor: ArmorData
  shield: ShieldData
  genericItem: GenericItemData
}

export type ItemType = keyof DataTypeMap
export type TestableTalentItemType = 'talent' | 'language' | 'scripture'
export type TalentItemType = TestableTalentItemType | 'combatTalent'
export type SkillItemType = TalentItemType | 'spell' | 'liturgy'
export type TestableSkillItemType = TestableTalentItemType | 'spell'
export type RollablSkillItemType = TestableSkillItemType | 'liturgy'
export type WeaponItemType = 'meleeWeapon' | 'rangedWeapon'
export type TraitItemType = 'advantage' | 'disadvantage' | 'specialAbility'
export type PropertyItemType =
  | SkillItemType
  | TraitItemType
  | 'spellVariant'
  | 'liturgyVariant'
export type PhysicalItemType =
  | WeaponItemType
  | 'armor'
  | 'shield'
  | 'genericItem'

export interface UpdateData<DataType> {
  name: string
  system: DeepPartial<DataType>
}

export type BaseDataAccessor<Type extends ItemType = ItemType> = {
  name: string | null
  id: string | null
  type: Type
  img: string | null
  system: DataTypeMap[Type]
  update(data: UpdateData<Type>): Promise<BaseDataAccessor<Type> | undefined>
}

export type DataAccessor<Types extends ItemType = ItemType> = {
  [Type in Types]: BaseDataAccessor<Type>
}[Types]

export type DataAccessorsByType = {
  [Type in ItemType]: DataAccessor<Type>[]
}

export function isTestable(
  skill: DataTypeMap[SkillItemType]
): skill is DataTypeMap[TestableSkillItemType] {
  return (skill as DataTypeMap[TestableSkillItemType]).test !== undefined
}

export function hasValue(
  trait: DataTypeMap[TraitItemType]
): trait is DataTypeMap[TraitItemType] & { value: number } {
  return (trait as { value: number }).value !== null
}

export type NamedItem<Data> = Data & {
  name: string
  itemType: ItemType
}

export interface BaseItemData {
  description: string
}

interface UniquelyIdentifiable {
  isUniquelyIdentifiable: boolean
  sid: string
}

interface SkillcheckAttributes {
  firstAttribute: AttributeName
  secondAttribute: AttributeName
  thirdAttribute: AttributeName
}

interface SkillcheckData {
  test: SkillcheckAttributes
}

interface EffectiveEncumbaranceData {
  type: 'none' | 'special' | 'formula'
  formula: string
}

export interface BaseTalentData {
  type: string
  category: string
  effectiveEncumbarance: EffectiveEncumbaranceData
  value: number
}

interface BaseWeaponData {
  talent: string
  damage: string
}

interface Buyable {
  price: string
}

interface Physical {
  weight: string
}

interface Equippable {
  equipped: boolean
}

interface TimeData {
  duration: number
  unit: string
  info: string
}

interface Castable {
  castTime: TimeData
  effectTime: TimeData
  targetClasses: string[]
  range: string
  technique: string
  effect: string
  variants: string[]
}

export interface PropertyData extends BaseItemData, UniquelyIdentifiable {}

export function isProperty<Data>(
  data: Data | PropertyData
): data is PropertyData {
  return (<PropertyData>data).sid !== undefined
}

export interface TalentData
  extends PropertyData,
    BaseTalentData,
    SkillcheckData {}

export interface LanguageData extends TalentData {
  complexity: string
}

export interface ScriptureData extends TalentData {
  complexity: string
}

interface CombatData {
  category: string
  attack: number
  parry: number
  rangedAttack: number
}

export interface CombatTalentData extends PropertyData, BaseTalentData {
  combat: CombatData
}

export interface AdvantageData extends PropertyData {
  value: number
}

export interface DisadvantageData extends PropertyData {
  negativeAttribute: boolean
  value: number
}

export interface SpecialAbilityData extends PropertyData {
  type: string
}

export interface SpellData extends PropertyData, SkillcheckData, Castable {
  value: number
  testMod: string
  astralCost: string
  modifications: string[]
  lcdPage: number
  reversalis: string
  antimagic: string
  properties: string[]
  complexity: string
  representation: string
  spread: string
}

export interface SpellVariantData extends PropertyData {
  modificator: string
  minimalValue: number
  astralCost: string
}

export interface LiturgyData extends PropertyData, Castable {
  type: string
  degree: string
  castType: string
  llpage: number
}

export interface LiturgyVariantData extends PropertyData {
  degree: number
}

export interface StrengthMod {
  threshold: number
  hitPointStep: number
}

export interface WeaponMod {
  attack: number
  parry: number
}

interface CommonWeaponData
  extends BaseItemData,
    BaseWeaponData,
    Buyable,
    Physical {
  improvised: boolean
}

export interface MeleeWeaponData extends CommonWeaponData {
  length: string
  strengthMod: StrengthMod
  breakingFactor: number
  initiativeMod: number
  weaponMod: WeaponMod
  distanceClass: string
  twoHanded: boolean
  priviliged: boolean
}

export interface RangeSteps {
  veryClose: number
  close: number
  medium: number
  far: number
  veryFar: number
}

export const RangeClassKeys: Record<RangeClass, keyof RangeSteps> =
  Object.freeze({
    [RangeClass.VeryNear]: 'veryClose',
    [RangeClass.Near]: 'close',
    [RangeClass.Medium]: 'medium',
    [RangeClass.Far]: 'far',
    [RangeClass.VeryFar]: 'veryFar',
  })

export interface RangedWeaponData extends CommonWeaponData {
  ranges: RangeSteps
  bonusDamages: RangeSteps
  loadtime: number
  projectilePrice: number
  loweredWoundThreshold: boolean
  entangles: boolean
}

export interface ArmorData extends BaseItemData, Buyable, Physical, Equippable {
  armorClass: number
  encumbarance: number
}

export interface ShieldData extends BaseItemData, Buyable, Physical {
  type: string
  sizeClass: string
  weaponMod: WeaponMod
  initiativeMod: number
  breakingFactor: number
}

export interface GenericItemData extends BaseItemData, Buyable, Physical {
  isConsumable: boolean
  quantity: number
}

export function isGeneric(
  item: DataAccessor<PhysicalItemType>
): item is DataAccessor<'genericItem'> {
  const data = (item as DataAccessor<'genericItem'>).system
  return data.isConsumable !== undefined && data.quantity !== undefined
}
