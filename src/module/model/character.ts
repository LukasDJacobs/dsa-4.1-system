import type {
  BaseProperty,
  BaseTalent,
  CombatTalent,
  NamedAttribute,
  Rollable,
  SpecialAbility,
  Spell,
  Liturgy,
  Talent,
} from './properties.js'
import type { Armor, RollFormula, Shield, Weapon } from './items.js'
import type {
  ManeuverDescriptor,
  ManeuverModifier,
  ManeuverType,
  ModifierDescriptor,
} from './modifier.js'
import type {
  RangeClass,
  SizeClass,
} from '../ruleset/rules/derived-combat-attributes.js'
import { AttributeName, CharacterData } from './character-data.js'
import {
  DataAccessor,
  DataAccessorsByType,
  PropertyItemType,
  TalentItemType,
  UpdateData,
  WeaponItemType,
} from './item-data.js'
import { InitiativeFormula } from '../ruleset/rules/basic-combat.js'

export interface BaseCombatOptions {
  modifiers?: ModifierDescriptor[]
  maneuvers?: ManeuverDescriptor[]
}

type Armed<CombatOptions extends BaseCombatOptions> = CombatOptions & {
  weapon: Weapon
}

type Unarmed<CombatOptions extends BaseCombatOptions> = CombatOptions & {
  talent: CombatTalent
}

export interface ArmedMeleeCombatOptions extends Armed<BaseCombatOptions> {
  shield?: Shield
  secondaryWeapon?: Weapon
  useSecondaryHand?: boolean
}

export type MeleeCombatOptions =
  | ArmedMeleeCombatOptions
  | Unarmed<BaseCombatOptions>

export interface RangedCombatOptions extends Armed<BaseCombatOptions> {
  sizeClass?: SizeClass
  rangeClass?: RangeClass
}

export interface CombatState {
  isArmed: boolean
  primaryHand: DataAccessor<WeaponItemType> | undefined
  secondaryHand: DataAccessor<WeaponItemType | 'shield'> | undefined
  unarmedTalent: DataAccessor<'combatTalent'> | undefined
}

export interface CharacterDataAccessor {
  name: string | null
  system: CharacterData

  update(data: UpdateData<CharacterData>): Promise<this | undefined>

  attribute(name: AttributeName): Rollable<NamedAttribute>
  baseAttack: number
  baseParry: number
  baseRangedAttack: number
  baseInitiative: number
  dodge: number

  talent(
    sid: string
  ):
    | DataAccessor<'talent' | 'language' | 'scripture' | 'combatTalent'>
    | undefined
  spell(sid: string): DataAccessor<'spell'> | undefined
  liturgy(sid: string): DataAccessor<'liturgy'> | undefined
  specialAbility(sid: string): DataAccessor<'specialAbility'> | undefined
  advantage(sid: string): DataAccessor<'advantage'> | undefined
  disadvantage(sid: string): DataAccessor<'disadvantage'> | undefined

  talents: DataAccessor<TalentItemType>[]
  spells: DataAccessor<'spell'>[]
  liturgies: DataAccessor<'liturgy'>[]
  properties: DataAccessor<PropertyItemType>[]
  specialAbilities: DataAccessor<'specialAbility'>[]
  advantages: DataAccessor<'advantage'>[]
  disadvantages: DataAccessor<'disadvantage'>[]

  weapons: DataAccessor<WeaponItemType>[]
  shields: DataAccessor<'shield'>[]
  armorItems: DataAccessor<'armor'>[]

  itemTypes: DataAccessorsByType

  combatState: CombatState

  addWound(): Promise<void>
  removeWound(): Promise<void>
  countWounds(): number
}

export interface BaseCharacter {
  data: CharacterDataAccessor

  attribute(name: AttributeName): Rollable<NamedAttribute>

  baseAttack: number
  baseParry: number
  baseRangedAttack: number
  baseDodge: number
  attackValue(useSecondaryHand?: boolean): number
  attackValue(options?: MeleeCombatOptions): number
  parryValue(useSecondaryHand?: boolean): number
  parryValue(options?: MeleeCombatOptions): number
  rangedAttackValue(options?: RangedCombatOptions): number
  attackModifiers(useSecondaryHand?: boolean): ModifierDescriptor[]
  attackModifiers(options?: MeleeCombatOptions): ModifierDescriptor[]
  parryModifiers(useSecondaryHand?: boolean): ModifierDescriptor[]
  parryModifiers(options?: MeleeCombatOptions): ModifierDescriptor[]
  rangedAttackModifiers(options?: RangedCombatOptions): ModifierDescriptor[]
  dodgeValue(options?: MeleeCombatOptions): number
  damage(useSecondaryHand?: boolean): RollFormula
  damage(options?: BaseCombatOptions): RollFormula
  initiative(): InitiativeFormula
  has(property: BaseProperty): boolean
  attack(options?: BaseCombatOptions): void
  rangedAttack(options?: BaseCombatOptions): void
  parry(options?: BaseCombatOptions): void
  dodge(options?: BaseCombatOptions): void
  rollDamage(options?: BaseCombatOptions): void
  maneuverList(
    maneuverType: ManeuverType,
    armed?: boolean
  ): ManeuverDescriptor[]
  maneuverModifiers: ManeuverModifier[]
  armorClass: number
  encumbarance: number

  talent(sid: string): BaseTalent | undefined
  spell(sid: string): Spell | undefined
  liturgy(sid: string): Liturgy | undefined

  armorItems: Armor[]
  specialAbilities: SpecialAbility[]
  properties: BaseProperty[]
  baseInitiative: number
  effectiveEncumbarance(formula: string): number
  degreeModifier(degree: string): number
  karmaTalent(): Talent | undefined
}
