export type Nameable<Type> = Type & {
  name: string
}

export function hasName<Type>(t: Type): t is Nameable<Type> {
  return (
    (t as Nameable<Type>).name !== undefined &&
    (t as Nameable<Type>).name !== null
  )
}
