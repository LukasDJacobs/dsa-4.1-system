import {
  SvelteApplication,
  SvelteApplicationOptions,
} from '@typhonjs-fvtt/runtime/svelte/application'
import { TJSDocument } from '@typhonjs-fvtt/runtime/svelte/store/fvtt/document'
import ActorSheetShellSvelte from '../../ui/ActorSheetShell.svelte'
import { getGame } from '../utils.js'
import { TJSSessionStorage } from '@typhonjs-fvtt/runtime/svelte/store/web-storage'
import type { DsaActor } from './actor.js'
import { ActorSettingsSheet } from './actor-settings-sheet.js'

export class DsaSvelteActorSheet extends SvelteApplication {
  #documentStore = new TJSDocument(void 0, { delete: this.close.bind(this) })

  sessionStorage = new TJSSessionStorage()

  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      width: 950,
      height: 800,
      resizable: true,
      minimizable: true,
      svelte: {
        class: ActorSheetShellSvelte,
        target: document.body,
        props: function () {
          return {
            doc: this.#documentStore,
          }
        },
      },
    })
  }

  private document: any
  public actor: DsaActor

  constructor(object: SvelteApplicationOptions) {
    super(object)
    this.actor = object as DsaActor

    /**
     * @member {object} document - Adds accessors to SvelteReactive to get / set the document associated with
     *                             Document.
     *
     * @memberof SvelteReactive#
     */
    Object.defineProperty(this.reactive, 'document', {
      get: () => this.#documentStore.get(),
      set: (document) => {
        this.document = document
        this.#documentStore.set(this.document)
      },
    })
    this.reactive.document = object
  }

  async update() {
    await this.document.update({
      ...this.document,
    })
  }

  _getHeaderButtons() {
    const buttons = super._getHeaderButtons()
    const game = getGame()

    if (
      game.user?.isGM ||
      (this.reactive.document.isOwner && game.user?.can('TOKEN_CONFIGURE'))
    ) {
      buttons.unshift({
        label: this.token ? 'Token' : 'TOKEN.TitlePrototype',
        class: 'configure-token',
        icon: 'fas fa-user-circle',
        onclick: (ev) => this._onConfigureToken(ev),
      })
    }

    if (this.reactive.document.getFlag('core', 'sheetLock') !== true) {
      buttons.unshift({
        label: 'Sheet',
        class: 'configure-sheet',
        icon: 'fas fa-cog',
        onclick: (ev) => this._onConfigureSheet(ev),
      })
    }

    if (game.user?.isGM || this.reactive.document.isOwner) {
      buttons.unshift({
        class: 'actor-settings',
        icon: 'fas fa-cog',
        label: 'DSA.actorSettings',
        onclick: (ev) => {
          if (ev) {
            ev.preventDefault()
          }
          new ActorSettingsSheet(this.reactive.document).render(true)
        },
      })
    }

    if (game.user?.isGM || this.reactive.document.isOwner) {
      const isEditMode = this.sessionStorage.getItem('edit-mode-state', false)

      buttons.unshift({
        class: 'toggle-edit-mode',
        icon: isEditMode ? 'fas fa-toggle-on' : 'fas fa-toggle-off',
        label: 'DSA.toggleEdit',
        onclick: (event) => {
          const newEditModeState = this.sessionStorage.swapItemBoolean(
            'edit-mode-state',
            true
          )
          $(event.currentTarget)
            .find('i')
            .attr(
              'class',
              newEditModeState ? 'fas fa-toggle-on' : 'fas fa-toggle-off'
            )
        },
      })
    }

    return buttons
  }

  _onConfigureSheet(event) {
    if (event) {
      event.preventDefault()
    }
    // eslint-disable-next-line no-undef
    new DocumentSheetConfig(this.actor, {
      top: this.position.top + 40,
      left:
        this.position.left +
        (this.position.width - DsaSvelteActorSheet.defaultOptions.width) / 2,
    }).render(true)
  }

  _onConfigureToken(event) {
    if (event) {
      event.preventDefault()
    }
    const actor = this.actor
    const token = actor.isToken ? actor.token : actor.prototypeToken
    new CONFIG.Token.prototypeSheetClass(token, {
      left: Math.max(this.position.left - 560 - 10, 10),
      top: this.position.top,
    }).render(true)
  }

  /** @inheritdoc */
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  _canDragStart(selector) {
    return true
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  _canDragDrop(selector) {
    return true
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDragStart(event) {
    const li = event.currentTarget
    if (event.target.classList.contains('content-link')) return

    // Create drag data
    let dragData

    // Owned Items
    if (li.dataset.itemId) {
      const item = this.actor.items.get(li.dataset.itemId)
      dragData = item.toDragData()
    }

    // Active Effect
    if (li.dataset.effectId) {
      const effect = this.actor.effects.get(li.dataset.effectId)
      dragData = effect.toDragData()
    }

    if (!dragData) return

    // Set data transfer
    event.dataTransfer.setData('text/plain', JSON.stringify(dragData))
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async _onDrop(event) {
    const data = TextEditor.getDragEventData(event)
    const actor = this.actor

    /**
     * A hook event that fires when some useful data is dropped onto an ActorSheet.
     * @function dropActorSheetData
     * @memberof hookEvents
     * @param {Actor} actor      The Actor
     * @param {ActorSheet} sheet The ActorSheet application
     * @param {object} data      The data that has been dropped onto the sheet
     */
    const allowed = Hooks.call('dropActorSheetData', actor, this, data)
    if (allowed === false) return

    // Handle different data types
    switch (data.type) {
      case 'ActiveEffect':
        return this._onDropActiveEffect(event, data)
      case 'Actor':
        return this._onDropActor(event, data)
      case 'Item':
        return this._onDropItem(event, data)
      case 'Folder':
        return this._onDropFolder(event, data)
    }
  }

  /* -------------------------------------------- */

  /**
   * Handle the dropping of ActiveEffect data onto an Actor Sheet
   * @param {DragEvent} event                  The concluding DragEvent which contains drop data
   * @param {object} data                      The data transfer extracted from the event
   * @returns {Promise<ActiveEffect|boolean>}  The created ActiveEffect object or false if it couldn't be created.
   * @protected
   */
  async _onDropActiveEffect(event, data) {
    const effect = await ActiveEffect.implementation.fromDropData(data)
    if (!this.actor.isOwner || !effect) return false
    if (this.actor.uuid === effect.parent.uuid) return false
    return ActiveEffect.create(effect.toObject(), { parent: this.actor })
  }

  /* -------------------------------------------- */

  /**
   * Handle dropping of an Actor data onto another Actor sheet
   * @param {DragEvent} event            The concluding DragEvent which contains drop data
   * @param {object} data                The data transfer extracted from the event
   * @returns {Promise<object|boolean>}  A data object which describes the result of the drop, or false if the drop was
   *                                     not permitted.
   * @protected
   */
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async _onDropActor(event, data) {
    if (!this.actor.isOwner) return false
  }

  /* -------------------------------------------- */

  /**
   * Handle dropping of an item reference or item data onto an Actor Sheet
   * @param {DragEvent} event            The concluding DragEvent which contains drop data
   * @param {object} data                The data transfer extracted from the event
   * @returns {Promise<Item[]|boolean>}  The created or updated Item instances, or false if the drop was not permitted.
   * @protected
   */
  async _onDropItem(event, data) {
    if (!this.actor.isOwner) return false
    const item = await Item.implementation.fromDropData(data)
    const itemData = item.toObject()

    // Handle item sorting within the same Actor
    if (this.actor.uuid === item.parent?.uuid)
      return this._onSortItem(event, itemData)

    // Create the owned item
    return this._onDropItemCreate(itemData)
  }

  /* -------------------------------------------- */

  /**
   * Handle dropping of a Folder on an Actor Sheet.
   * The core sheet currently supports dropping a Folder of Items to create all items as owned items.
   * @param {DragEvent} event     The concluding DragEvent which contains drop data
   * @param {object} data         The data transfer extracted from the event
   * @returns {Promise<Item[]>}
   * @protected
   */
  async _onDropFolder(event, data) {
    if (!this.actor.isOwner) return []
    if (data.documentName !== 'Item') return []
    const folder = await Folder.implementation.fromDropData(data)
    if (!folder) return []
    return this._onDropItemCreate(
      folder.contents.map((item) => {
        return game.items.fromCompendium(item)
      })
    )
  }

  /* -------------------------------------------- */

  /**
   * Handle the final creation of dropped Item data on the Actor.
   * This method is factored out to allow downstream classes the opportunity to override item creation behavior.
   * @param {object[]|object} itemData     The item data requested for creation
   * @returns {Promise<Item[]>}
   * @private
   */
  async _onDropItemCreate(itemData) {
    itemData = itemData instanceof Array ? itemData : [itemData]
    return this.actor.createEmbeddedDocuments('Item', itemData)
  }

  /* -------------------------------------------- */

  /**
   * Handle a drop event for an existing embedded Item to sort that Item relative to its siblings
   * @param {Event} event
   * @param {Object} itemData
   * @private
   */
  _onSortItem(event, itemData) {
    // Get the drag source and drop target
    const items = this.actor.items
    const source = items.get(itemData._id)
    const dropTarget = event.target.closest('[data-item-id]')
    if (!dropTarget) return
    const target = items.get(dropTarget.dataset.itemId)

    // Don't sort on yourself
    if (source.id === target.id) return

    // Identify sibling items based on adjacent HTML elements
    const siblings = []
    for (const el of dropTarget.parentElement.children) {
      const siblingId = el.dataset.itemId
      if (siblingId && siblingId !== source.id)
        siblings.push(items.get(el.dataset.itemId))
    }

    // Perform the sort
    const sortUpdates = SortingHelpers.performIntegerSort(source, {
      target,
      siblings,
    })
    const updateData = sortUpdates.map((u) => {
      const update = u.update
      update._id = u.target._id
      return update
    })

    // Perform the update
    return this.actor.updateEmbeddedDocuments('Item', updateData)
  }
}
