import { TJSDocument } from '@typhonjs-fvtt/runtime/svelte/store/fvtt/document'
import { SvelteApplication } from '@typhonjs-fvtt/runtime/svelte/application'
import ActorSettingsSheetShellSvelte from '../../ui/actor-settings/ActorSettingsSheetShell.svelte'
import { getLocalizer } from '../utils.js'

declare module '@typhonjs-fvtt/runtime/svelte/application' {
  interface SvelteApplication extends Application {}
}

const localize = getLocalizer()

export class ActorSettingsSheet extends SvelteApplication {
  private documentStore = new TJSDocument(void 0, {
    delete: this.close.bind(this),
  })

  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      title: localize('actorSettings'),
      svelte: {
        class: ActorSettingsSheetShellSvelte,
        target: document.body,
        props: function () {
          return {
            localize: (key: string) => localize(key),
            doc: this.documentStore,
          }
        },
      },
    })
  }

  constructor(document) {
    super(document)
    this.documentStore.set(document)
  }
}
