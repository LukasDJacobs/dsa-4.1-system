import { getGame } from '../utils.js'

Hooks.on('createActor', async (actor, options, userId) => {
  if (userId === getGame().user?.id) {
    if (actor.type === 'character' && options.renderSheet) {
      const game = getGame()
      for (const packName of ['talent', 'combattalent']) {
        const pack = await game.packs?.get(`dsa-41.${packName}`)
        const talentIndex = await pack?.getDocuments()
        if (talentIndex) {
          const baseTalents = talentIndex.filter(
            (item) => item.system.type == 'basic'
          )
          const items = baseTalents.map((talent) => talent.toObject())
          await actor.createEmbeddedDocuments('Item', items)
        }
      }
    }
  }
})
