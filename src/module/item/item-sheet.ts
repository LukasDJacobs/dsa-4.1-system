import { Attribute } from '../enums.js'
import { labeledEnumValues } from '../i18n.js'
import { getGame } from '../utils.js'

export class DsaItemSheet extends ItemSheet<ItemSheet.Options> {
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      width: 'auto',
      height: 'auto',
      classes: ['dsa-41', 'sheet', 'item'],
      resizable: true,
    })
  }

  activateListeners(html: JQuery<HTMLElement>) {
    super.activateListeners(html)

    html.find('.item-delete').click(() => {
      if (this.item.actor && this.item.id) {
        this.item.actor.deleteEmbeddedDocuments('Item', [this.item.id])
        this.close()
      }
    })
  }

  get template() {
    return `systems/dsa-41/templates/items/${this.item.type}-sheet.html`
  }

  async getData() {
    const data = (await super.getData()) as any

    data.attributes = labeledEnumValues(Attribute)

    const game = getGame()
    const CombatTalentCompendium = game.settings.get(
      'dsa-41',
      'combat-talent-pack'
    )
    const combatTalentPack = await game.packs
      .get(CombatTalentCompendium)
      ?.getDocuments()

    const combatTalents = combatTalentPack?.map((talent) => ({
      name: talent.name,
      sid: talent.system.sid,
      type: talent.system.combat.category,
    }))

    data.meleeCombatTalents = combatTalents?.filter(
      (talent) => talent.type === 'melee' || talent.type === 'unarmed'
    )
    data.rangedCombatTalents = combatTalents?.filter(
      (talent) => talent.type === 'ranged'
    )

    return data
  }
}
