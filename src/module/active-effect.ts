import { Attribute, CombatAttribute } from './enums.js'
import { labeledEnumValues } from './i18n.js'
import { ManeuverDescriptor } from './model/modifier.js'
import { Maneuvers } from './ruleset/rules/maneuvers/maneuvers.js'
import { getLocalizer } from './utils.js'

const localize = getLocalizer()

interface DsaActiveEffectConfigData extends DocumentSheetOptions {
  changeableAttributes: Record<string, string>
  availableManeuvers: Record<string, string>
  effect: ActiveEffect
  maneuvers: ManeuverDescriptor[]
}

// eslint-disable-next-line no-undef
export class DsaActiveEffectConfig extends ActiveEffectConfig<DsaActiveEffectConfigData> {
  get template(): string {
    return `systems/dsa-41/templates/active-effect.html`
  }

  async getData(
    options: Application.RenderOptions | undefined
  ): Promise<DsaActiveEffectConfigData> {
    const data = (await super.getData(options)) as DsaActiveEffectConfigData

    const attributes = labeledEnumValues(Attribute, false)
    const combatAttributes = labeledEnumValues(CombatAttribute, false)

    data.changeableAttributes = attributes.reduce((prev, labeledValue) => {
      const key = `system.base.basicAttributes.${labeledValue.value}.value`
      return { ...prev, [key]: labeledValue.label }
    }, {})

    data.changeableAttributes = combatAttributes.reduce(
      (prev, labeledValue) => {
        const key = `system.base.combatAttributes.active.${labeledValue.value}.value`
        return { ...prev, [key]: labeledValue.label }
      },
      data.changeableAttributes
    )

    data.changeableAttributes[
      'system.base.combatAttributes.passive.magicResistance.value'
    ] = localize('magicResistance')

    data.changeableAttributes['system.base.movement.speed.value'] =
      localize('speed')

    data.changeableAttributes['system.additionalItems'] =
      localize('additionalItem')

    data.changeableAttributes['system.modifiers.maneuverModifiers'] =
      localize('maneuverModifier')

    data.availableManeuvers = Maneuvers.reduce((prev, maneuver) => {
      return { ...prev, [maneuver.name]: localize(maneuver.name) }
    }, {})

    data.effect.changes = data.effect.changes.map((change) => {
      if (
        change.key === 'system.additionalItems' ||
        change.key === 'system.modifiers.maneuverModifiers'
      ) {
        change.value =
          typeof change.value === 'string'
            ? { ...JSON.parse(change.value), json: change.value }
            : change.value
      }
      return change
    })

    return data
  }

  async _onDrop(event) {
    // Try to extract the data
    let data
    try {
      data = JSON.parse(event.dataTransfer.getData('text/plain'))
    } catch (err) {
      return false
    }

    if (data.type === 'Item') {
      const item = await Item.fromDropData(data)
      if (item !== undefined) {
        const idx = this.document.changes.length
        return this.submit({
          preventClose: true,
          updateData: {
            [`changes.${idx}`]: {
              key: 'system.additionalItems',
              mode: CONST.ACTIVE_EFFECT_MODES.ADD,
              value: JSON.stringify({
                name: item.name,
                uuid: data.uuid,
              }),
            },
          },
        })
      }
    }
  }

  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      dragDrop: [{ dragSelector: null, dropSelector: null }],
    })
  }

  _getSubmitData(updateData = {}) {
    const data = super._getSubmitData(updateData)
    data.changes = data.changes.map((change) => {
      if (change.key === 'system.additionalItems' && change.extraValue) {
        change.value = JSON.stringify({
          ...JSON.parse(change.value),
          value: change.extraValue,
        })
      }
      if (change.key === 'system.modifiers.maneuverModifiers') {
        change.value = JSON.stringify(change.value)
      }
      return change
    })
    return data
  }
}

const woundChanges = [
  {
    key: 'data.base.combatAttributes.active.baseAttack.value',
    value: '-2',
    mode: CONST.ACTIVE_EFFECT_MODES.ADD,
  },
  {
    key: 'data.base.combatAttributes.active.baseParry.value',
    value: '-2',
    mode: CONST.ACTIVE_EFFECT_MODES.ADD,
  },
  {
    key: 'data.base.combatAttributes.active.baseRangedAttack.value',
    value: '-2',
    mode: CONST.ACTIVE_EFFECT_MODES.ADD,
  },
  {
    key: 'data.base.combatAttributes.active.baseInitiative.value',
    value: '-2',
    mode: CONST.ACTIVE_EFFECT_MODES.ADD,
  },
  {
    key: 'data.base.basicAttributes.agility.value',
    value: '-2',
    mode: CONST.ACTIVE_EFFECT_MODES.ADD,
  },
  {
    key: 'data.base.movement.speed.value',
    value: '-1',
    mode: CONST.ACTIVE_EFFECT_MODES.ADD,
  },
]

const effectData = {
  name: 'wound',
  icon: 'icons/svg/blood.svg',
  changes: woundChanges,
  id: 'simpleWound',
  flags: {
    core: {
      statusId: 'wound',
    },
    'dsa-41': {
      effectId: 'simpleWound',
    },
  },
}

export function woundTemplate(): Record<string, unknown> {
  return { ...effectData }
}
