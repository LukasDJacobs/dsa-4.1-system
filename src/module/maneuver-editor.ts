import App from '../ui/maneuver-editor/ManeuverEditor.svelte'
import type { BaseCharacter } from './model/character.js'
import type {
  ModifierDescriptor,
  ManeuverType,
  ManeuverDescriptor,
} from './model/modifier.js'
import { getGame } from './utils.js'

export class ManeuverEditor extends Application {
  callback: any
  maneuvers: ManeuverDescriptor[]
  lastManeuver: ManeuverDescriptor[]
  character: BaseCharacter

  constructor(
    options,
    callback,
    maneuvers: ManeuverDescriptor[],
    lastManeuver: ManeuverDescriptor[],
    character: BaseCharacter
  ) {
    super(options)
    this.callback = callback
    this.maneuvers = maneuvers
    this.lastManeuver = lastManeuver
    this.character = character
  }

  component: App

  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      height: 'auto',
      resizable: true,
    } as any)
  }

  // activateListeners(html) {
  //   // console.log($(html)[0])
  //   // this.component = new App({
  //   //   target: $(html)[0],
  //   //   props: {
  //   //     name: 'Foundry',
  //   //   },
  //   // })
  // }

  async _renderInner() {
    const inner = $('<div class="svelte-app"></div>')
    // // if ( html === "" ) throw new Error(`No data was returned from template ${this.template}`);
    // return $(html)
    // const inner = await super._renderInner(data, options)
    this.component = new App({
      target: $(inner).get(0),
      props: {
        availableManeuvers: this.maneuvers,
        lastManeuver: this.lastManeuver,
        foundryApp: this,
        callback: this.callback,
        character: this.character,
        localize: (key: string) => getGame().i18n.localize('DSA.' + key),
      },
    })
    // this.component.$$.context.set('localize', game.i18n.localize)
    return inner
  }

  static create(
    maneuverType: ManeuverType,
    lastManeuver: ManeuverDescriptor[],
    character: BaseCharacter,
    armed: boolean
  ): Promise<any> {
    return new Promise((resolve) =>
      new ManeuverEditor(
        {
          title: getGame().i18n.localize('DSA.maneuverEditor'),
        },
        (modifiers: ModifierDescriptor[], mod: number) =>
          resolve([modifiers, mod]),

        character.maneuverList(maneuverType, armed),
        lastManeuver,
        character
      ).render(true)
    )
  }
}
