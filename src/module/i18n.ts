import {
  Attribute,
  CombatAttribute,
  SpellModification,
  SpellTargetClass,
  LiturgyType,
  LiturgyTargetClass,
  LiturgyCastType,
  TalentType,
  TalentCategory,
  CombatTalentCategory,
  EffectiveEncumbaranceType,
} from './enums.js'
import {
  RangeClass,
  SizeClass,
} from './ruleset/rules/derived-combat-attributes.js'
import { getLocalizer } from './utils.js'

const I18nMap = {
  [Attribute.Courage]: 'DSA.courage',
  [Attribute.Cleverness]: 'DSA.cleverness',
  [Attribute.Intuition]: 'DSA.intuition',
  [Attribute.Charisma]: 'DSA.charisma',
  [Attribute.Dexterity]: 'DSA.dexterity',
  [Attribute.Agility]: 'DSA.agility',
  [Attribute.Constitution]: 'DSA.constitution',
  [Attribute.Strength]: 'DSA.strength',

  [CombatAttribute.BaseAttack]: 'DSA.baseAttack',
  [CombatAttribute.BaseParry]: 'DSA.baseParry',
  [CombatAttribute.BaseRangedAttack]: 'DSA.baseRangedAttack',
  [CombatAttribute.BaseInitiative]: 'DSA.baseInitiative',
  [CombatAttribute.Dodge]: 'DSA.dodge',

  [SpellModification.spellCastTime]: 'DSA.spellCastTime',
  [SpellModification.ForceEffect]: 'DSA.forceEffect',
  [SpellModification.SaveCosts]: 'DSA.saveCosts',
  [SpellModification.MultipleTargets]: 'DSA.multipleTargets',
  [SpellModification.Range]: 'DSA.range',

  [SpellTargetClass.Object]: 'DSA.object',
  [SpellTargetClass.MultipleObjects]: 'DSA.multipleObjects',
  [SpellTargetClass.Person]: 'DSA.person',
  [SpellTargetClass.MultiplePersons]: 'DSA.multiplePersons',
  [SpellTargetClass.Zone]: 'DSA.zone',
  [SpellTargetClass.Creature]: 'DSA.creature',
  [SpellTargetClass.MultipleCreatures]: 'DSA.multipleCreatures',

  [LiturgyTargetClass.G]: 'DSA.liturgyTargetClassG',
  [LiturgyTargetClass.P]: 'DSA.liturgyTargetClassP',
  [LiturgyTargetClass.PP]: 'DSA.liturgyTargetClassPP',
  [LiturgyTargetClass.PPP]: 'DSA.liturgyTargetClassPPP',
  [LiturgyTargetClass.PPPP]: 'DSA.liturgyTargetClassPPPP',
  [LiturgyTargetClass.Z]: 'DSA.liturgyTargetClassZ',
  [LiturgyTargetClass.ZZ]: 'DSA.liturgyTargetClassZZ',
  [LiturgyTargetClass.ZZZ]: 'DSA.liturgyTargetClassZZZ',
  [LiturgyTargetClass.ZZZZ]: 'DSA.liturgyTargetClassZZZZ',

  [LiturgyCastType.BumpPrayer]: 'DSA.bumpPrayer',
  [LiturgyCastType.Prayer]: 'DSA.prayer',
  [LiturgyCastType.Devotion]: 'DSA.devotion',
  [LiturgyCastType.Ceremony]: 'DSA.ceremony',
  [LiturgyCastType.Cycle]: 'DSA.cycle',

  [LiturgyType.Basic]: 'DSA.basic',
  [LiturgyType.Special]: 'DSA.special',

  [TalentType.Basic]: 'DSA.basic',
  [TalentType.Special]: 'DSA.special',

  [TalentCategory.Combat]: 'DSA.combat',
  [TalentCategory.Physical]: 'DSA.physical',
  [TalentCategory.Social]: 'DSA.social',
  [TalentCategory.Nature]: 'DSA.nature',
  [TalentCategory.Knowledge]: 'DSA.knowledge',
  [TalentCategory.Language]: 'DSA.language',
  [TalentCategory.Crafting]: 'DSA.crafting',
  [TalentCategory.Karma]: 'DSA.karmaKnowledge',
  [TalentCategory.Gift]: 'DSA.gifts',

  [CombatTalentCategory.Melee]: 'DSA.meleeCombat',
  [CombatTalentCategory.Ranged]: 'DSA.rangedCombat',
  [CombatTalentCategory.Special]: 'DSA.special',

  [RangeClass[RangeClass.VeryNear]]: 'DSA.veryNear',
  [RangeClass[RangeClass.Near]]: 'DSA.near',
  [RangeClass[RangeClass.Medium]]: 'DSA.medium',
  [RangeClass[RangeClass.Far]]: 'DSA.far',
  [RangeClass[RangeClass.VeryFar]]: 'DSA.veryFar',

  [SizeClass[SizeClass.Tiny]]: 'DSA.tiny',
  [SizeClass[SizeClass.VerySmall]]: 'DSA.verySmall',
  [SizeClass[SizeClass.Small]]: 'DSA.small',
  [SizeClass[SizeClass.Medium]]: 'DSA.medium',
  [SizeClass[SizeClass.Big]]: 'DSA.big',
  [SizeClass[SizeClass.VeryBig]]: 'DSA.veryBig',

  [EffectiveEncumbaranceType.None]: 'DSA.none',
  [EffectiveEncumbaranceType.Special]: 'DSA.special',
  [EffectiveEncumbaranceType.Formula]: 'DSA.formula',
}

const localize = getLocalizer('')

export function labeledEnumValues(enumType, useKeyValue = false) {
  return Object.keys(enumType)
    .filter((key) => typeof enumType[key] !== 'number')
    .map((key) => ({
      label: localize(I18nMap[enumType[key]]),
      value: useKeyValue ? key : enumType[key],
    }))
}
