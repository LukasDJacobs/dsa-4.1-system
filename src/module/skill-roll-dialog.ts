import App from '../ui/skill-roll/SkillRoll.svelte'
import type { BaseCharacter } from './model/character.js'
import type { ModifierDescriptor } from './model/modifier.js'
import { BaseSkill, Testable, TestAttributes } from './model/properties.js'
import { getGame } from './utils.js'

export type SkillDialogCallback = (
  modifiers: ModifierDescriptor[],
  totalModifier: number,
  selectedAttributes: TestAttributes
) => void

export class SkillRollDialog extends Application {
  private callback: SkillDialogCallback
  private skill: Testable<BaseSkill>
  private character: BaseCharacter

  constructor(
    options: Partial<Application.Options> | undefined,
    callback: SkillDialogCallback,
    skill: Testable<BaseSkill>,
    character: BaseCharacter
  ) {
    super(options)
    this.callback = callback
    this.skill = skill
    this.character = character
  }

  component: App

  async _renderInner(): Promise<JQuery<HTMLElement>> {
    const inner = $('<div class="svelte-app"></div>')
    this.component = new App({
      target: $(inner).get(0) as Element,
      props: {
        foundryApp: this,
        skill: this.skill,
        character: this.character,
        callback: this.callback,
        localize: (key: string) => getGame().i18n.localize('DSA.' + key),
      },
    })
    return inner
  }

  static create(
    skill: Testable<BaseSkill>,
    character: BaseCharacter
  ): Promise<any> {
    return new Promise((resolve) =>
      new SkillRollDialog(
        {
          title: getGame().i18n.localize('DSA.skillRollDialog'),
        },
        (
          modifiers: ModifierDescriptor[],
          totalModifier: number,
          selectedAttributes: TestAttributes
        ) => resolve([modifiers, totalModifier, selectedAttributes]),
        skill,
        character
      ).render(true)
    )
  }
}
