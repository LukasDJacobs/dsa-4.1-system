import type { BaseCharacter } from '../model/character.js'

export type Func<OptionsData, ResultType> = (options: OptionsData) => ResultType
export type PreHook<OptionData = any> = (options: OptionData) => OptionData
export type PostHook<OptionData = any, ResultType = any> = (
  options: OptionData,
  result: ResultType
) => ResultType

type RuleComponentType = 'computation' | 'action' | 'effect'
export interface RuleComponentIdentifier {
  name: string
  type: RuleComponentType
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface AmendedRuleComponentIdentifier<
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  OptionData,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  ResultType,
> extends RuleComponentIdentifier {}

export type BaseComputationOptionData = {
  character: BaseCharacter
  mod?: number
}

export interface ComputationIdentifier<
  OptionData extends BaseComputationOptionData,
  ResultType,
> extends AmendedRuleComponentIdentifier<OptionData, ResultType> {
  type: 'computation'
}

export function CreateComputationIdentifier<
  OptionData extends BaseComputationOptionData,
  ResultType,
>(name: string): ComputationIdentifier<OptionData, ResultType> {
  return { name, type: 'computation' }
}

export type BaseActionResultType<OptionData> = {
  options: OptionData
}

export type BaseActionOptionData = {
  action?: RuleComponentIdentifier
}

export interface ActionIdentifier<
  OptionData extends BaseActionOptionData,
  ResultType extends BaseActionResultType<OptionData>,
> extends AmendedRuleComponentIdentifier<OptionData, ResultType> {
  type: 'action'
}

export function CreateActionIdentifier<
  OptionData extends BaseActionOptionData,
  ResultType extends BaseActionResultType<OptionData>,
>(name: string): ActionIdentifier<OptionData, ResultType> {
  return { name, type: 'action' }
}

export type BaseEffectOptionData<ListenedResultType> = {
  result: ListenedResultType
}

export type ListenedResultTypeOf<OptionData> =
  OptionData extends BaseEffectOptionData<infer ListenedResultType>
    ? ListenedResultType
    : never

export interface EffectIdentifier<ListenedResultType>
  extends AmendedRuleComponentIdentifier<
    BaseEffectOptionData<ListenedResultType>,
    Promise<boolean>
  > {
  type: 'effect'
}

export function CreateEffectIdentifier<ListenedResultType>(
  name: string
): EffectIdentifier<ListenedResultType> {
  return { name, type: 'effect' }
}

interface RuleExecutor<BaseOptionData, ResultType> {
  identifier: RuleComponentIdentifier
  execute<OptionData extends BaseOptionData>(options: OptionData): ResultType
  addPreHook(hook: PreHook): void
  addPostHook(hook: PostHook): void
}

abstract class RuleComponent<BaseOptionData, ResultType>
  implements RuleExecutor<BaseOptionData, ResultType>
{
  identifier: RuleComponentIdentifier
  protected preHooks: Set<PreHook>
  protected postHooks: Set<PostHook>
  private type: RuleComponentType

  protected constructor(
    identifier: RuleComponentIdentifier,
    type: RuleComponentType
  ) {
    this.type = type
    // assert(this.type === identifier.type)
    this.identifier = identifier
    this.preHooks = new Set<PreHook>()
    this.postHooks = new Set<PostHook>()
  }

  abstract execute<OptionData extends BaseOptionData>(
    options: OptionData
  ): ResultType

  addPreHook<OptionData extends BaseOptionData>(
    hook: PreHook<OptionData>
  ): void {
    this.preHooks.add(hook)
  }

  addPostHook<OptionData extends BaseOptionData>(
    hook: PostHook<OptionData, ResultType>
  ): void {
    this.postHooks.add(hook)
  }
}

export class Computation<
  ComputationOptionData extends
    BaseComputationOptionData = BaseComputationOptionData,
  ResultType = any,
> extends RuleComponent<ComputationOptionData, ResultType> {
  private func: Func<ComputationOptionData, ResultType>

  constructor(
    identifier: RuleComponentIdentifier,
    func: Func<ComputationOptionData, ResultType>
  ) {
    super(identifier, 'computation')
    this.func = func
  }

  execute<D extends ComputationOptionData>(options: D): ResultType {
    const preparedOptions = [...this.preHooks].reduce(
      (options, hook) => hook(options),
      options
    )
    let result = this.func(preparedOptions)
    result = [...this.postHooks].reduce(
      (result, hook) => hook(options, result),
      result
    )
    return result
  }
}

export interface RuleManager {
  registerComputation(computation: Computation): void
  registerComputationPreHook<
    OptionData extends BaseComputationOptionData,
    ResultType,
  >(
    identifier: ComputationIdentifier<OptionData, ResultType>,
    hook: PreHook
  ): void
  registerComputationPostHook<
    OptionData extends BaseComputationOptionData,
    ResultType,
  >(
    identifier: ComputationIdentifier<OptionData, ResultType>,
    hook: PostHook
  ): void
  registerAction<
    ActionOptionData extends BaseActionOptionData,
    ResultType extends BaseActionResultType<ActionOptionData>,
    A extends Action<ActionOptionData, ResultType>,
  >(
    action: A
  ): void
  registerEffect<ListenedResultType, E extends Effect<ListenedResultType>>(
    effect: E
  ): void
  registerActionPreHook<
    OptionData extends BaseActionOptionData,
    ResultType extends BaseActionResultType<OptionData>,
  >(
    identifier: ActionIdentifier<OptionData, ResultType>,
    hook: PreHook
  ): void
  registerActionPostHook<
    OptionData extends BaseActionOptionData,
    ResultType extends BaseActionResultType<OptionData>,
  >(
    identifier: ActionIdentifier<OptionData, ResultType>,
    hook: PostHook
  ): void
  compute<OptionData extends BaseComputationOptionData, ResultType>(
    identifier: ComputationIdentifier<OptionData, ResultType>,
    options: OptionData
  ): ResultType
  execute<
    OptionData extends BaseActionOptionData,
    ResultType extends BaseActionResultType<OptionData>,
  >(
    identifier: ActionIdentifier<OptionData, ResultType>,
    options: OptionData
  ): Promise<ResultType>
}

export abstract class Action<
  ActionOptionData extends BaseActionOptionData,
  ResultType extends BaseActionResultType<ExtendedActionOptionData>,
  ExtendedActionOptionData extends ActionOptionData = ActionOptionData,
> extends RuleComponent<ActionOptionData, Promise<ResultType>> {
  private ruleset_: RuleManager

  constructor(identifier: RuleComponentIdentifier) {
    super(identifier, 'action')
  }

  abstract _execute<OptionData extends ActionOptionData>(
    options: OptionData
  ): Promise<ResultType>

  get ruleset(): RuleManager {
    if (this.ruleset_ !== undefined) return this.ruleset_
    else return (game as any).ruleset
  }

  set ruleset(ruleset: RuleManager) {
    this.ruleset_ = ruleset
  }

  async execute<OptionData extends ActionOptionData>(
    options: OptionData
  ): Promise<ResultType> {
    options.action = this.identifier
    const preparedOptions = (await [...this.preHooks].reduce(
      (options, hook) => options.then((options) => hook(options)),
      new Promise((resolve) => resolve(options))
    )) as any
    let result = await this._execute(preparedOptions)
    result.options = {
      ...preparedOptions,
      ...result.options,
    }
    // result.action = this.identifier
    result = [...this.postHooks].reduce(
      (result, hook) => hook(preparedOptions, result),
      result
    )
    return result
  }
}

export abstract class Effect<ListenedResultType> extends RuleComponent<
  BaseEffectOptionData<ListenedResultType>,
  Promise<boolean>
> {
  listenedActions: Set<string>

  constructor(identifier: EffectIdentifier<ListenedResultType>) {
    super(identifier, 'effect')
    this.listenedActions = new Set<string>()
  }

  listenTo<
    OptionData extends BaseActionOptionData,
    ResultType extends BaseActionResultType<OptionData>,
  >(identifier: ActionIdentifier<OptionData, ResultType>): void {
    this.listenedActions.add(identifier.name)
  }

  abstract _apply<R extends ListenedResultType>(
    result: R,
    options?: any
  ): Promise<any>

  execute<OptionData extends BaseEffectOptionData<ListenedResultType>>(
    options: OptionData
  ): Promise<boolean> {
    // if (result.action?.name && this.listenedActions.has(result.action.name)) {
    const preparedOptions = [...this.preHooks].reduce(
      (options, hook) => hook(options),
      options
    )
    const result = preparedOptions.result
    // return this._apply(preparedResult)
    return this._apply(result, options).then(() => true)
    // }
    // return false
  }

  apply<R extends ListenedResultType>(result: R): Promise<boolean> {
    return this.execute({ result })
  }
}

export interface ChatEffectListenedResult {
  roll: any
}

export class ChatEffect extends Effect<ChatEffectListenedResult> {
  constructor() {
    super(CreateEffectIdentifier('chat'))
  }

  _apply<R extends ChatEffectListenedResult>(result: R): Promise<boolean> {
    result.roll.toMessage()
    return new Promise((resolve) => resolve(true))
  }
}
