import type { SpecialAbility } from '../../../model/properties.js'
import { DescribeRule } from '../../rule.js'
import { Ruleset } from '../../ruleset.js'
import { ComputeInitiative } from '../basic-combat.js'
import type { CombatComputationData } from '../derived-combat-attributes.js'
import { CharacterHas } from '../maneuvers/basic-maneuver.js'

export const CombatReflexes: SpecialAbility = {
  name: 'Kampfreflexe',
  identifier: 'ability-kampfreflexe',
}
export const CombatReflexesRule = DescribeRule(
  'combatReflexes',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeInitiative)
      .when(CharacterHas(CombatReflexes))
      .do((_options: CombatComputationData, result: number) => result + 4)
  }
)
