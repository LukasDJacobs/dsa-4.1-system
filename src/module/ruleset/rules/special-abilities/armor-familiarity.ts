import type { SpecialAbility } from '../../../model/properties.js'
import { DescribeRule } from '../../rule.js'
import { BaseComputationOptionData } from '../../rule-components.js'
import { And, Not, Or, Ruleset } from '../../ruleset.js'
import { ComputeInitiative } from '../basic-combat.js'
import { ComputeEncumbarance } from '../derived-combat-attributes.js'
import { CharacterHas } from '../maneuvers/basic-maneuver.js'

export const ArmorFamiliarityI: SpecialAbility = {
  name: 'Rüstungsgewöhnung 1',
  identifier: 'ability-ruestungsgewoehnung-i',
}
export const ArmorFamiliarityII: SpecialAbility = {
  name: 'Rüstungsgewöhnung 2',
  identifier: 'ability-ruestungsgewoehnung-ii',
}
export const ArmorFamiliarityIII: SpecialAbility = {
  name: 'Rüstungsgewöhnung 3',
  identifier: 'ability-ruestungsgewoehnung-iii',
}
export const ArmorFamiliarityRule = DescribeRule(
  'armorFamiliarity',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeEncumbarance)
      .when(
        And(
          Or(CharacterHas(ArmorFamiliarityI), CharacterHas(ArmorFamiliarityII)),
          Not(CharacterHas(ArmorFamiliarityIII))
        )
      )
      .do((options: BaseComputationOptionData, result: number) => {
        return Math.max(0, result - 1)
      })

    ruleset
      .after(ComputeEncumbarance)
      .when(CharacterHas(ArmorFamiliarityIII))
      .do((options: BaseComputationOptionData, result: number) => {
        return Math.max(0, result - 2)
      })

    ruleset
      .after(ComputeInitiative)
      .when(CharacterHas(ArmorFamiliarityIII))
      .do((options: BaseComputationOptionData, result: number) => {
        return result + Math.floor((options.character.encumbarance + 2) / 2)
      })
  }
)
