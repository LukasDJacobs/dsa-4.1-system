import { ArmorFamiliarityRule } from './armor-familiarity.js'
import { BladeDancerRule } from './blade-dancer.js'
import { CombatIntuitionRule } from './combat-intuition.js'
import { CombatReflexesRule } from './combat-reflexes.js'

export const SpecialAbilityRules = [
  ArmorFamiliarityRule,
  BladeDancerRule,
  CombatIntuitionRule,
  CombatReflexesRule,
]
