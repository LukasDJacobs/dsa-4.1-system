import type { SpecialAbility } from '../../../model/properties.js'
import { DescribeRule } from '../../rule.js'
import { Ruleset } from '../../ruleset.js'
import { ComputeInitiative } from '../basic-combat.js'
import type { CombatComputationData } from '../derived-combat-attributes.js'
import { CharacterHas } from '../maneuvers/basic-maneuver.js'

export const CombatIntuition: SpecialAbility = {
  name: 'Kampfgespür',
  identifier: 'ability-kampfgespuer',
}
export const CombatIntuitionRule = DescribeRule(
  'combatIntuition',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeInitiative)
      .when(CharacterHas(CombatIntuition))
      .do((_options: CombatComputationData, result: number) => result + 2)
  }
)
