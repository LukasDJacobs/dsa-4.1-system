import type { BaseCharacter } from '../../model/character.js'
import type { SkillDescriptor, TestAttributes } from '../../model/properties.js'
import { DescribeRule } from '../rule.js'
import {
  Action,
  BaseActionOptionData,
  BaseActionResultType,
  CreateActionIdentifier,
} from '../rule-components.js'
import type { Ruleset } from '../ruleset.js'
import {
  Messagable,
  Rollable,
  RollSkill,
  RollSkillToChatEffect,
  SkillRollActionData,
} from './basic-roll-mechanic.js'

export interface SkillActionData extends BaseActionOptionData {
  skill: SkillDescriptor
  character: BaseCharacter
  mod?: number
  testAttributes?: TestAttributes
}

export interface SkillActionResult
  extends BaseActionResultType<SkillActionData & SkillRollActionData> {
  mod: number
  roll: Rollable & Messagable
  success: boolean
  damage?: string
}

export const TalentAction = CreateActionIdentifier<
  SkillActionData,
  SkillActionResult
>('talent')
export const SpellAction = CreateActionIdentifier<
  SkillActionData,
  SkillActionResult
>('spell')
export const LiturgyAction = CreateActionIdentifier<
  SkillActionData,
  SkillActionResult
>('liturgy')

export class SkillAction extends Action<SkillActionData, SkillActionResult> {
  async _execute<OptionData extends SkillActionData>(
    options: OptionData
  ): Promise<SkillActionResult> {
    let skill
    switch (options.skill.skillType) {
      case 'talent':
        skill = options.character.talent(options.skill.identifier)
        break
      case 'spell':
        skill = options.character.spell(options.skill.identifier)
        break
      case 'liturgy':
        skill = options.character.liturgy(options.skill.identifier)
        break
    }
    const testAttributes = options.testAttributes
      ? options.testAttributes
      : skill.testAttributes
    const testAttributeData = testAttributes.map((attributeName) => {
      const attribute = options.character.attribute(attributeName)
      return { name: attribute.name, value: attribute.value }
    })
    const rollResultPromise = this.ruleset.execute(RollSkill, {
      ...options,
      skillName: options.skill.name,
      skillType: options.skill.skillType,
      skillValue: skill.value,
      testAttributeData,
      mod: options.mod || 0,
    })
    return rollResultPromise.then((rollResult) => ({
      mod: options.mod || 0,
      roll: rollResult.roll,
      success: rollResult.success,
      options: {
        ...options,
        ...rollResult.options,
      },
    }))
  }
}

export const BasicSkillRule = DescribeRule(
  'basic-skill-rule',
  {
    changeable: false,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset.on(TalentAction).do(SkillAction)
    ruleset.after(TalentAction).trigger(RollSkillToChatEffect)
    ruleset.on(SpellAction).do(SkillAction)
    ruleset.after(SpellAction).trigger(RollSkillToChatEffect)
    ruleset.on(LiturgyAction).do(SkillAction)
    ruleset.after(LiturgyAction).trigger(RollSkillToChatEffect)
  }
)
