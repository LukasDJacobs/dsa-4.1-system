import { AttributeName } from '../../model/character-data.js'
import type { ModifierDescriptor } from '../../model/modifier.js'
import type { SkillType } from '../../model/properties.js'
import { getGame } from '../../utils.js'
import {
  Action,
  Effect,
  CreateActionIdentifier,
  CreateEffectIdentifier,
} from '../rule-components.js'
import type {
  ActionIdentifier,
  EffectIdentifier,
  BaseActionOptionData,
  BaseActionResultType,
} from '../rule-components.js'
import type { Ruleset } from '../ruleset.js'
import type { CombatActionData } from './basic-combat.js'
import type { SkillActionResult } from './basic-skill.js'
import type { DamageFormula } from './derived-combat-attributes.js'

export interface BasicRollActionData extends BaseActionOptionData {
  formula: string
  mod?: number
  modifiers?: ModifierDescriptor[]
}

export interface BasicRollActionResult
  extends BaseActionResultType<BasicRollActionData> {
  roll: Rollable & Messagable
}

export interface AttributeRollActionData extends BaseActionOptionData {
  attributeName: string
  isLocalized?: boolean
  checkCritical?: boolean
  mod?: number
  targetValue: number
}

export interface AttributeActionResult<
  OptionData extends AttributeRollActionData = AttributeRollActionData,
> extends BaseActionResultType<OptionData> {
  success: boolean
  critical?: boolean
  roll: Rollable & Messagable
  damage?: DamageFormula
  penality?: number
}

export interface AttributeRollActionResult<
  OptionData extends AttributeRollActionData = AttributeRollActionData,
> extends BaseActionResultType<OptionData> {
  roll: Rollable & Messagable
  success: boolean
  critical?: boolean
  damage?: DamageFormula
  penality?: number
  additionalFlavor?: string
}

export const BasicRoll = CreateActionIdentifier<
  BasicRollActionData,
  BasicRollActionResult
>('basicRoll')

export const RollAttribute = CreateActionIdentifier<
  AttributeRollActionData,
  AttributeRollActionResult
>('rollAttribute')
export const RollCombatAttribute = CreateActionIdentifier<
  AttributeRollActionData & CombatActionData,
  AttributeRollActionResult<AttributeRollActionData & CombatActionData>
>('rollCombatAttribute')

export interface SkillRollActionData extends BaseActionOptionData {
  mod: number
  skillName: string
  skillValue: number
  skillType: SkillType
  testAttributeData: SkillCheckData
  modifiers?: ModifierDescriptor[]
}

export interface SkillRollActionResult
  extends BaseActionResultType<SkillRollActionData> {
  testAttributeData?: SkillCheckData
  skillType?: string
  roll: Rollable & Messagable
  success: boolean
  damage?: string
}

export const RollSkill = CreateActionIdentifier<
  SkillRollActionData,
  SkillRollActionResult
>('rollSkill')

export const BasicRollToChatEffect =
  CreateEffectIdentifier<BasicRollActionResult>('basicRollToChat')
export const RollAttributeToChatEffect =
  CreateEffectIdentifier<AttributeRollActionResult>('rollAttributeToChat')
export const RollSkillToChatEffect =
  CreateEffectIdentifier<SkillActionResult>('rollSkillToChat')

export interface Localizer {
  localize(stringId: string): string
}

export type ExtendedRollData<D> = D & {
  is3d20Roll?: boolean
  skillType?: string
  skillCheckData?: SkillCheckData
  damage?: string
  penality?: number
  modifiers?: ModifierDescriptor[]
}

export type MessageData<
  T extends DeepPartial<ConstructorParameters<typeof ChatMessage>[0]>,
> = {
  user: string
  type: (typeof foundry.CONST.CHAT_MESSAGE_TYPES)['ROLL']
  content: number
  sound: typeof CONFIG.sounds.dice
} & T

export interface Messagable<
  D extends Record<string, unknown> = Record<string, unknown>,
> {
  // toMessage<T extends object = {}>(
  //   messageData: T,
  //   {
  //     rollMode,
  //     create,
  //   }?: { rollMode?: Const.DiceRollMode | null; create?: true }
  // ): Promise<ChatMessage>
  toMessage<T extends Record<string, unknown> = Record<string, unknown>>(
    messageData: T,
    {
      rollMode,
      create,
    }: { rollMode?: foundry.CONST.DiceRollMode | null; create: false }
  ): Promise<MessageData<T>>

  data: ExtendedRollData<D>
}
export interface RollMessageCreator {
  create(data: Parameters<Messagable['toMessage']>[0])
}

type SkillCheckDataEntry = { name: AttributeName; value: number }
export type SkillCheckData = [
  SkillCheckDataEntry,
  SkillCheckDataEntry,
  SkillCheckDataEntry,
]

interface Dice {
  critical?: boolean
  success?: boolean
  secondRoll?: number
}
export interface Rollable {
  roll(options: any): Rollable & Messagable
  total?: number
  dice: Dice[]
}

export interface RollFactory {
  create<D extends Record<string, unknown> = Record<string, unknown>>(
    formula: string,
    data?: D
  ): Rollable & Messagable
}

export interface SpeakerProvider {
  getSpeaker(
    speaker?: Parameters<typeof ChatMessage.getSpeaker>[0]
  ): foundry.data.ChatMessageData['speaker']['_source']
}

export function totalModifier(modifiers: ModifierDescriptor[]) {
  return modifiers.reduce((prev, current) => prev + current.mod, 0)
}

class BasicRollAction extends Action<
  BasicRollActionData,
  BasicRollActionResult
> {
  private rollFactory: RollFactory

  constructor(
    identifier: ActionIdentifier<BasicRollActionData, BasicRollActionResult>,
    rollFactory: RollFactory
  ) {
    super(identifier)
    this.rollFactory = rollFactory
  }

  async _execute<OptionData extends BasicRollActionData>(
    options: OptionData
  ): Promise<BasicRollActionResult> {
    if (options.mod === undefined && options.modifiers !== undefined) {
      options.mod = totalModifier(options.modifiers)
    }
    const roll = this.rollFactory
      .create(`${options.formula} + ${options.mod || 0}`)
      .roll({ async: false })
    return {
      roll,
      options,
    }
  }
}

class AttributeRollAction extends Action<
  AttributeRollActionData,
  AttributeRollActionResult
> {
  private rollFactory: RollFactory

  constructor(
    identifier: ActionIdentifier<
      AttributeRollActionData,
      AttributeRollActionResult
    >,
    rollFactory: RollFactory
  ) {
    super(identifier)
    this.rollFactory = rollFactory
  }

  async _execute<OptionData extends AttributeRollActionData>(
    options: OptionData
  ): Promise<AttributeRollActionResult> {
    const modifier = options.checkCritical ? 'cc' : ''
    const roll = this.rollFactory
      .create(`1dz${modifier} + ${options.mod || 0}`)
      .roll({ async: false })
    const total = roll.total || 0
    const success = roll.dice[0].success ?? total <= options.targetValue
    return {
      roll,
      success,
      options,
    }
  }
}

class SkillRollAction extends Action<
  SkillRollActionData,
  SkillRollActionResult
> {
  private rollFactory: RollFactory

  constructor(
    identifier: ActionIdentifier<SkillRollActionData, SkillRollActionResult>,
    rollFactory: RollFactory
  ) {
    super(identifier)
    this.rollFactory = rollFactory
  }

  async _execute<OptionData extends SkillRollActionData>(
    options: OptionData
  ): Promise<SkillRollActionResult> {
    const valueModDiff = options.skillValue - options.mod
    const rollMod = Math.min(valueModDiff, 0)
    const totalMod = Math.max(valueModDiff, 0)
    let rollFormula = 'min('
    for (const attribute of options.testAttributeData) {
      rollFormula += `min((${attribute.value}-1d20+${rollMod}),0) + `
    }
    rollFormula += `${totalMod}`
    rollFormula += `,${options.skillValue})`
    const roll = this.rollFactory.create(rollFormula).roll({ async: false })
    const total = roll.total || 0
    const success = total >= 0
    return {
      roll,
      success,
      options,
    }
  }
}

export class BasicRollChatEffect extends Effect<BasicRollActionResult> {
  private speakerProvider: SpeakerProvider
  private messageCreator: RollMessageCreator
  private localizer: Localizer

  constructor(
    identifier: EffectIdentifier<BasicRollActionResult>,
    speakerProvider: SpeakerProvider,
    messageCreator: RollMessageCreator,
    localizer: Localizer
  ) {
    super(identifier)
    this.speakerProvider = speakerProvider
    this.messageCreator = messageCreator
    this.localizer = localizer
  }

  _apply<R extends BasicRollActionResult>(result: R): Promise<any> {
    const chatDataPromise = result.roll.toMessage(
      {
        speaker: this.speakerProvider.getSpeaker(),
        flavor: '',
      },
      {
        create: false,
      }
    )

    chatDataPromise.then((chatData) => {
      this.messageCreator.create(chatData)
    })
    return chatDataPromise
  }
}

export class AttributeRollChatEffect extends Effect<AttributeActionResult> {
  private speakerProvider: SpeakerProvider
  private messageCreator: RollMessageCreator
  private localizer: Localizer

  constructor(
    identifier: EffectIdentifier<AttributeActionResult>,
    speakerProvider: SpeakerProvider,
    messageCreator: RollMessageCreator,
    localizer: Localizer
  ) {
    super(identifier)
    this.speakerProvider = speakerProvider
    this.messageCreator = messageCreator
    this.localizer = localizer
  }

  _apply<R extends AttributeActionResult>(result: R): Promise<any> {
    const successInfo = result.success ? 'success' : 'failed'
    const name = result.options.isLocalized
      ? result.options.attributeName
      : this.localizer.localize(`DSA.${result.options.attributeName}`)
    let flavor = name + ` (${result.options.targetValue}) `
    if (result.critical) {
      flavor += `${this.localizer.localize(`DSA.critical`)} `
    }
    flavor += this.localizer.localize(`DSA.${successInfo}`)

    result.roll.data.damage = result.damage?.formula
    result.roll.data.penality = result.penality

    const chatDataPromise = result.roll.toMessage(
      {
        speaker: this.speakerProvider.getSpeaker(),
        flavor,
        // additionalFlavor: result.options.additionalFlavor,
        // additionalContent: result.options.additionalContent,
      },
      {
        create: false,
      }
    )

    chatDataPromise.then((chatData) => {
      this.messageCreator.create(chatData)
    })
    return chatDataPromise
  }
}

export class SkillRollChatEffect extends Effect<SkillActionResult> {
  private speakerProvider: SpeakerProvider
  private messageCreator: RollMessageCreator
  private localizer: Localizer

  constructor(
    identifier: EffectIdentifier<SkillActionResult>,
    speakerProvider: SpeakerProvider,
    messageCreator: RollMessageCreator,
    localizer: Localizer
  ) {
    super(identifier)
    this.speakerProvider = speakerProvider
    this.messageCreator = messageCreator
    this.localizer = localizer
  }

  _apply<R extends SkillActionResult>(result: R): Promise<any> {
    const successInfo = result.success ? 'success' : 'failed'
    const flavor =
      `${result.options.skillName} +${result.mod} (${result.options.skillValue}) ` +
      this.localizer.localize(`DSA.${successInfo}`)

    result.roll.data.is3d20Roll = true
    result.roll.data.skillCheckData = result.options.testAttributeData
    result.roll.data.skillType = result.options.skillType
    result.roll.data.damage = result.damage

    const chatDataPromise = result.roll.toMessage(
      {
        speaker: this.speakerProvider.getSpeaker(),
        flavor,
      },
      {
        create: false,
      }
    )
    chatDataPromise.then((chatData) => {
      this.messageCreator.create(chatData)
    })
    return chatDataPromise
  }
}

export function BasicRollMechanicRule(
  ruleset: Ruleset,
  rollFactory: RollFactory = Roll,
  speakerProvider: SpeakerProvider = ChatMessage,
  messageCreator: RollMessageCreator = ChatMessage,
  localizer: Localizer = getGame().i18n
): void {
  ruleset.registerAction<
    BasicRollActionData,
    BasicRollActionResult,
    BasicRollAction
  >(new BasicRollAction(BasicRoll, rollFactory))

  ruleset.registerAction<
    AttributeRollActionData,
    AttributeRollActionResult,
    AttributeRollAction
  >(new AttributeRollAction(RollAttribute, rollFactory))

  ruleset.registerAction<
    SkillRollActionData,
    SkillRollActionResult,
    SkillRollAction
  >(new SkillRollAction(RollSkill, rollFactory))

  ruleset.registerAction<
    AttributeRollActionData,
    AttributeRollActionResult,
    AttributeRollAction
  >(new AttributeRollAction(RollCombatAttribute, rollFactory))

  ruleset.before(RollCombatAttribute).do((options) => {
    options.checkCritical = true
    return options
  })

  ruleset.after(RollCombatAttribute).do((_options, result) => {
    const success = result.success
    const secondRoll = result.roll.dice[0]?.secondRoll
    result.critical = secondRoll
      ? success === secondRoll <= result.options.targetValue
      : undefined
    return result
  })

  ruleset.after(RollCombatAttribute).do((_options, result) => {
    if (result.options.modifiers) {
      result.roll.data.modifiers = result.options.modifiers.filter(
        (modifier) => modifier.name !== 'custom'
      )
    }
    return result
  })

  ruleset.after(RollSkill).do((_options, result) => {
    if (result.options.modifiers) {
      result.roll.data.modifiers = result.options.modifiers.filter(
        (modifier) => modifier.name !== 'custom'
      )
    }
    return result
  })

  ruleset.registerEffect<BasicRollActionResult, BasicRollChatEffect>(
    new BasicRollChatEffect(
      BasicRollToChatEffect,
      speakerProvider,
      messageCreator,
      localizer
    )
  )

  ruleset.registerEffect<AttributeActionResult, AttributeRollChatEffect>(
    new AttributeRollChatEffect(
      RollAttributeToChatEffect,
      speakerProvider,
      messageCreator,
      localizer
    )
  )
  ruleset.after(RollAttribute).trigger(RollAttributeToChatEffect)

  ruleset.registerEffect<SkillActionResult, SkillRollChatEffect>(
    new SkillRollChatEffect(
      RollSkillToChatEffect,
      speakerProvider,
      messageCreator,
      localizer
    )
  )
}
