import { Maneuver } from '../../../character/maneuver.js'
import type { SpecialAbility } from '../../../model/properties.js'

import { createManeuverRule } from './maneuver-creator.js'

export const HammerStrike: SpecialAbility = {
  name: 'Hammerschlag',
  identifier: 'ability-hammerschlag',
}

export const HammerStrikeManeuver = new Maneuver('hammerStrike', 'offensive', {
  minMod: 8,
})
export const HammerStrikeRule = createManeuverRule(
  HammerStrike,
  HammerStrikeManeuver
)
