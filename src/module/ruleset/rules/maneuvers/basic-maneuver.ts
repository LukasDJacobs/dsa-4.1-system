import type {
  ManeuverDescriptor,
  ManeuverType,
  ModifierDescriptor,
} from '../../../model/modifier.js'
import type { BaseProperty } from '../../../model/properties.js'
import { DescribeRule } from '../../rule.js'
import { CreateComputationIdentifier } from '../../rule-components.js'
import type { BaseComputationOptionData } from '../../rule-components.js'
import type { Ruleset } from '../../ruleset.js'
import { AttackAction, DodgeAction, ParryAction } from '../basic-combat.js'
import type { CombatActionData, CombatActionResult } from '../basic-combat.js'
import { RangedAttackAction } from '../basic-ranged-combat.js'
import {
  CombatComputationData,
  CombatComputationResult,
  ComputeAttack,
  ComputeParry,
  ComputeRangedAttack,
} from '../derived-combat-attributes.js'

export interface ManeuverListOptions extends BaseComputationOptionData {
  isArmed?: boolean
  maneuverType: ManeuverType
}
export interface ManeuverListResult {
  maneuvers: ManeuverDescriptor[]
}

export const ComputeManeuverList = CreateComputationIdentifier<
  ManeuverListOptions,
  ManeuverListResult
>('maneuverList')

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function CreateManeuverList(_options: ManeuverListOptions): ManeuverListResult {
  return {
    maneuvers: [],
  }
}

function IsFailed(_options: CombatActionData, result?: CombatActionResult) {
  return result?.success === false
}

export function IsOffensiveManeuver(options: ManeuverListOptions): boolean {
  return options.maneuverType === 'offensive'
}

export function IsDefensiveManeuver(options: ManeuverListOptions): boolean {
  return options.maneuverType === 'defensive'
}

export function IsArmedManeuver(options: ManeuverListOptions): boolean {
  return options.isArmed === undefined ? true : options.isArmed
}

export function CharacterHas<OptionData extends BaseComputationOptionData>(
  property: BaseProperty
) {
  return (options: OptionData): boolean => options.character.has(property)
}

export function AddManeuver(maneuver: ManeuverDescriptor) {
  return (
    _options: ManeuverListOptions,
    result: ManeuverListResult
  ): ManeuverListResult => {
    result.maneuvers.push(maneuver)
    return result
  }
}

function DeterminePenality(
  options: CombatActionData,
  result: CombatActionResult
) {
  const maneuvers: ManeuverDescriptor[] = (options.modifiers || [])
    .filter(
      (modifier: ModifierDescriptor) => modifier.modifierType === 'maneuver'
    )
    .map((modifier) => modifier as ManeuverDescriptor)

  if (maneuvers.length > 0) {
    result.penality = maneuvers.reduce(
      (previous: number, current) => previous + current.mod,
      0
    )
  }

  return result
}

function ComputeTotalModifier<
  OptionData extends CombatActionData = CombatActionData,
>(options: OptionData) {
  const totalMod = options.modifiers
    ? options.modifiers.reduce((prev, current) => prev + current.mod, 0)
    : 0
  return {
    ...options,
    mod: totalMod,
  }
}

function AddAppliableManeuverModifiers(
  options: CombatComputationData,
  result: CombatComputationResult
) {
  const maneuverModifiers = options.character.maneuverModifiers
  if (options.maneuvers !== undefined) {
    maneuverModifiers.forEach((maneuverModifier) => {
      if (
        options.maneuvers?.some(
          (maneuver) => maneuver.name === maneuverModifier.name
        )
      ) {
        result.modifiers?.push({
          name: maneuverModifier.name,
          mod: maneuverModifier.value,
          modifierType: 'maneuverModifier',
          source: maneuverModifier.source,
        })
      }
    })
  }
  return result
}

export const BasicManeuverRule = DescribeRule(
  'basic-maneuver',
  {
    changeable: false,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    const combatActions = [
      AttackAction,
      ParryAction,
      RangedAttackAction,
      DodgeAction,
    ] as const
    combatActions.forEach((action) => {
      ruleset.before(action).do(ComputeTotalModifier)
    })

    const attackParryActions = [AttackAction, ParryAction] as const
    attackParryActions.forEach((action) => {
      ruleset.after(action).when(IsFailed).do(DeterminePenality)
    })

    const computeActions = [
      ComputeAttack,
      ComputeParry,
      ComputeRangedAttack,
    ] as const
    computeActions.forEach((action) => {
      ruleset.after(action).do(AddAppliableManeuverModifiers)
    })

    ruleset.on(ComputeManeuverList).do(CreateManeuverList)
  }
)
