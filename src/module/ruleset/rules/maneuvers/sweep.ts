import { Maneuver } from '../../../character/maneuver.js'
import type { SpecialAbility } from '../../../model/properties.js'

import { createManeuverRule } from './maneuver-creator.js'

export const Sweep: SpecialAbility = {
  name: 'Umreißen',
  identifier: 'ability-umreissen',
}

export const SweepManeuver = new Maneuver('sweep', 'offensive', {
  minMod: 4,
})
export const SweepRule = createManeuverRule(Sweep, SweepManeuver)
