import { Maneuver } from '../../../character/maneuver.js'
import type { SpecialAbility } from '../../../model/properties.js'

import { createManeuverRule } from './maneuver-creator.js'

export const Disarm: SpecialAbility = {
  name: 'Entwaffnen',
  identifier: 'ability-entwaffnen',
}

export const DisarmManeuver = new Maneuver('disarm', 'defensive', {
  minMod: 8,
})
export const DisarmRule = createManeuverRule(Disarm, DisarmManeuver)
