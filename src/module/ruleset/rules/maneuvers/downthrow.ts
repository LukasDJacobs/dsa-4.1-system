import { Maneuver } from '../../../character/maneuver.js'
import type { SpecialAbility } from '../../../model/properties.js'

import { createManeuverRule } from './maneuver-creator.js'

export const Downthrow: SpecialAbility = {
  name: 'Niederwerfen',
  identifier: 'ability-niederwerfen',
}

export const DownthrowManeuver = new Maneuver('downthrow', 'offensive', {
  minMod: 4,
})

export const DownthrowRule = createManeuverRule(Downthrow, DownthrowManeuver)
