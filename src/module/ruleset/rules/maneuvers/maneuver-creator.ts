import { Maneuver } from '../../../character/maneuver.js'
import type { SpecialAbility } from '../../../model/properties.js'
import { DescribeRule } from '../../rule.js'
import type { RuleDescriptor } from '../../rule.js'
import { Ruleset } from '../../ruleset.js'
import {
  IsOffensiveManeuver,
  IsDefensiveManeuver,
  ComputeManeuverList,
  IsArmedManeuver,
  CharacterHas,
  AddManeuver,
} from './basic-maneuver.js'

export function createManeuverRule(
  neededAbility: SpecialAbility,
  maneuver: Maneuver,
  checkForAbility = true
): RuleDescriptor {
  const typePrecondition = {
    offensive: IsOffensiveManeuver,
    defensive: IsDefensiveManeuver,
  }[maneuver.type]
  return DescribeRule(
    maneuver.name,
    {
      changeable: true,
      enabled: true,
    },
    (ruleset: Ruleset): void => {
      ruleset
        .after(ComputeManeuverList)
        .when(typePrecondition)
        .when(IsArmedManeuver)
        .when((options) =>
          checkForAbility ? CharacterHas(neededAbility)(options) : true
        )
        .do(AddManeuver(maneuver))
    }
  )
}
