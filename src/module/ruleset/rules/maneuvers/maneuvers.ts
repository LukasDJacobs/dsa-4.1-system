import { ManeuverDescriptor } from '../../../model/modifier.js'
import { BindManeuver, BindRule } from './bind.js'
import { BladeStormManeuver, BladeStormRule } from './blade-storm.js'
import { ClearanceManeuver, ClearanceRule } from './clearance.js'
import { DeathBlowManeuver, DeathBlowRule } from './death-blow.js'
import {
  DefensiveStyleManeuver,
  DefensiveStyleRule,
} from './defensive-style.js'
import { DisarmManeuver, DisarmRule } from './disarm.js'
import { DownthrowManeuver, DownthrowRule } from './downthrow.js'
import { FeintManeuver, FeintRule } from './feint.js'
import { HammerStrikeManeuver, HammerStrikeRule } from './hammer-strike.js'
import { HoldupManeuver, HoldupRule } from './holdup.js'
import { MasterparryManeuver, MasterparryRule } from './masterparry.js'
import { MightyStrikeManeuver, MightyStrikeRule } from './mighty-strike.js'
import { SallyManeuver, SallyRule } from './sally.js'
import {
  ShieldSplitterManeuver,
  ShieldSplitterRule,
} from './shield-splitter.js'
import { SweepManeuver, SweepRule } from './sweep.js'
import { TargetedStabManeuver, TargetedStabRule } from './targeted-stab.js'

export const Maneuvers: ManeuverDescriptor[] = [
  BindManeuver,
  BladeStormManeuver,
  ClearanceManeuver,
  DeathBlowManeuver,
  DefensiveStyleManeuver,
  DisarmManeuver,
  DownthrowManeuver,
  FeintManeuver,
  HammerStrikeManeuver,
  HoldupManeuver,
  MasterparryManeuver,
  MightyStrikeManeuver,
  SallyManeuver,
  ShieldSplitterManeuver,
  SweepManeuver,
  TargetedStabManeuver,
]

export const ManeuverRules = [
  BindRule,
  BladeStormRule,
  ClearanceRule,
  DeathBlowRule,
  DefensiveStyleRule,
  DisarmRule,
  DownthrowRule,
  FeintRule,
  HammerStrikeRule,
  HoldupRule,
  MasterparryRule,
  MightyStrikeRule,
  SallyRule,
  ShieldSplitterRule,
  SweepRule,
  TargetedStabRule,
]
