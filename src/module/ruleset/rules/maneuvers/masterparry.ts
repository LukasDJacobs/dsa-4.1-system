import { Maneuver } from '../../../character/maneuver.js'
import type { SpecialAbility } from '../../../model/properties.js'

import { createManeuverRule } from './maneuver-creator.js'

export const Masterparry: SpecialAbility = {
  name: 'Meisterparade',
  identifier: 'ability-meisterparade',
}

export const MasterparryManeuver = new Maneuver('masterparry', 'defensive')
export const MasterparryRule = createManeuverRule(
  Masterparry,
  MasterparryManeuver
)
