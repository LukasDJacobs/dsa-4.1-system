import { Maneuver } from '../../../character/maneuver.js'
import type { SpecialAbility } from '../../../model/properties.js'

import { createManeuverRule } from './maneuver-creator.js'

export const BladeStorm: SpecialAbility = {
  name: 'Klingensturm',
  identifier: 'ability-klingensturm',
}

export const BladeStormManeuver = new Maneuver('bladeStorm', 'offensive', {
  minMod: 0,
})
export const BladeStormRule = createManeuverRule(BladeStorm, BladeStormManeuver)
