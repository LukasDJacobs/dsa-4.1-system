import { getGame } from './utils.js'

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace ClientSettings {
    interface Values {
      'dsa-41.combat-talent-pack': string
      'dsa-41.lcd-page-offset': number
      'dsa-41.ll-page-offset': number
      'dsa-41.myranor': boolean
    }
  }
}

export const registerSettings = function (): void {
  const game = getGame()

  const packChoices = {}
  game.packs.forEach((p) => {
    packChoices[
      p.collection
    ] = `${p.metadata.label} (${p.metadata.packageName})`
  })

  game.settings.register('dsa-41', 'combat-talent-pack', {
    name: game.i18n.localize('DSA.settings.combatTalentPack'),
    hint: game.i18n.localize('DSA.settings.combatTalentPackHint'),
    scope: 'world',
    config: true,
    type: String,
    choices: packChoices,
    default: 'dsa-41.combattalent',
  })

  game.settings.register('dsa-41', 'lcd-page-offset', {
    name: game.i18n.localize('DSA.settings.lcdPageOffset'),
    hint: game.i18n.localize('DSA.settings.lcdPageOffsetHint'),
    scope: 'world',
    config: true,
    type: Number,
    default: 0,
  })

  game.settings.register('dsa-41', 'll-page-offset', {
    name: game.i18n.localize('DSA.settings.llPageOffset'),
    hint: game.i18n.localize('DSA.settings.llPageOffsetHint'),
    scope: 'world',
    config: true,
    type: Number,
    default: 0,
  })

  game.settings.register('dsa-41', 'myranor', {
    name: `dsa-41.settings.myranor`,
    hint: `dsa-41.settings.myranorHint`,
    scope: 'world',
    config: true,
    type: Boolean,
    requiresReload: true,
    default: false,
  })
}
