import { SkillType } from './enums.js'
import { getGame } from './utils.js'
import type { ExtendedRollData } from './ruleset/rules/basic-roll-mechanic.js'

const KeepLeftSkillPointsI18N = {
  [SkillType.Talent]: 'DSA.keepLeftTalentPoints',
  [SkillType.Spell]: 'DSA.keepLeftSpellPoints',
  [SkillType.Ritual]: 'DSA.keepLeftRitualPoints',
  [SkillType.Karma]: 'DSA.keepLeftKarmaPoints',
  [SkillType.Liturgy]: 'DSA.keepLeftKarmaPoints',
}

class NicerDsaRoll<
  D extends Record<string, unknown> = Record<string, unknown>,
> extends Roll<D> {
  declare data: ExtendedRollData<D>

  constructor(formula: string, data?: D) {
    super(formula, data)
  }

  async render(chatOptions: any = {}) {
    const game = getGame()
    const template = this.data?.is3d20Roll
      ? 'systems/dsa-41/templates/chat/nicer-roll.html'
      : 'systems/dsa-41/templates/chat/roll.html'

    chatOptions = mergeObject(
      {
        user: game.user?.id,
        flavor: null,
        template,
        blind: false,
      },
      chatOptions
    )
    const isPrivate = chatOptions.isPrivate
    // Execute the roll, if needed
    if (!this._evaluated) this.roll()
    // Define chat data
    const chatData = {
      formula: isPrivate ? '???' : this._formula,
      flavor: isPrivate ? null : chatOptions.flavor,
      user: chatOptions.user,
      tooltip: isPrivate ? '' : await this.getTooltip(),
      total: isPrivate ? '?' : Math.round((this.total || 0) * 100) / 100,
      keepLeftName: '',
      rolls: null,
      damage: this.data.damage,
      penality: this.data.penality,
      modifiers: this.data.modifiers,
    }
    if (this.data?.is3d20Roll) {
      chatData.keepLeftName = game.i18n.localize(
        this.data.skillType ? KeepLeftSkillPointsI18N[this.data.skillType] : ''
      )
      chatData.rolls = this.dice.map((d, i) => {
        let r = d.results[0]
        r = r.result as any
        const skillCheckData = this.data?.skillCheckData
        if (skillCheckData) {
          return {
            total: d.total,
            result: d.getResultLabel(r),
            target: skillCheckData[i].value,
            attributeName: game.i18n.localize(
              `DSA.${skillCheckData[i].name}_abbr`
            ),
          }
        }
      }) as any
    }
    // Render the roll display template
    const html = renderTemplate(chatOptions.template, chatData) as Promise<any>
    return html
  }

  toJSON() {
    return {
      ...super.toJSON(),
      data: this.data,
    }
  }

  static fromData<T extends Roll>(data): T {
    const roll = super.fromData(data) as T
    roll.data = data.data
    return roll
  }
}

CONFIG.Dice.rolls.unshift(NicerDsaRoll)

function onDamageRoll(event) {
  const button = event.currentTarget
  const damageFormula = $(button).data('damageFormula')

  Roll.create(damageFormula).toMessage()
}

Hooks.on('renderChatMessage', (app, html) => {
  html.find('.damage-roll button').click(onDamageRoll)
})
