export const preloadHandlebarsTemplates = async function () {
  const templatePaths = [
    //Actor partials
    'systems/dsa-41/templates/partials/character-tab.html',
    'systems/dsa-41/templates/partials/talents-tab.html',
    'systems/dsa-41/templates/partials/combat-tab.html',
    'systems/dsa-41/templates/partials/magic-tab.html',
    'systems/dsa-41/templates/partials/karma-tab.html',
    'systems/dsa-41/templates/partials/inventory-tab.html',
    //Actor talents partials
    'systems/dsa-41/templates/partials/talents/combat-tab.html',
    'systems/dsa-41/templates/partials/talents/physical-tab.html',
    'systems/dsa-41/templates/partials/talents/social-tab.html',
    'systems/dsa-41/templates/partials/talents/nature-tab.html',
    'systems/dsa-41/templates/partials/talents/knowledge-tab.html',
    'systems/dsa-41/templates/partials/talents/crafting-tab.html',
    'systems/dsa-41/templates/partials/talents/language-tab.html',
    'systems/dsa-41/templates/partials/talents/gift-tab.html',
    //Actor inventory partials
    'systems/dsa-41/templates/partials/inventory/item-tab.html',
    'systems/dsa-41/templates/partials/inventory/melee-weapons-tab.html',
    'systems/dsa-41/templates/partials/inventory/ranged-weapons-tab.html',
    'systems/dsa-41/templates/partials/inventory/armor-tab.html',
    'systems/dsa-41/templates/partials/inventory/shield-tab.html',
  ]

  await loadTemplates(templatePaths)
  await loadTemplates({
    combatAttribute:
      'systems/dsa-41/templates/partials/combat/combat-attribute.html',
  })
}
