import {
  BaseProperty,
  BaseTalent,
  CombatTalent,
  NamedAttribute,
  Rollable,
  SpecialAbility,
  Spell,
  Liturgy,
  Talent,
  Advantage,
  Disadvantage,
  BaseSkill,
} from '../model/properties.js'
import type { Armor, RollFormula, Shield, Weapon } from '../model/items.js'
import type {
  ArmedMeleeCombatOptions,
  BaseCharacter,
  BaseCombatOptions,
  CharacterDataAccessor,
  MeleeCombatOptions,
  RangedCombatOptions,
} from '../model/character.js'
import type { Ruleset } from '../ruleset/ruleset.js'
import {
  CombatComputationData,
  CombatComputationResult,
  ComputeArmorClass,
  ComputeAttack,
  ComputeDamageFormula,
  ComputeDodge,
  ComputeEffectiveEncumbarance,
  ComputeEncumbarance,
  ComputeParry,
  ComputeRangedAttack,
} from '../ruleset/rules/derived-combat-attributes.js'

import {
  AttackAction,
  CombatActionData,
  CombatActionResult,
  ComputeInitiativeFormula,
  DamageAction,
  DodgeAction,
  InitiativeFormula,
  ParryAction,
} from '../ruleset/rules/basic-combat.js'

import { ComputeDegreeModifier } from '../ruleset/rules/basic-karma.js'
import { RangedAttackAction } from '../ruleset/rules/basic-ranged-combat.js'
import { ComputeManeuverList } from '../ruleset/rules/maneuvers/basic-maneuver.js'
import type {
  ManeuverDescriptor,
  ManeuverModifier,
  ManeuverType,
  ModifierDescriptor,
} from '../model/modifier.js'
import { getGame } from '../utils.js'
import {
  ActionIdentifier,
  ComputationIdentifier,
} from '../ruleset/rule-components.js'
import {
  GenericLiturgy,
  GenericSpell,
  isBaseTalent,
  isTalent,
  Skill,
} from './skill.js'
import { GenericWeapon } from './weapon.js'
import { GenericShield } from './shield.js'
import { DataAccessor, WeaponItemType } from '../model/item-data.js'
import { AttributeName, WoundThresholds } from '../model/character-data.js'
import { Attribute } from './attribute.js'

type CombatIdentifier = 'attack' | 'parry' | 'ranged' | 'dodge' | 'damage'

function actionIdentifierByName(
  action: CombatIdentifier
): ActionIdentifier<CombatActionData, CombatActionResult> {
  return {
    attack: AttackAction,
    parry: ParryAction,
    ranged: RangedAttackAction,
    dodge: DodgeAction,
    damage: DamageAction,
  }[action]
}

function computationIdentifierByName(
  action: CombatIdentifier
):
  | ComputationIdentifier<CombatComputationData, CombatComputationResult>
  | undefined {
  return {
    attack: ComputeAttack,
    parry: ComputeParry,
    ranged: ComputeRangedAttack,
    dodge: ComputeDodge,
    damage: undefined,
  }[action]
}

export class Character implements BaseCharacter {
  private ruleset: Ruleset
  data: CharacterDataAccessor

  constructor(data: CharacterDataAccessor, ruleset?: Ruleset) {
    if (ruleset) {
      this.ruleset = ruleset
    } else {
      this.ruleset = getGame().ruleset
    }
    this.data = data
  }

  attribute(name: AttributeName): Rollable<NamedAttribute> {
    return new Attribute(this.data.system, name, this.ruleset)
  }

  get baseAttack(): number {
    return this.data.system.base.combatAttributes.active.baseAttack.value
  }

  get baseParry(): number {
    return this.data.system.base.combatAttributes.active.baseParry.value
  }

  get baseRangedAttack(): number {
    return this.data.system.base.combatAttributes.active.baseRangedAttack.value
  }

  get baseDodge(): number {
    return this.data.system.base.combatAttributes.active.dodge.value
  }

  get woundThresholds(): WoundThresholds {
    const woundThresholdsWithMod =
      this.data.system.base.combatAttributes.passive.woundThresholds
    const calcWoundThreshold = (name: keyof WoundThresholds): number => {
      return woundThresholdsWithMod[name] + woundThresholdsWithMod.mod
    }
    return {
      first: calcWoundThreshold('first'),
      second: calcWoundThreshold('second'),
      third: calcWoundThreshold('third'),
    }
  }

  private get combatOptionsFromState(): Partial<
    MeleeCombatOptions | RangedCombatOptions
  > {
    let options: {
      weapon?: Weapon
      shield?: Shield
      secondaryWeapon?: Weapon
      talent?: BaseSkill
    } = {}
    const combatState = this.data.combatState
    if (combatState.isArmed) {
      if (
        combatState.primaryHand !== undefined &&
        ['meleeWeapon', 'rangedWeapon'].includes(combatState.primaryHand.type)
      ) {
        options = {
          weapon: GenericWeapon.create(combatState.primaryHand),
        }
        if (combatState.secondaryHand !== undefined) {
          if (combatState.secondaryHand?.type === 'shield') {
            options.shield = new GenericShield(
              combatState.secondaryHand as DataAccessor<'shield'>
            )
          }
          if (
            ['meleeWeapon', 'rangedWeapon'].includes(
              combatState.secondaryHand.type
            )
          ) {
            options.secondaryWeapon = GenericWeapon.create(
              combatState.secondaryHand as DataAccessor<WeaponItemType>
            )
          }
        }
      }
    } else if (combatState.unarmedTalent) {
      options = {
        talent: Skill.create(combatState.unarmedTalent, this),
      }
    }
    return options
  }

  private prepareCombatOptions(
    param?: BaseCombatOptions | boolean
  ): Partial<MeleeCombatOptions | RangedCombatOptions> {
    let options: Partial<MeleeCombatOptions | RangedCombatOptions>
    let useSecondaryHand = false
    if (param === undefined || typeof param === 'boolean') {
      useSecondaryHand = typeof param === 'boolean' ? param : false
      options = this.combatOptionsFromState
    } else {
      options = {
        ...this.combatOptionsFromState,
        ...param,
      }
    }

    const isArmedMelee = function (
      opt: Partial<MeleeCombatOptions>
    ): opt is ArmedMeleeCombatOptions {
      return (opt as ArmedMeleeCombatOptions)?.weapon !== undefined
    }

    if (
      isArmedMelee(options) &&
      options.secondaryWeapon &&
      (options.useSecondaryHand || useSecondaryHand)
    ) {
      options.weapon = options.secondaryWeapon
    }
    return options
  }

  private computeCombatParameters(
    name: CombatIdentifier,
    param?: MeleeCombatOptions | boolean
  ): CombatComputationResult | undefined {
    const options = this.prepareCombatOptions(param)

    const computationIdentifier = computationIdentifierByName(name)
    if (computationIdentifier === undefined) return

    return this.ruleset.compute(computationIdentifier, {
      character: this,
      ...options,
    })
  }

  private computeCombatValue(
    name: CombatIdentifier,
    param?: MeleeCombatOptions | boolean
  ): number {
    return this.computeCombatParameters(name, param)?.value || 0
  }

  private computeCombatModifiers(
    name: CombatIdentifier,
    param?: MeleeCombatOptions | boolean
  ): ModifierDescriptor[] {
    return this.computeCombatParameters(name, param)?.modifiers || []
  }

  attackValue(useSecondaryHand?: boolean): number
  attackValue(options?: MeleeCombatOptions): number
  attackValue(param?: MeleeCombatOptions | boolean): number {
    return this.computeCombatValue('attack', param)
  }

  attackModifiers(useSecondaryHand?: boolean): ModifierDescriptor[]
  attackModifiers(options?: MeleeCombatOptions): ModifierDescriptor[]
  attackModifiers(param?: MeleeCombatOptions | boolean): ModifierDescriptor[] {
    return this.computeCombatModifiers('attack', param)
  }

  parryValue(useSecondaryHand?: boolean): number
  parryValue(options?: MeleeCombatOptions): number
  parryValue(options?: MeleeCombatOptions | boolean): number {
    return this.computeCombatValue('parry', options)
  }

  parryModifiers(useSecondaryHand?: boolean): ModifierDescriptor[]
  parryModifiers(options?: MeleeCombatOptions): ModifierDescriptor[]
  parryModifiers(param?: MeleeCombatOptions | boolean): ModifierDescriptor[] {
    return this.computeCombatModifiers('parry', param)
  }

  rangedAttackValue(options?: RangedCombatOptions): number {
    return this.computeCombatValue('ranged', options)
  }

  rangedAttackModifiers(options?: RangedCombatOptions): ModifierDescriptor[] {
    return this.computeCombatModifiers('ranged', options)
  }

  dodgeValue(options?: MeleeCombatOptions): number {
    return this.computeCombatValue('dodge', options)
  }

  get encumbarance(): number {
    return this.ruleset.compute(ComputeEncumbarance, {
      character: this,
    })
  }

  effectiveEncumbarance(formula: string): number {
    return this.ruleset.compute(ComputeEffectiveEncumbarance, {
      character: this,
      formula,
    })
  }

  degreeModifier(degree: string): number {
    return this.ruleset.compute(ComputeDegreeModifier, {
      character: this,
      degree,
    })
  }

  get armorClass(): number {
    return this.ruleset.compute(ComputeArmorClass, {
      character: this,
    })
  }

  damage(useSecondaryHand?: boolean): RollFormula
  damage(options?: MeleeCombatOptions | RangedCombatOptions): RollFormula
  damage(
    param?: MeleeCombatOptions | RangedCombatOptions | boolean
  ): RollFormula {
    const options = this.prepareCombatOptions(param)

    return this.ruleset.compute(ComputeDamageFormula, {
      character: this,
      ...options,
    }).formula
  }

  get baseInitiative(): number {
    return this.data.baseInitiative
  }

  initiative(): InitiativeFormula {
    const combatState = this.data.combatState
    const unarmedTalent = combatState.isArmed
      ? undefined
      : combatState.unarmedTalent
      ? (Skill.create(combatState.unarmedTalent, this) as CombatTalent)
      : undefined
    const weapon =
      combatState.isArmed && combatState.primaryHand
        ? GenericWeapon.create(combatState.primaryHand)
        : undefined
    const shield =
      combatState.isArmed &&
      combatState.secondaryHand &&
      combatState.secondaryHand.type === 'shield'
        ? new GenericShield(combatState.secondaryHand as DataAccessor<'shield'>)
        : undefined
    return this.ruleset.compute(ComputeInitiativeFormula, {
      character: this,
      talent: unarmedTalent,
      weapon,
      shield,
    })
  }

  has(property: BaseProperty): boolean {
    return this.data.properties.some(
      (p) => p.system.sid === property.identifier
    )
  }

  private baseCombatAction(
    action: CombatIdentifier,
    options?: BaseCombatOptions
  ): void {
    const actionIdentifier = actionIdentifierByName(action)

    this.ruleset.execute(actionIdentifier, {
      character: this,
      ...this.prepareCombatOptions(options),
    })
  }

  attack(options?: BaseCombatOptions): void {
    this.baseCombatAction('attack', options)
  }

  rangedAttack(options?: BaseCombatOptions): void {
    this.baseCombatAction('ranged', options)
  }

  parry(options?: BaseCombatOptions): void {
    this.baseCombatAction('parry', options)
  }

  dodge(options?: BaseCombatOptions): void {
    this.baseCombatAction('dodge', options)
  }

  rollDamage(options?: BaseCombatOptions): void {
    this.baseCombatAction('damage', options)
  }

  maneuverList(
    maneuverType: ManeuverType,
    isArmed?: boolean
  ): ManeuverDescriptor[] {
    return this.ruleset.compute(ComputeManeuverList, {
      character: this,
      maneuverType,
      isArmed,
    }).maneuvers
  }

  get maneuverModifiers(): ManeuverModifier[] {
    return this.data.system.modifiers.maneuverModifiers.map((modifier) => ({
      ...modifier,
      value: parseInt(`${modifier.value}`),
    }))
  }

  talent(sid: string): BaseTalent | undefined {
    const talentData = this.data.talent(sid)
    const skill = talentData
      ? Skill.create(talentData, this, this.ruleset)
      : undefined
    return skill !== undefined && isBaseTalent(skill) ? skill : undefined
  }

  spell(sid: string): Spell | undefined {
    const spellData = this.data.spell(sid)
    return spellData !== undefined
      ? new GenericSpell(spellData, this, this.ruleset)
      : undefined
  }

  liturgy(sid: string): Liturgy | undefined {
    const liturgyData = this.data.liturgy(sid)
    return liturgyData !== undefined
      ? new GenericLiturgy(liturgyData, this, this.ruleset)
      : undefined
  }

  karmaTalent(): Talent | undefined {
    let karmaTalent = this.talent('talent-liturgiekenntnis')
    if (karmaTalent !== undefined && isTalent(karmaTalent)) {
      return karmaTalent
    }
    const karmaTalentData = this.data.talents.find(
      (talent) => talent.system.category === 'karma'
    )
    if (karmaTalentData !== undefined) {
      karmaTalent = Skill.create(
        karmaTalentData,
        this,
        this.ruleset
      ) as BaseTalent
      if (karmaTalent !== undefined && isTalent(karmaTalent)) {
        return karmaTalent
      }
    }
    return undefined
  }

  get armorItems(): Armor[] {
    return this.data.armorItems.map((item) => ({
      name: item.name || '',
      armorClass: item.system.armorClass,
      encumbarance: item.system.encumbarance,
    }))
  }

  get specialAbilities(): SpecialAbility[] {
    return this.data.specialAbilities.map((item) => ({
      name: item.name || '',
      identifier: item.system.sid,
    }))
  }

  get advantages(): Advantage[] {
    return this.data.advantages.map((item) => ({
      name: item.name || '',
      identifier: item.system.sid,
      value: item.system.value,
    }))
  }

  get disadvantages(): Disadvantage[] {
    return this.data.disadvantages.map((item) => ({
      name: item.name || '',
      identifier: item.system.sid,
      value: item.system.value,
      negativeAttribute: item.system.negativeAttribute,
    }))
  }

  get properties(): BaseProperty[] {
    return this.data.properties.map((item) => ({
      name: item.name || '',
      identifier: item.system.sid,
    }))
  }
}
