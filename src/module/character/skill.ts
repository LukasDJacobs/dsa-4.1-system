import type { BaseCharacter } from '../model/character.js'
import type {
  BaseSkill,
  Testable,
  SkillDescriptor,
  SkillType,
  Spell,
  Talent,
  TestAttributes,
  EffectiveEncumbarance,
  Liturgy,
  Rollable,
  BaseTalent,
} from '../model/properties.js'
import type { ActionIdentifier } from '../ruleset/rule-components.js'
import {
  LiturgyAction,
  SpellAction,
  TalentAction,
} from '../ruleset/rules/basic-skill.js'
import type {
  SkillActionData,
  SkillActionResult,
} from '../ruleset/rules/basic-skill.js'
import type { Ruleset } from '../ruleset/ruleset.js'
import { getGame } from '../utils.js'
import type { CombatTalent } from '../model/properties.js'
import type { ComputationIdentifier } from '../ruleset/rule-components.js'
import {
  CombatComputationData,
  CombatComputationResult,
  ComputeAttack,
  ComputeParry,
  ComputeRangedAttack,
} from '../ruleset/rules/derived-combat-attributes.js'
import type {
  DataAccessor,
  RollablSkillItemType,
  SkillItemType,
  TestableSkillItemType,
  TestableTalentItemType,
} from '../model/item-data.js'
import { Property } from './property.js'

interface SkillRollOptions {
  mod?: number
}

type OptionalValue<ItemData> = ItemData & {
  value?: number
}

type OptionalDataValue<T extends { system: any }> = T & {
  system: OptionalValue<T['system']>
}

export abstract class Skill<ItemType extends SkillItemType>
  extends Property<ItemType>
  implements BaseSkill
{
  public declare item: OptionalDataValue<DataAccessor<ItemType>>
  protected declare character: BaseCharacter
  public abstract skillType: SkillType

  get value(): number {
    return this.item.system.value || 0
  }

  static create(
    item: DataAccessor<SkillItemType>,
    character: BaseCharacter,
    ruleset?: Ruleset
  ): BaseSkill | undefined {
    let skill: BaseSkill
    switch (item.type) {
      case 'spell':
        skill = new GenericSpell(
          item as DataAccessor<'spell'>,
          character,
          ruleset
        )
        break
      case 'talent':
      case 'language':
      case 'scripture':
        skill = new GenericTalent(
          item as DataAccessor<TestableTalentItemType>,
          character,
          ruleset
        )
        break
      case 'combatTalent':
        skill = new GenericCombatTalent(
          item as DataAccessor<'combatTalent'>,
          character,
          ruleset
        )
        break
      case 'liturgy':
        skill = new GenericLiturgy(
          item as DataAccessor<'liturgy'>,
          character,
          ruleset
        )
        break
    }
    return skill
  }
}

export abstract class RollableSkill<ItemType extends RollablSkillItemType>
  extends Skill<ItemType>
  implements Rollable<BaseSkill>
{
  protected abstract skillAction: ActionIdentifier<
    SkillActionData,
    SkillActionResult
  >
  private ruleset: Ruleset

  constructor(
    item: DataAccessor<ItemType>,
    character: BaseCharacter,
    ruleset?: Ruleset
  ) {
    super(item, character)

    if (ruleset) {
      this.ruleset = ruleset
    } else {
      this.ruleset = getGame().ruleset
    }
  }

  private get skillDescriptor(): SkillDescriptor {
    return {
      name: this.name,
      identifier: this.identifier,
      skillType: this.skillType,
    }
  }

  roll(options: SkillRollOptions): void {
    this.ruleset.execute(this.skillAction, {
      character: this.character,
      ...options,
      skill: this.skillDescriptor,
    })
  }

  static create(
    item: DataAccessor<TestableSkillItemType>,
    character: BaseCharacter,
    ruleset?: Ruleset
  ): Rollable<BaseSkill> | undefined {
    return Skill.create(item, character, ruleset) as Rollable<BaseSkill>
  }
}

export abstract class TestableSkill<ItemType extends TestableSkillItemType>
  extends RollableSkill<ItemType>
  implements Testable<BaseSkill>
{
  get testAttributes(): TestAttributes {
    return [
      this.item.system.test.firstAttribute,
      this.item.system.test.secondAttribute,
      this.item.system.test.thirdAttribute,
    ]
  }

  static create(
    item: DataAccessor<TestableSkillItemType>,
    character: BaseCharacter,
    ruleset?: Ruleset
  ): Testable<BaseSkill> | undefined {
    return Skill.create(item, character, ruleset) as Testable<BaseSkill>
  }
}

export function isBaseTalent(skill: BaseSkill): skill is BaseTalent {
  return skill.skillType === 'talent'
}

export function isTalent(skill: BaseSkill): skill is Talent {
  return isBaseTalent(skill) && skill.talentType === 'normal'
}

export function isLiturgy(skill: BaseSkill): skill is Liturgy {
  return skill.skillType === 'liturgy'
}

export class GenericTalent
  extends TestableSkill<TestableTalentItemType>
  implements Talent
{
  public skillType = 'talent' as const
  public talentType = 'normal' as const

  protected skillAction: ActionIdentifier<SkillActionData, SkillActionResult> =
    TalentAction

  constructor(
    item: DataAccessor<TestableTalentItemType>,
    character: BaseCharacter,
    ruleset?: Ruleset
  ) {
    super(item, character, ruleset)
  }

  get effectiveEncumbarance(): EffectiveEncumbarance {
    return this.item.system.effectiveEncumbarance
  }
}

export class GenericSpell extends TestableSkill<'spell'> implements Spell {
  public skillType = 'spell' as const
  protected skillAction: ActionIdentifier<SkillActionData, SkillActionResult> =
    SpellAction

  constructor(
    item: DataAccessor<'spell'>,
    character: BaseCharacter,
    ruleset?: Ruleset
  ) {
    super(item, character, ruleset)
  }
}

export class GenericCombatTalent
  extends Skill<'combatTalent'>
  implements CombatTalent
{
  public skillType = 'talent' as const
  public talentType = 'combat' as const
  private ruleset: Ruleset

  constructor(
    item: DataAccessor<'combatTalent'>,
    character: BaseCharacter,
    ruleset?: Ruleset
  ) {
    super(item, character)
    if (ruleset) {
      this.ruleset = ruleset
    } else {
      this.ruleset = getGame().ruleset
    }
  }

  private computeValue(
    identifier: ComputationIdentifier<
      CombatComputationData,
      CombatComputationResult
    >
  ): number {
    return this.ruleset.compute(identifier, {
      character: this.character,
      talent: this,
    }).value
  }

  get attack(): number {
    return this.computeValue(ComputeAttack)
  }

  get parry(): number {
    return this.computeValue(ComputeParry)
  }

  get rangedAttack(): number {
    return this.computeValue(ComputeRangedAttack)
  }

  get attackMod(): number {
    return this.item.system.combat.attack
  }

  get parryMod(): number {
    return this.item.system.combat.parry
  }

  get rangedAttackMod(): number {
    return this.item.system.combat.rangedAttack
  }

  get isUnarmed(): boolean {
    return ['talent-raufen', 'talent-ringen'].includes(this.item.system.sid)
  }

  get effectiveEncumbarance(): EffectiveEncumbarance {
    return this.item.system.effectiveEncumbarance
  }
}
export class GenericLiturgy
  extends RollableSkill<'liturgy'>
  implements Liturgy, Testable<BaseSkill>
{
  public skillType = 'liturgy' as const
  private karmaTalent: Talent | undefined
  protected skillAction: ActionIdentifier<SkillActionData, SkillActionResult> =
    LiturgyAction

  constructor(
    item: DataAccessor<'liturgy'>,
    character: BaseCharacter,
    ruleset?: Ruleset
  ) {
    super(item, character, ruleset)
    this.karmaTalent = character.karmaTalent()
  }

  get degree(): string {
    return this.item.system.degree
  }

  get value(): number {
    if (this.karmaTalent) return this.karmaTalent.value

    return 0
  }

  get testAttributes(): TestAttributes {
    if (this.karmaTalent) return this.karmaTalent.testAttributes

    return ['courage', 'intuition', 'charisma']
  }
}
