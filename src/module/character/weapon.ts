import { DataAccessor, WeaponItemType } from '../model/item-data.js'
import type {
  MeleeWeapon,
  RangedWeapon,
  RollFormula,
  StrengthMod,
  Weapon,
  WeaponMod,
  WeaponType,
} from '../model/items.js'

export abstract class GenericWeapon implements Weapon {
  public class = 'weapon' as const

  protected item: DataAccessor<WeaponItemType>
  public abstract type: WeaponType

  constructor(item: DataAccessor<WeaponItemType>) {
    this.item = item
  }

  get name(): string {
    return this.item.name || ''
  }

  get talent(): string {
    return this.item.system.talent
  }

  get damage(): RollFormula {
    return this.item.system.damage
  }

  static create(item: DataAccessor<WeaponItemType>): Weapon | undefined {
    switch (item.type) {
      case 'meleeWeapon':
        return new GenericMeleeWeapon(item)
      case 'rangedWeapon':
        return new GenericRangedWeapon(item)
    }
  }
}

export class GenericMeleeWeapon extends GenericWeapon implements MeleeWeapon {
  public type = 'melee' as const
  protected declare item: DataAccessor<'meleeWeapon'>

  get initiativeMod(): number {
    return this.item.system.initiativeMod
  }

  get weaponMod(): WeaponMod {
    return this.item.system.weaponMod
  }

  get strengthMod(): StrengthMod {
    return this.item.system.strengthMod
  }
}

export class GenericRangedWeapon extends GenericWeapon implements RangedWeapon {
  public type = 'ranged' as const
  protected declare item: DataAccessor<'rangedWeapon'>
}
