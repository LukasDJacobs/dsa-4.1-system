import { SkillRollEventListener } from './actor/actor-sheet.js'
import { isProperty } from './model/item-data.js'
import { getGame } from './utils.js'

Hooks.on('renderSidebarTab', (_0, html) => {
  const chat = html[0]?.querySelector('#chat-message')
  if (!chat) return

  chat.addEventListener('drop', (event) => handleChatItemDrop(chat, event))
})

async function handleChatItemDrop(chat, event) {
  let data
  try {
    data = JSON.parse(event.dataTransfer.getData('text/plain'))
    if (data.type !== 'Item') return
  } catch (err) {
    return false
  }

  const item = await Item.fromDropData(data)
  if (item?.type === 'talent') {
    ChatMessage.create({
      content: `<button class="talent-from-chat" data-sid="${item.system.sid}">${item.name}</button>`,
    })
  }
}

function onCustomButtonClick(event) {
  const button = event.currentTarget
  const sid = $(button).data('sid')

  rollSkillBySid(sid)
}

Hooks.on('renderChatMessage', (app, html) => {
  html.find('button.talent-from-chat').click(onCustomButtonClick)
})

function rollSkillBySid(sid) {
  const game = getGame()
  const actor = game.user?.character as Actor
  if (actor === undefined) {
    ui.notifications?.warn('No Actor selected!')
  }

  const item = actor?.items?.filter(
    (i) => isProperty(i.data.data) && i.data.data.sid === sid
  )[0]
  if (item) {
    const eventHandler = new SkillRollEventListener(actor)
    const event = {
      currentTarget: {
        name: item.id,
      },
    }
    eventHandler.handleEvent(event)
  } else {
    ui.notifications?.info('Actor does not own this skill!')
  }
}

declare class QuickInsert {
  static open(options: any)
}

Hooks.once('init', function () {
  const game = getGame()
  game.keybindings.register('dsa-4.1', 'QuickRoll', {
    name: 'QUICKINSERT.SettingsQuickOpen',
    textInput: true,
    editable: [
      { key: 'Space', modifiers: [KeyboardManager.MODIFIER_KEYS.SHIFT] },
    ],
    onDown: callQuickInsert,
    precedence: CONST.KEYBINDING_PRECEDENCE.NORMAL,
  })
})

function callQuickInsert() {
  const game = getGame()

  QuickInsert.open({
    filter: 'actor.items',
    onSubmit: async (searchItem) => {
      const pack = await game.packs.get(searchItem.package)
      const item = await pack?.getDocument(searchItem.id)

      if (item) {
        rollSkillBySid(item.system.sid)
      }
    },
  })
}
